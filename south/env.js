module.exports.getStartDate = (season) => {
  if (season === "2020") return '2020-09-12';
  if (season === "2021") return '2021-07-25';
  if (season === "2022") return '2022-07-25';
  return `${season}-08-05`;
}

const POINTS_FOR_VICTORY = [0, 16, 24];
//const POINTS_FOR_VICTORY = [0];
module.exports.POINTS_FOR_VICTORY = POINTS_FOR_VICTORY;

const POINTS_FOR_DEFEAT = [0, 16, 24];
//const POINTS_FOR_DEFEAT = [16];
module.exports.POINTS_FOR_DEFEAT = POINTS_FOR_DEFEAT;

const BET_IF_SCORE_SUPERIOR_TO = [0, 8, 16];
//const BET_IF_SCORE_SUPERIOR_TO = [0];
module.exports.BET_IF_SCORE_SUPERIOR_TO = BET_IF_SCORE_SUPERIOR_TO;

const BET_IF_ODD_SUPERIOR_TO = [1.9, 2.1, 2.4, 2.6, 2.8, 3.2, 3.4];
//const BET_IF_ODD_SUPERIOR_TO = [1.8];
module.exports.BET_IF_ODD_SUPERIOR_TO = BET_IF_ODD_SUPERIOR_TO;

const POINTS_PER_GOAL_SCORED = [-4, -2, 0, 4];
module.exports.POINTS_PER_GOAL_SCORED = POINTS_PER_GOAL_SCORED;

const POINTS_PER_GOAL_TAKEN = [-4, -2, 0, 4];
module.exports.POINTS_PER_GOAL_TAKEN = POINTS_PER_GOAL_TAKEN;

const FACTOR_M1 = [1, 2, 3, 4];
module.exports.FACTOR_M1 = FACTOR_M1;

const FACTOR_M2 = [0, 1];
module.exports.FACTOR_M2 = FACTOR_M2;

const FACTOR_M3 = [0, 1];
module.exports.FACTOR_M3 = FACTOR_M3;

const VICTORIES_RATE_BONUS = [0];
module.exports.VICTORIES_RATE_BONUS = VICTORIES_RATE_BONUS;

const HOME_BONUS = [0];
module.exports.HOME_BONUS = HOME_BONUS;

const BET_ON_DRAW = [false];
module.exports.BET_ON_DRAW = BET_ON_DRAW;

module.exports.getNumSimulationsPerProcess = () => {
  /*
    Do not count POINTS_FOR_VICTORY because there is
    one process per season per POINTS_FOR_VICTORY
  */
  return  POINTS_FOR_DEFEAT.length *
  BET_IF_SCORE_SUPERIOR_TO.length *
  BET_IF_ODD_SUPERIOR_TO.length *
  POINTS_PER_GOAL_TAKEN.length *
  POINTS_PER_GOAL_SCORED.length *
  FACTOR_M1.length *
  FACTOR_M2.length *
  FACTOR_M3.length *
  VICTORIES_RATE_BONUS.length *
  HOME_BONUS.length *
  BET_ON_DRAW.length;
}

module.exports.bestConfig = {
  "pointsForVictory": 24,
  "pointsForDefeat": 16,
  "betIfScoreSuperiorTo": 0,
  "betIfOddSuperiorTo": 2.4,
  "pointsPerGoalScored": -4,
  "pointsPerGoalTaken": 4,
  "factorM1": 2,
  "factorM2": 0,
  "factorM3": 1,
  "victoriesRateBonus": 0,
  "homeBonus": 0,
  "betOnDraw": false,
  // 2018 17/4
  // 2019 17/3
  /* "pointsForVictory": 25,
  "pointsForDefeat": 5,
  "betIfScoreSuperiorTo": 125,
  "betIfOddSuperiorTo": 1.1,
  "goalDiffFactor": 12,
  "factorM1": 2,
  "factorM2": 0,
  "factorM3": 0,
  "victoriesRateBonus": 2,
  doNotBetOnTeams: {} */
};
