module.exports.getProcessArgv = (param) => {
  const index = process.argv.findIndex((arg) => arg === param);
  if (index === -1) {
    return undefined;
  }

  return process.argv[index + 1];
};

module.exports.rightPad = (str, length) => {
  let s = str;
  for (let i = 0; i <= length - str.length; i += 1) {
    s += " ";
  }
  return s;
};

module.exports.IGNORE_OP = {
  'all': [],
};

module.exports.teamsCompleteName = {
  "Anderlecht": "Anderlecht",
  "Antwerp": "Antwerp",
  "Club Brugge KV": "Club Brugge KV",
  "Brugge": "Club Brugge KV",
  "Cercle Brugge KSV": "Cercle Brugge KSV",
  "Cercle Brugge": "Cercle Brugge KSV",
  "Charleroi": "Charleroi",
  "Genk": "Genk",
  "Royale Union SG": "Royale Union SG",
  "Union Saint-Gilloise": "Royale Union SG",
  "KV Mechelen": "KV Mechelen",
  "Gent": "Gent",
  "La Gantoise": "Gent",
  "St. Liege": "St. Liege",
  "Standard Liege": "St. Liege",
  "Antwerp": "Antwerp",
  "Eupen": "Eupen",
  "Seraing": "Seraing",
  "Seraing United": "Seraing",
  "Kortrijk": "Kortrijk",
  "KV Courtrai": "Kortrijk",
  "Courtrai": "Kortrijk",
  "Waregem": "Waregem",
  "Zulte Waregem": "Waregem",
  "Oostende": "Oostende",
  "KV Mechelen": "KV Mechelen",
  "Leuven": "Leuven",
  "OH Leuven": "Leuven",
  "St. Truiden": "St. Truiden",
  "Sint-Truiden": "St. Truiden",
  "Westerlo": "Westerlo",
  "KFCO Beerschot-Wilrijk": "Wilrijk",
  "Beerschot VA": "Wilrijk",
  "Wilrijk": "Wilrijk",
};
