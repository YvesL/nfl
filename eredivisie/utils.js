module.exports.getProcessArgv = (param) => {
  const index = process.argv.findIndex((arg) => arg === param);
  if (index === -1) {
    return undefined;
  }

  return process.argv[index + 1];
};

module.exports.rightPad = (str, length) => {
  let s = str;
  for (let i = 0; i <= length - str.length; i += 1) {
    s += " ";
  }
  return s;
};

module.exports.IGNORE_OP = {
  'all': [],
};

module.exports.teamsCompleteName = {
  "Utrecht": "Utrecht",
  "Fortuna Sittard": "Sittard",
  "Sittard": "Sittard",
  "Excelsior": "Excelsior",
  "Groningen": "Groningen",
  "Nijmegen": "Nijmegen",
  "Waalwijk": "Waalwijk",
  "RKC Waalwijk": "Waalwijk",
  "FC Volendam": "FC Volendam",
  "Volendam": "FC Volendam",
  "Nijmegen": "Nijmegen",
  "Nec Nijmegen": "Nijmegen",
  "NEC Nimègue": "Nijmegen",
  "Sparta Rotterdam": "Sparta Rotterdam",
  "Jong Sparta Rotterdam": "Sparta Rotterdam",
  "FC Emmen": "FC Emmen",
  "Emmen": "FC Emmen",
  "Heerenveen": "Heerenveen",
  "Groningen": "Groningen",
  "Feyenoord": "Feyenoord",
  "G.A. Eagles": "G.A. Eagles",
  "Go Ahead Eagles": "G.A. Eagles",
  "PSV": "PSV",
  "PSV Eindhoven": "PSV",
  "Ajax": "Ajax",
  "AZ Alkmaar": "AZ Alkmaar",
  "Az Alkmaar": "AZ Alkmaar",
  "AZ": "AZ Alkmaar",
  "Twente": "Twente",
  "Cambuur": "Cambuur",
  "Cambuur Leeuwarden": "Cambuur",
  "Heracles": "Heracles",
  "Vitesse": "Vitesse",
  "Vitesse Arnhem": "Vitesse",
  "Willem II": "Willem II",
  "Zwolle": "Zwolle",
};
