
const fs = require("fs");
const { blake2b } = require('blakejs');
const { createClient } = require('redis')

const { getProcessArgv, getSingleProcessArgv, toId } = require("./utils");

let client;
let fromMemory = 0;
let run;

const noCache = getSingleProcessArgv("--no-cache");
let defaultOdd = getProcessArgv("--default-odd");
if (defaultOdd) {
  defaultOdd = parseFloat(defaultOdd, 10);
} else {
  defaultOdd = null;
}
const pointsForVictory = parseInt(getProcessArgv("--pointsForVictory"), 10);
if (typeof pointsForVictory !== 'number') {
  throw new Error('Missing --pointsForVictory')
}
const season = getProcessArgv("--season");
if (typeof season !== 'string' || season.length !== 4) {
  throw new Error('Missing --season')
}
const daysToRun = parseInt(getProcessArgv("--daysToRun"), 10);
if (typeof daysToRun !== 'number') {
  throw new Error('Missing --daysToRun')
}
const startDate = getProcessArgv("--start-date");
const childProcess = getProcessArgv("--childProcess");

let games;
let gamesStringified;
let envs;
let league;
if (getSingleProcessArgv("--nfl")) {
  league = 'nfl';
  run = require("./run").run;
} else if (getSingleProcessArgv("--nba")) {
  league = 'nba';
  run = require("./run").run;
} else if (getSingleProcessArgv("--ncaa")) {
  league = 'ncaa';
  run = require("./run").run;
} else if (getSingleProcessArgv("--bundesliga")) {
  league = 'bundesliga';
  run = require("./runSoccer.js").run;
} else if (getSingleProcessArgv("--bundesliga2")) {
  league = 'bundesliga2';
  run = require("./runSoccer.js").run;
} else if (getSingleProcessArgv("--ligue1")) {
  league = 'ligue1';
  run = require("./runSoccer.js").run;
} else if (getSingleProcessArgv("--seriea")) {
  league = 'seriea';
  run = require("./runSoccer.js").run;
} else if (getSingleProcessArgv("--serieb")) {
  league = 'serieb';
  run = require("./runSoccer.js").run;
} else if (getSingleProcessArgv("--superlig")) {
  league = 'superlig';
  run = require("./runSoccer.js").run;
} else if (getSingleProcessArgv("--liga")) {
  league = 'liga';
  run = require("./runSoccer.js").run;
} else if (getSingleProcessArgv("--premiere")) {
  league = 'premiere';
  run = require("./runSoccer.js").run;
} else if (getSingleProcessArgv("--championship")) {
  league = 'championship';
  run = require("./runSoccer.js").run;
} else if (getSingleProcessArgv("--eredivisie")) {
  league = 'eredivisie';
  run = require("./runSoccer.js").run;
} else if (getSingleProcessArgv("--primeira")) {
  league = 'primeira';
  run = require("./runSoccer.js").run;
} else if (getSingleProcessArgv("--oneliga")) {
  league = 'oneliga';
  run = require("./runSoccer.js").run;
} else if (getSingleProcessArgv("--superliga")) {
  league = 'superliga';
  run = require("./runSoccer.js").run;
} else if (getSingleProcessArgv("--jupiler")) {
  league = 'jupiler';
  run = require("./runSoccer.js").run;
} else if (getSingleProcessArgv("--botola")) {
  league = 'botola';
  run = require("./runSoccer.js").run;
} else if (getSingleProcessArgv("--leagueone")) {
  league = 'leagueone';
  run = require("./runSoccer.js").run;
} else if (getSingleProcessArgv("--leaguetwo")) {
  league = 'leaguetwo';
  run = require("./runSoccer.js").run;
} else if (getSingleProcessArgv("--north")) {
  league = 'north';
  run = require("./runSoccer.js").run;
} else if (getSingleProcessArgv("--south")) {
  league = 'south';
  run = require("./runSoccer.js").run;
} else {
  throw new Error("no competition");
}

envs = `./${league}/env.js`;
gamesStringified = fs.readFileSync(`./${league}/games-wh-${season}.json`, "utf8");
games = JSON.parse(gamesStringified);

const {
  POINTS_FOR_DEFEAT,
  BET_IF_SCORE_SUPERIOR_TO,
  BET_IF_ODD_SUPERIOR_TO,
  POINTS_PER_GOAL_SCORED,
  POINTS_PER_GOAL_TAKEN,
  FACTOR_M1,
  FACTOR_M2,
  FACTOR_M3,
  VICTORIES_RATE_BONUS,
  HOME_BONUS,
  getStartDate,
  BET_ON_DRAW,
  getNumSimulationsPerProcess,
} = require(envs);

const buf = Buffer.from(JSON.stringify({
  games: games,
}), 'utf8');
const uInt8Array = new Uint8Array(buf);
const blake2bHash = blake2b(uInt8Array, 0, 32);
const hashOfGames = Buffer.from(blake2bHash).toString('hex');

const matrix = {};

matrix[season] = {};

if (typeof defaultOdd === "number") {
  odds = null;
}

const init = async () => {
  const redisDatabase = `${{
    bundesliga: 1,
    bundesliga2: 1,
    liga: 2,
    jupiler: 2,
    primeira: 2,
    seriea: 3,
    leagueone: 3,
    botola: 4,
    championship: 4,
    north: 4,
    premiere: 5,
    ligue1: 5,
    ligue2: 5,
    nba: 6,
    leaguetwo: 6,
    ncaa: 7,
    nfl: 7,
    seriea: 8,
    superliga: 8,
    serieb: 9,
    oneliga: 9,
    superlig: 10,
    south: 10,
    eredivisie: 10,
  }[league]}`
  client = createClient();
  client.on('error', (err) => {
    throw err
  });
  await client.connect();
  const a = await client.sendCommand(['select', redisDatabase]);
  go();
}

const go = () => {
  
  const numSimulationsPerProcess = getNumSimulationsPerProcess();

  let t = Math.round(new Date().getTime() / 1000);
  let i = 0;
  for (let j = 0; j < POINTS_FOR_DEFEAT.length; j += 1) {
    const pointsForDefeat = POINTS_FOR_DEFEAT[j];

    /* update progresses.json file */
    let progress = {};
    try {
      progress = JSON.parse(fs.readFileSync('./progresses.json', 'utf8'))
    } catch (err) {
    }
    const now = Math.round(new Date().getTime() / 1000);
    if (!progress[league]) progress[league] = {};
    if (!progress[league][season]) progress[league][season] = {};
    progress[league][season][childProcess] = `${Math.round(
      (j * 100) / (POINTS_FOR_DEFEAT.length)
    )}% in ${Math.round((now - t) / 60)} min`;

    fs.writeFileSync(
      './progresses.json',
      JSON.stringify(
        progress,
        null,
        1
      ),
      'utf8'
    );

    for (let k = 0; k < BET_IF_SCORE_SUPERIOR_TO.length; k += 1) {
      const betIfScoreSuperiorTo = BET_IF_SCORE_SUPERIOR_TO[k];
      for (let k2 = 0; k2 < BET_IF_ODD_SUPERIOR_TO.length; k2 += 1) {
        const betIfOddSuperiorTo = BET_IF_ODD_SUPERIOR_TO[k2];
        for (let l = 0; l < POINTS_PER_GOAL_SCORED.length; l += 1) {
          const pointsPerGoalScored = POINTS_PER_GOAL_SCORED[l];
          for (let s = 0; s < POINTS_PER_GOAL_TAKEN.length; s += 1) {
            const pointsForGoalTaken = POINTS_PER_GOAL_TAKEN[s];
            for (let m = 0; m < FACTOR_M1.length; m += 1) {
              const factorM1 = FACTOR_M1[m];
              for (let n = 0; n < FACTOR_M2.length; n += 1) {
                const factorM2 = FACTOR_M2[n];
                for (let o = 0; o < FACTOR_M3.length; o += 1) {
                  const factorM3 = FACTOR_M3[o];
                  for (let p = 0; p < HOME_BONUS.length; p += 1) { 
                    const homeBonus = HOME_BONUS[p];
                    
                    // BET_ON_DRAW does not exist in non-soccer leagues
                    for (let q = 0; q < BET_ON_DRAW.length; q += 1) {
                      const betOnDraw = BET_ON_DRAW[q];
                      for (let r = 0; r < VICTORIES_RATE_BONUS.length; r += 1) {
                        const victoriesRateBonus = VICTORIES_RATE_BONUS[r];
        
                        const id = toId({ pointsForVictory, pointsForDefeat, betIfScoreSuperiorTo, betIfOddSuperiorTo, pointsPerGoalScored, pointsForGoalTaken, factorM1, factorM2, factorM3, victoriesRateBonus, homeBonus, betOnDraw });
                        const dbId = `memo:${hashOfGames}${id}${daysToRun}`;
                        client.get(dbId).then(value => {
                          if (value && !noCache) {
                            fromMemory += 1;
                            matrix[season][id] = {
                              results: JSON.parse(value),
                            };
                            i += 1;
                            if (i === numSimulationsPerProcess) {
                              end()
                            }
                          } else {
                            const res = run(
                              games,
                              defaultOdd,
                              pointsForVictory,
                              pointsForDefeat,
                              betIfScoreSuperiorTo,
                              betIfOddSuperiorTo,
                              pointsPerGoalScored,
                              pointsForGoalTaken,
                              factorM1,
                              factorM2,
                              factorM3,
                              victoriesRateBonus,
                              homeBonus,
                              betOnDraw,
                              season,
                              startDate || getStartDate(season),
                              daysToRun,
                            );
                            matrix[season][id] = {
                              results: res.results,
                            };
                            client.set(dbId, JSON.stringify(res.results)).then(() => {
                              i += 1;
                              if (i === numSimulationsPerProcess) {
                                end()
                              }
                            });
                          }
                        })
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

const end = () => {
  const keys = Object.keys(matrix[season]);
  let topSimulations = { [season]: {} };
  const topNetKeys = keys
    .sort((k1, k2) => {
      return (
        matrix[season][k2].results.net -
        matrix[season][k1].results.net
      );
    });
  
  const topRateKeys = keys
    .sort((k1, k2) => {
      return (
        matrix[season][k2].results.rate -
        matrix[season][k1].results.rate
      );
    });
  
  topNetKeys.concat(topRateKeys).forEach((key) => {
    topSimulations[season][key] = matrix[season][key];
  });

  fs.writeFileSync(
    `./tmp/hash-${league}-${season}.txt`,
    hashOfGames,
    "utf8"
  );
  fs.writeFileSync(
    `./tmp/result-${league}-${season}-${childProcess}.json`,
    JSON.stringify({
      matrix: topSimulations,
      fromMemory: fromMemory
    }),
    "utf8"
  );
  process.exit();
}


init();