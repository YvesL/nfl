const puppeteer = require("puppeteer");
const { parse } = require("node-html-parser");
const fs = require("fs");
const { parseDate, getProcessArgv } = require('./utils');

const findArg = (param) => {
  return process.argv.findIndex((arg) => arg === param) !== -1
}
let HOURS_OFFSET_PARIS = 0;
const YEARS_BASED_ON_MONTH = {
  1: {
    'Jan': 1,
    'Feb': 1,
    'Mar': 1,
    'Apr': 1,
    'May': 1,
    'Jun': 1,
  },
  1: {
    'Jan': 1,
    'Feb': 1,
    'Mar': 1,
    'Apr': 1,
    'May': 1,
    'Jun': 1,
    'Jul': 1,
    'Aug': 1,
  },
}
let YEAR_BASED_ON_MONTH;
let SEASON = 2022;
if (findArg('--season') && ["2018", "2019", "2020", "2021"].includes(getProcessArgv('--season'))) {
  SEASON = parseInt(getProcessArgv('--season'));
}

let games;
let url;
let path;
let teamsCompleteName;
let league;
if (findArg('--bundesliga')) {
  league = 'bundesliga';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/germany/bundesliga/results/`;
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/soccer/germany/bundesliga-2021-2022/results/'
  }
} else if (findArg('--bundesliga2')) {
  league = 'bundesliga2';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/germany/2-bundesliga/results/`;
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/soccer/germany/2-bundesliga-2021-2022/results/'
  }
} else if (findArg('--seriea')) {
  league = 'seriea';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/italy/serie-a/results/`;
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/soccer/italy/serie-a-2021-2022/results/'
  }
} else if (findArg('--serieb')) {
  league = 'serieb';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/italy/serie-b/results/`;
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/soccer/italy/serie-b-2021-2022/results/'
  }
} else if (findArg('--superlig')) {
  league = 'superlig';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/turkey/super-lig/results/`;
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/soccer/turkey/super-lig-2021-2022/results/'
  }
} else if (findArg('--liga')) {
  league = 'liga';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/spain/laliga/results/`;
  if (SEASON === 2021) {
    url = `https://www.oddsportal.com/soccer/spain/laliga-2021-2022/results/`;
  }
} else if (findArg('--premiere')) {
  league = 'premiere';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/england/premier-league/results/`;
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/soccer/england/premier-league-2021-2022/results/'
  }
} else if (findArg('--championship')) {
  league = 'championship';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/england/championship/results/`;
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/soccer/england/championship-2021-2022/results/'
  } else if (SEASON === 2019) {
    url = 'https://www.oddsportal.com/soccer/england/championship-2019-2020/results/'
  }
} else if (findArg('--eredivisie')) {
  league = 'eredivisie';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/netherlands/eredivisie/results/`;
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/soccer/netherlands/eredivisie-2021-2022/results/'
  }
} else if (findArg('--primeira')) {
  league = 'primeira';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/portugal/liga-portugal/results/`;
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/soccer/portugal/liga-portugal-2021-2022/results/'
  }
} else if (findArg('--oneliga')) {
  league = 'oneliga';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/czech-republic/1-liga/results/`;
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/soccer/czech-republic/1-liga-2021-2022/results/'
  }
} else if (findArg('--superliga')) {
  league = 'superliga';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/serbia/super-liga/results/`;
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/soccer/serbia/super-liga-2021-2022/results/'
  }
} else if (findArg('--jupiler')) {
  league = 'jupiler';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/belgium/jupiler-pro-league/results/`;
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/soccer/belgium/jupiler-pro-league-2021-2022/results/'
  }
} else if (findArg('--leagueone')) {
  league = 'leagueone';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/england/league-one/results/`;
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/soccer/england/league-one-2021-2022/results/';
  } else if (SEASON === 2018) {
    url = 'https://www.oddsportal.com/soccer/england/league-one-2018-2019/results/';
  } else if (SEASON === 2019) {
    url = 'https://www.oddsportal.com/soccer/england/league-one-2019-2020/results/';
  }
} else if (findArg('--leaguetwo')) {
  league = 'leaguetwo';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/england/league-two/results/`;
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/soccer/england/league-two-2021-2022/results/';
  } else if (SEASON === 2019) {
    url = 'https://www.oddsportal.com/soccer/england/league-two-2019-2020/results/';
  } else if (SEASON === 2018) {
    url = 'https://www.oddsportal.com/soccer/england/league-two-2018-2019/results/';
  }
} else if (findArg('--botola')) {
  league = 'botola';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/morocco/botola-pro/results/`;
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/soccer/morocco/botola-pro-2021-2022/results/';
  }
} else if (findArg('--nba')) {
  league = 'nba';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[2]
  HOURS_OFFSET_PARIS = -8;
  url = `https://www.oddsportal.com/basketball/usa/nba/results/`;
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/basketball/usa/nba-2021-2022/results/'
  } else if (SEASON === 2020) {
    url = 'https://www.oddsportal.com/basketball/usa/nba-2020-2021/results/'
  }
} else {
  throw new Error('unknown league, provide for example --bundesliga')
}
teamsCompleteName = require(__dirname + `/${league}/utils`).teamsCompleteName;
path = `${__dirname}/${league}/games-wh-${SEASON}.json`;
games = JSON.parse(fs.readFileSync(path, "utf8"));

let i = 1;
let j = 0;
const go = async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  console.log(url + `#/page/${i}/`)
  await page.goto(url + `#/page/${i}/`);

  await new Promise((r) => {
    setTimeout(r, 6000);
  });
  console.log('(1)');
  const k = i;
  setTimeout(() => {
    if (k === i) {
      console.log('stuck ?')
      console.log('stuck at ', i, "saving games");
      fs.writeFileSync(
        path,
        JSON.stringify(games, null, 2),
        'utf8'
      )
      process.exit(0);
    }
  }, 15000);
  let html;
  try {
    html = await page.evaluate(() => {
      return document.querySelectorAll('div#tournamentTable')[0].innerHTML;
    });
  } catch (err) {
    console.log(err);
    html = null;
  }
  console.log('(2)')

  if (html) {
    const root = parse(html);
    const lines = root.querySelectorAll("tr");

    if (lines.length === 1) {
      console.log(j, 'games recorded');
      fs.writeFileSync(
        path,
        JSON.stringify(games, null, 2),
        'utf8'
      )
      process.exit(0);
    }
    let date;
    let playoffs = false;
    lines.forEach(a => {
      let d;
      try {
        if (a.childNodes[0].childNodes[0].rawTagName === 'span') {
          d = a.childNodes[0].childNodes[0].childNodes[0].rawText;
          // Skip Play Offs or Play Out or Relegation games
          if (a.childNodes[0].childNodes[1] && a.childNodes[0].childNodes[1].rawText) {
            if (a.childNodes[0].childNodes[1].rawText.includes('canc')) {
              console.log('skip because of Canc')
              playoffs = true;
            }
            if (a.childNodes[0].childNodes[1].rawText.includes('Leg')) {
              console.log('skip because of Leg')
              playoffs = true;
            }
            if (a.childNodes[0].childNodes[1].rawText.includes('Relegation')) {
              console.log('skip because of relegation game')
              playoffs = true;
            }
            if (a.childNodes[0].childNodes[1].rawText.includes('Championship Group')) {
              console.log('skip because of Championship Group game')
              playoffs = true;
            }
            if (a.childNodes[0].childNodes[1].rawText.includes('Play')) {
              console.log('skip because of play offs')
              playoffs = true;
            }
          } else {
            playoffs = false;
          }
        }
      } catch (e) {}
      if (d) {
        console.log('d', d)
        date = parseDate(
          d,
          0, 
          0,
          SEASON,
          YEAR_BASED_ON_MONTH
        );
        return;
      }
      if (playoffs) {
        return;
      }
      if (date === "ignore") {
        return;
      }
      if (!a.childNodes[1] || typeof a.childNodes[1].childNodes[0] === 'undefined') {
        return;
      }

      if (!date) {
        return;
      }

      // each game unique date
      console.log('-------- now must calculate precise date for game, hour :', a.childNodes[0].childNodes[0].rawText);
      const dateWithHour = `${date}T${a.childNodes[0].childNodes[0].rawText}`;
      console.log('dateWithHour', dateWithHour)
      const gameDate = new Date(new Date(dateWithHour).getTime() + HOURS_OFFSET_PARIS * 60 * 60 * 1000).toISOString().slice(0,10)
      console.log('gameDate', gameDate);
      /* const gameDate = parseDate(
        date,
        parseInt(a.childNodes[0].childNodes[0].rawText.split(':')[0]), 
        HOURS_OFFSET_PARIS,
        SEASON,
        YEAR_BASED_ON_MONTH
      ); 
      console.log('gameDate', gameDate)
      if (gameDate === "ignore") {
        return;
      }*/

      const tryToGetText = (t) => {
        if (t.rawText) {
          return t.rawText
        } else {
          return t.childNodes[0].rawText;
        }
      }

      const teams = tryToGetText(a.childNodes[1].childNodes[0])
      let team1 = teams.split(' - ')[0];
      if (!teamsCompleteName[team1]) {
        if (team1 === "Team LeBron") return;
        if (team1 === "Team Durant") return;
        throw new Error('Unknown team ' + team1)
        process.exit(1)
      }
      team1 = teamsCompleteName[team1];
      let team2 = teams.split(' - ')[1];
      if (!teamsCompleteName[team2]) {
        if (team2 === "Team LeBron") return;
        if (team2 === "Team Durant") return;
        throw new Error('Unknown team ' + team2)
        process.exit(1)
      }
      team2 = teamsCompleteName[team2];
      // postponed
      if (a.childNodes[2].childNodes[0].rawText === 'canc.') {
        console.log('skip canceled');
        return;
      }
      if (a.childNodes[2].childNodes[0].rawText === 'postp.') {
        console.log('skip postponed');
        return;
      }
      if (a.childNodes[2].childNodes[0].rawText.includes('abn')) {
        console.log('skip abandonned');
        return;
      }
      if (a.childNodes[2].childNodes[0].rawText.includes('award')) {
        console.log('skip award');
        return;
      }
      const score = a.childNodes[2].childNodes[0].rawText.split(':').map(a => parseInt(a, 10));
      score.forEach(s => {
        if (!findArg('--nba') && findArg('--nfl')) {
          if (s < 0 || s > 12) {
            throw new Error('Invalid score ' + s);
            process.exit(1);
          }
        }
      })
      const odd1 = parseFloat(a.childNodes[3].childNodes[0].rawText);
      if (typeof odd1 !== 'number' || odd1 < 1 || odd1 > 40) {
        throw new Error('Error with odd 1 ' + odd1);
        process.exit(1)
      }
      let oddDraw;
      let odd2;
      if (findArg('--nba') || findArg('--nfl')) {
        odd2 = parseFloat(a.childNodes[4].childNodes[0].rawText);
        if (odd2 < 1 || odd2 > 40) {
          throw new Error('Error with oddDraw ' + oddDraw);
          process.exit(1)
        }
        console.log(`${gameDate} - ${team1} (odd ${odd1}) - ${team2} (odd ${odd2})`)
      } else {
        oddDraw = parseFloat(a.childNodes[4].childNodes[0].rawText);
        if (typeof oddDraw !== 'number' || oddDraw < 1 || oddDraw > 40) {
          throw new Error('Error with oddDraw ' + oddDraw);
          process.exit(1)
        }
        odd2 = parseFloat(a.childNodes[5].childNodes[0].rawText);
        if (typeof odd2 !== 'number' || odd2 < 1 || odd2 > 80) {
          throw new Error('Error with odd2 ' + odd2);
          process.exit(1)
        }
        console.log(`${gameDate} - ${team1} (odd ${odd1}) - (draw ${oddDraw}) - ${team2} (odd ${odd2})`)
      }
      // bug with this game
      if (gameDate === "2022-03-18" && team1 === "Boch") {
        return;
      }
      if (!games[team1] && findArg('--newgames')) {
        games[team1] = {}
      }
      if (!games[team1][gameDate]) {
        if (findArg('--newgames')) {
          games[team1][gameDate] = {
            date: gameDate,
            op: team2,
            score: score,
            home: true,
            victory: score[0] > score[1]
          }
        } else {
          throw new Error(' Game not found ' + gameDate + team1 + ' ' + team2)
        }
      }
      let oddsTeam1;
      if (findArg('--nba') || findArg('--nfl')) {
        oddsTeam1 = [odd1,odd2]
      } else {
        oddsTeam1 = [odd1,odd2,oddDraw]
      }
      games[team1][gameDate] = {
        ...games[team1][gameDate],
        odds: {
          ...games[team1][gameDate].odds,
          oddsportal: oddsTeam1
        }
      }
      if (!games[team2] && findArg('--newgames')) {
        games[team2] = {}
      }
      if (!games[team2][gameDate]) {
        if (findArg('--newgames')) {
          games[team2][gameDate] = {
            date: gameDate,
            op: team1,
            score: [score[1], score[0]],
            home: false,
            victory: score[0] < score[1]
          }
        } else {
          throw new Error(' Game not found ' + gameDate + team1 + ' ' + team2)
        }
      }
      let oddsTeam2;
      if (findArg('--nba') || findArg('--nfl')) {
        oddsTeam2 = [odd2,odd1]
      } else {
        oddsTeam2 = [odd2,odd1,oddDraw]
      }
      games[team2][gameDate] = {
        ...games[team2][gameDate],
        odds: {
          ...games[team2][gameDate].odds,
          oddsportal: oddsTeam2
        }
      }
      j += 1;
    });

    console.log('end of page ' + i);
    await browser.close();
    i += 1
    go();

    return;
  }
}
go();