const fs = require("fs");

const { getProcessArgv, getSingleProcessArgv } = require("./utils");

const daysToRun = parseInt(getProcessArgv("--days"));
const season = getProcessArgv("--season");

let config = getProcessArgv("--config");
if (config) {
  config = JSON.parse(decodeURIComponent(getProcessArgv("--config")));
} else {
  config = null;
}

let defaultOdd = getProcessArgv("--default-odd");
if (defaultOdd) {
  defaultOdd = parseFloat(defaultOdd, 10);
} else {
  defaultOdd = null;
}

let run;
let bestConfig;
let getStartDate;
let games;
let league;
if (getSingleProcessArgv("--nfl")) {
  run = require("./run").run;
  league = 'nfl';
} else if (getSingleProcessArgv("--nba")) {
  run = require("./run").run;
  league = 'nba';
} else if (getSingleProcessArgv("--ncaa")) {
  run = require("./run").run;
  league = 'ncaa';
} else if (getSingleProcessArgv("--ligue1")) {
  run = require('./runSoccer.js').run;
  league = 'ligue1';
} else if (getSingleProcessArgv("--bundesliga")) {
  run = require('./runSoccer.js').run;
  league = 'bundesliga';
} else if (getSingleProcessArgv("--bundesliga2")) {
  run = require('./runSoccer.js').run;
  league = 'bundesliga2';
} else if (getSingleProcessArgv("--seriea")) {
  run = require('./runSoccer.js').run;
  league = 'seriea';
} else if (getSingleProcessArgv("--serieb")) {
  run = require('./runSoccer.js').run;
  league = 'serieb';
} else if (getSingleProcessArgv("--superlig")) {
  run = require('./runSoccer.js').run;
  league = 'superlig';
} else if (getSingleProcessArgv("--liga")) {
  run = require('./runSoccer.js').run;
  league = 'liga';
} else if (getSingleProcessArgv("--premiere")) {
  run = require('./runSoccer.js').run;
  league = 'premiere';
} else if (getSingleProcessArgv("--championship")) {
  run = require('./runSoccer.js').run;
  league = 'championship';
} else if (getSingleProcessArgv("--eredivisie")) {
  run = require('./runSoccer.js').run;
  league = 'eredivisie';
} else if (getSingleProcessArgv("--primeira")) {
  run = require('./runSoccer.js').run;
  league = 'primeira';
} else if (getSingleProcessArgv("--oneliga")) {
  run = require('./runSoccer.js').run;
  league = 'oneliga';
} else if (getSingleProcessArgv("--superliga")) {
  run = require('./runSoccer.js').run;
  league = 'superliga';
} else if (getSingleProcessArgv("--jupiler")) {
  run = require('./runSoccer.js').run;
  league = 'jupiler';
} else if (getSingleProcessArgv("--botola")) {
  run = require('./runSoccer.js').run;
  league = 'botola';
} else if (getSingleProcessArgv("--leagueone")) {
  run = require('./runSoccer.js').run;
  league = 'leagueone';
} else if (getSingleProcessArgv("--leaguetwo")) {
  run = require('./runSoccer.js').run;
  league = 'leaguetwo';
} else if (getSingleProcessArgv("--north")) {
  run = require('./runSoccer.js').run;
  league = 'north';
} else if (getSingleProcessArgv("--south")) {
  run = require('./runSoccer.js').run;
  league = 'south';
} else {
  throw new Error("no competition");
}
if (config) {
  bestConfig = JSON.parse(config);
} else {
  bestConfig = require(`./${league}/env.js`).bestConfig;
  if (bestConfig[season]) {
    bestConfig = bestConfig[season];
  }

}
getStartDate = require(`./${league}/env.js`).getStartDate;
games = fs.readFileSync(`./${league}/games-wh-${season}.json`, "utf8");
games = JSON.parse(games);

//NFL
/* const pointsForVictory = 58;
const pointsForDefeat = 18;
const betIfScoreSuperiorTo = 22;
const goalDiffFactor = 2;
const factorM1 = 4.1;
const factorM2 = 2.3;
const factorM3 = 2.5; */

// KHL
/* const pointsForVictory = 60;
const pointsForDefeat = 10;
const betIfScoreSuperiorTo = 200;
const goalDiffFactor = 16;
const factorM1 = 1;
const factorM2 = 1;
const factorM3 = 1; */

const a = new Date().getTime();
const r = run(
  games,
  null,
  bestConfig.pointsForVictory,
  bestConfig.pointsForDefeat,
  bestConfig.betIfScoreSuperiorTo,
  bestConfig.betIfOddSuperiorTo,
  bestConfig.pointsPerGoalScored,
  bestConfig.pointsPerGoalTaken,
  bestConfig.factorM1,
  bestConfig.factorM2,
  bestConfig.factorM3,
  bestConfig.victoriesRateBonus,
  bestConfig.homeBonus,
  bestConfig.betOnDraw,
  season,
  getStartDate(season),
  daysToRun,
  false
);

console.log(r.output);
return;
console.log(r.results)
console.log(Math.round(100 * (new Date().getTime() - a)) / 100, 'ms')
