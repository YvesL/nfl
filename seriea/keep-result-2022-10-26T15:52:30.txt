season(s): 2021,2022, sport: --seriea
Finished 2022-10-26T15:52:29.526Z in 9.731minutes
Seasons 2021,2022
Ran 139968 simulations (total) across 4x2 processes

=== SORT BY NET:

Winner config: 9/15/70/1.4/2/3/0/0/75/60/false
Net results : 
2021 : 24.77x
2022 : -6.8x
Full config and results 2021
{
  "pointsForVictory": 9,
  "pointsForDefeat": 15,
  "betIfScoreSuperiorTo": 70,
  "betIfOddSuperiorTo": 1.4,
  "goalDiffFactor": 2,
  "factorM1": 3,
  "factorM2": 0,
  "factorM3": 0,
  "victoriesRateBonus": 75,
  "homeBonus": 60,
  "betOnDraw": "false",
  "results": {
    "net": 24.77,
    "wins": 62,
    "winsNet": 141.77,
    "looses": 117,
    "rate": 0.35
  }
}
Top 10 :
9/15/70/1.4/2/3/0/0/75/60/false: net = 2021:24.77x 2022:-6.8x avg:8.99x, wins: 2021:62    2022:24    avg:43, looses: 2021:117   2022:42    avg:79.5
9/15/70/1.1/2/3/0/0/75/60/false: net = 2021:24.26x 2022:-6.8x avg:8.73x, wins: 2021:70    2022:24    avg:47, looses: 2021:120   2022:42    avg:81
40/9/110/1.4/4/3/0/0/55/60/false: net = 2021:21.2x 2022:-4.18x avg:8.51x, wins: 2021:55    2022:22    avg:38.5, looses: 2021:92    2022:36    avg:64
40/9/110/1.1/4/3/0/0/55/60/false: net = 2021:21.17x 2022:-4.18x avg:8.5x, wins: 2021:62    2022:22    avg:42, looses: 2021:94    2022:36    avg:65
40/9/110/1.8/4/3/0/0/55/60/false: net = 2021:20.66x 2022:-5.33x avg:7.67x, wins: 2021:44    2022:16    avg:30, looses: 2021:86    2022:34    avg:60
40/9/110/1.4/3/3/0/0/55/60/false: net = 2021:19.2x 2022:-4.18x avg:7.51x, wins: 2021:55    2022:22    avg:38.5, looses: 2021:94    2022:36    avg:65
40/6/110/1.4/2/3/0/0/55/60/false: net = 2021:20.2x 2022:-5.18x avg:7.51x, wins: 2021:55    2022:22    avg:38.5, looses: 2021:93    2022:37    avg:65
40/9/110/1.1/3/3/0/0/55/60/false: net = 2021:19.17x 2022:-4.18x avg:7.5x, wins: 2021:62    2022:22    avg:42, looses: 2021:96    2022:36    avg:66
40/6/110/1.1/2/3/0/0/55/60/false: net = 2021:20.17x 2022:-5.18x avg:7.5x, wins: 2021:62    2022:22    avg:42, looses: 2021:95    2022:37    avg:66
16/6/110/1.1/4/3/0/0/55/60/false: net = 2021:17.27x 2022:-3.18x avg:7.05x, wins: 2021:61    2022:22    avg:41.5, looses: 2021:98    2022:35    avg:66.5


=== SORT BY RATE (wins/looses):

Winner config (rate wins/looses): 16/9/180/1.1/3/2/1/1/55/24/false
Full config and results 2021
{
  "pointsForVictory": 16,
  "pointsForDefeat": 9,
  "betIfScoreSuperiorTo": 180,
  "betIfOddSuperiorTo": 1.1,
  "goalDiffFactor": 3,
  "factorM1": 2,
  "factorM2": 1,
  "factorM3": 1,
  "victoriesRateBonus": 55,
  "homeBonus": 24,
  "betOnDraw": "false",
  "results": {
    "net": -0.37,
    "wins": 9,
    "winsNet": 3.63,
    "looses": 4,
    "rate": 0.69
  }
}
Top 10 :
16/9/180/1.1/3/2/1/1/55/24/false: rate = 2021:0.69  2022:0.9   avg:0.8, wins: 2021:9     2022:9     avg:9, looses: 2021:4     2022:1     avg:2.5
16/6/180/1.1/2/2/1/1/55/24/false: rate = 2021:0.69  2022:0.9   avg:0.8, wins: 2021:9     2022:9     avg:9, looses: 2021:4     2022:1     avg:2.5
9/6/180/1.1/4/2/1/1/55/24/false: rate = 2021:0.69  2022:0.9   avg:0.8, wins: 2021:9     2022:9     avg:9, looses: 2021:4     2022:1     avg:2.5
32/15/160/1.1/4/2/1/0/55/24/false: rate = 2021:0.7   2022:0.83  avg:0.76, wins: 2021:7     2022:10    avg:8.5, looses: 2021:3     2022:2     avg:2.5
32/9/160/1.1/3/2/1/0/55/24/false: rate = 2021:0.7   2022:0.83  avg:0.76, wins: 2021:7     2022:10    avg:8.5, looses: 2021:3     2022:2     avg:2.5
32/6/180/1.1/4/2/1/0/55/24/false: rate = 2021:0.7   2022:0.82  avg:0.76, wins: 2021:7     2022:9     avg:8, looses: 2021:3     2022:2     avg:2.5
32/6/160/1.1/2/2/1/0/55/24/false: rate = 2021:0.7   2022:0.8   avg:0.75, wins: 2021:7     2022:8     avg:7.5, looses: 2021:3     2022:2     avg:2.5
40/15/160/1.1/2/2/1/0/55/24/false: rate = 2021:0.7   2022:0.8   avg:0.75, wins: 2021:7     2022:8     avg:7.5, looses: 2021:3     2022:2     avg:2.5
40/9/180/1.1/4/1/1/1/55/24/false: rate = 2021:0.73  2022:0.77  avg:0.75, wins: 2021:8     2022:10    avg:9, looses: 2021:3     2022:3     avg:3
16/6/140/1.1/4/2/1/0/55/24/false: rate = 2021:0.64  2022:0.86  avg:0.75, wins: 2021:9     2022:12    avg:10.5, looses: 2021:5     2022:2     avg:3.5

