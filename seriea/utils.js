module.exports.getProcessArgv = (param) => {
  const index = process.argv.findIndex((arg) => arg === param);
  if (index === -1) {
    return undefined;
  }

  return process.argv[index + 1];
};

module.exports.rightPad = (str, length) => {
  let s = str;
  for (let i = 0; i <= length - str.length; i += 1) {
    s += " ";
  }
  return s;
};

module.exports.IGNORE_OP = {
  'all': [],
};

module.exports.teamsCompleteName = {
  "Naples": "Naples",
  "SSC Napoli": "Naples",
  "Napoli": "Naples",
  "Benevento": "Benevento",
  "Sampdoria": "Sampdoria",
  "UC Sampdoria": "Sampdoria",
  "Milan": "Milan AC",
  "AC Milan": "Milan AC",
  "Spezia": "Spezia",
  "Udinese": "Udinese",
  "Udinese Calcio": "Udinese Calcio",
  "Brescia Calcio": "Brescia Calcio",
  "Sassuolo": "Sassuolo",
  "US Sassuolo Calcio": "Sassuolo",
  "Lecce": "Lecce",
  "US Lecce": "Lecce",
  "FC Crotone": "Crotone",
  "Monza": "Monza",
  "Monza": "Monza",
  "Juventus": "Juventus",
  "Salernitana": "Salernitana",
  "Atalanta": "Atalanta",
  "Atalanta BC": "Atalanta",
  "Atalanta Bergamo": "Atalanta",
  "Inter": "Inter",
  "FC Internazionale Milano": "Inter",
  "Torino": "Torino",
  "Parma": "Parma",
  "Frosinone": "Frosinone",
  "Torino FC": "Torino",
  "SPAL Ferrara": "Ferrara",
  "Fiorentina": "Fiorentina",
  "ACF Fiorentina": "Fiorentina",
  "Spezia": "Spezia",
  "Spezia": "Spezia",
  "Cremonese": "Cremonese",
  "Cremoneze": "Cremonese",
  "Empoli": "Empoli",
  "Venezia": "Venise",
  "Vérone": "Vérone",
  "Verona": "Vérone",
  "Hellas Verona": "Vérone",
  "Hellas Verona": "Vérone",
  "Hellas Verona FC": "Vérone",
  "Roma": "Roma",
  "AS Roma": "Roma",
  "Bologne": "Bologne",
  "Bologna": "Bologne",
  "Genoa": "Genes",
  "Genoa CFC": "Genes",
  "Bologna FC": "Bologne",
  "Lazio": "Lazio",
  "SS Lazio": "Lazio",
  "Cagliari": "Cagliari",
  "Cagliari Calcio": "Cagliari",
  "AC Chievo Verona": "AC Chievo Verona"
};
