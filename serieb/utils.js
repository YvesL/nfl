module.exports.getProcessArgv = (param) => {
  const index = process.argv.findIndex((arg) => arg === param);
  if (index === -1) {
    return undefined;
  }

  return process.argv[index + 1];
};

module.exports.rightPad = (str, length) => {
  let s = str;
  for (let i = 0; i <= length - str.length; i += 1) {
    s += " ";
  }
  return s;
};

module.exports.IGNORE_OP = {
  'all': [],
};

module.exports.teamsCompleteName = {
  "Perugia Calcio Spa": "Perugia",
  "Perugia": "Perugia",
  "Pérouse": "Perugia",
  "Parma": "Parma",
  "Parme": "Parma",
  "L.R. Vicenza": "Vicence",
  "L.R. Vicenza Virtus": "Vicence",
  "Monza": "Monza",
  "Spal": "Ferrara",
  "SPAL": "Ferrara",
  "SPAL Ferrara": "Ferrara",
  "Benevento": "Benevento",
  "Ascoli": "Ascoli",
  "Frosinone": "Frosinone",
  "Crotone": "Crotone",
  "FC Crotone": "Crotone",
  "Cosenza": "Nuova Cosenza",
  "Nuova Cosenza": "Nuova Cosenza",
  "Ascoli": "Ascoli",
  "Cittadella": "Cittadella",
  "Reggina": "Reggina",
  "Venezia": "Venise",
  "Ternana": "Ternana",
  "Ternana Calcio": "Ternana",
  "Como Calcio": "Como",
  "Como": "Como",
  "Côme": "Como",
  "Nueva Cosenza": "Nueva Cosenza",
  "Brescia": "Brescia",
  "Pise": "Pisa",
  "Pisa": "Pisa",
  "Alessandria": "Alessandria",
  "Genoa": "Genes",
  "Genoa CFC": "Genes",
  "Lecce": "Lecce",
  "US Lecce": "Lecce",
  "Pordenone": "Pordenone",
  "Cremonese": "Cremonese",
  "Cremoneze": "Cremonese",
  "Pordenone Calcio": "Pordenone",
  "Modène": "Modena",
  "Modena": "Modena",
  "Bari": "Bari",
  "Sudtirol": "Sudtirol",
  "Palermo": "Palermo",
  "Palerme": "Palermo",
  "Cagliari": "Cagliari",
  "Cagliari Calcio": "Cagliari",
};
