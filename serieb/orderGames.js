const fs = require("fs");

const getProcessArgv = require("./utils").getProcessArgv;

const SEASONS = getProcessArgv("--seasons");

SEASONS.split(',').forEach(season => {
  const games = JSON.parse(
    fs.readFileSync(__dirname + "/games-wh-" + season + ".json", "utf8")
  );

  Object.keys(games).forEach((team) => {
    games[team] = Object.keys(games[team]).sort().reduce(
      (obj, key) => { 
        obj[key] = games[team][key]; 
        return obj;
      }, 
      {}
    );
  });
  Object.keys(games).forEach((team) => {
    Object.keys(games[team]).forEach((date, index) => {
      games[team][date].wk = index;
    })
  });
  
  fs.writeFileSync(
    __dirname + "/games-wh-" + season + ".json",
    JSON.stringify(games, null, 2),
    "utf8"
  );
});
