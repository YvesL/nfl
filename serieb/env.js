module.exports.getStartDate = (season) => {
  if (season === "2020") return '2020-09-12';
  if (season === "2021") return '2021-08-10';
  if (season === "2022") return '2022-08-10';
  return `${season}-08-10`;
}

const POINTS_FOR_VICTORY = [0, 8];
module.exports.POINTS_FOR_VICTORY = POINTS_FOR_VICTORY;

// How many points for a defeat ? (matches n-1, n-2 and n-3 are taken into consideration)
const POINTS_FOR_DEFEAT = [0, 8, 16, 24];
//const POINTS_FOR_DEFEAT = [5];
module.exports.POINTS_FOR_DEFEAT = POINTS_FOR_DEFEAT;

// Only bet if a score reaches a minimum
const BET_IF_SCORE_SUPERIOR_TO = [0, 10, 15, 20, 25];
//const BET_IF_SCORE_SUPERIOR_TO = [22, 24];
module.exports.BET_IF_SCORE_SUPERIOR_TO = BET_IF_SCORE_SUPERIOR_TO;

// Only bet if a score reaches a minimum
const BET_IF_ODD_SUPERIOR_TO = [1.3, 1.6, 1.8, 2, 2.2, 2.6];
//const BET_IF_ODD_SUPERIOR_TO = [1.3];
module.exports.BET_IF_ODD_SUPERIOR_TO = BET_IF_ODD_SUPERIOR_TO;

const POINTS_PER_GOAL_SCORED = [-8, -4, 0, 4, 8];
module.exports.POINTS_PER_GOAL_SCORED = POINTS_PER_GOAL_SCORED;

const POINTS_PER_GOAL_TAKEN = [-20, -16, -8, -4, 0, 4, 8];
module.exports.POINTS_PER_GOAL_TAKEN = POINTS_PER_GOAL_TAKEN;

// Weight for match n-1, n-2, and n-3
const FACTOR_M1 = [1, 2, 3];
//const FACTOR_M1 = [4.1, 4.2];
module.exports.FACTOR_M1 = FACTOR_M1;

const FACTOR_M2 = [0, 1];
//const FACTOR_M2 = [1.2];
module.exports.FACTOR_M2 = FACTOR_M2;

const FACTOR_M3 = [0, 1];
//const FACTOR_M3 = [1.0];
module.exports.FACTOR_M3 = FACTOR_M3;

const VICTORIES_RATE_BONUS = [0];
//const VICTORIES_RATE_BONUS = [1.0];
module.exports.VICTORIES_RATE_BONUS = VICTORIES_RATE_BONUS;

const HOME_BONUS = [0];
//const HOME_BONUS = [1.0, 1.2, 1.6];
module.exports.HOME_BONUS = HOME_BONUS;

const BET_ON_DRAW = [false];
//const BET_ON_DRAW = [true];
module.exports.BET_ON_DRAW = BET_ON_DRAW;

module.exports.getNumSimulationsPerProcess = () => {
  /*
    Do not count POINTS_FOR_VICTORY because there is
    one process per season per POINTS_FOR_VICTORY
  */
  return  POINTS_FOR_DEFEAT.length *
  BET_IF_SCORE_SUPERIOR_TO.length *
  BET_IF_ODD_SUPERIOR_TO.length *
  POINTS_PER_GOAL_SCORED.length *
  POINTS_PER_GOAL_TAKEN.length *
  FACTOR_M1.length *
  FACTOR_M2.length *
  FACTOR_M3.length *
  VICTORIES_RATE_BONUS.length *
  HOME_BONUS.length *
  BET_ON_DRAW.length;
}

module.exports.bestConfig = {
  "pointsForVictory": 8,
  "pointsForDefeat": 0,
  "betIfScoreSuperiorTo": 10,
  "betIfOddSuperiorTo": 2.2,
  "pointsPerGoalScored": 0,
  "pointsPerGoalTaken": 0,
  "factorM1": 3,
  "factorM2": 0,
  "factorM3": 0,
  "victoriesRateBonus": 0,
  "homeBonus": 0,
  "betOnDraw": false,
  // 2018 70/12
  // 2019 53/3
  // 202211/1 (at 2022-10-16)
  /* "pointsForVictory": 10,
  "pointsForDefeat": 12,
  "betIfScoreSuperiorTo": 124,
  "betIfOddSuperiorTo": 1.3,
  "goalDiffFactor": 8,
  "factorM1": 1,
  "factorM2": 1,
  "factorM3": 1,
  "victoriesRateBonus": 100,
  "homeBonus": 16,
  doNotBetOnTeams: {} */
  // 2018 18/3
  // 2019 21/5
  /* "pointsForVictory": 20,
  "pointsForDefeat": 9,
  "betIfScoreSuperiorTo": 90,
  "betIfOddSuperiorTo": 1.1,
  "goalDiffFactor": 12,
  "factorM1": 1,
  "factorM2": 0,
  "factorM3": 2,
  "victoriesRateBonus": 1,
  "homeBonus": 0, 
  doNotBetOnTeams: {
    "Naples": true,
    "Juventus": true,
    "Milan AC": true,
    "Inter": true,
    "Atalanta": true
  }*/
  // 2018 17/3
  // 2019 30/1
  /* "pointsForVictory": 15,
  "pointsForDefeat": 3,
  "betIfScoreSuperiorTo": 70,
  "betIfOddSuperiorTo": 1.1,
  "goalDiffFactor": 4,
  "factorM1": 2,
  "factorM2": 0,
  "factorM3": 2,
  "victoriesRateBonus": 1,
  doNotBetOnTeams: {
    "Naples": true,
    "Juventus": true,
  } */
  // 2018 30/2
  // 2019 24/1
  /* "pointsForVictory": 15,
  "pointsForDefeat": 6,
  "betIfScoreSuperiorTo": 90,
  "betIfOddSuperiorTo": 1.1,
  "goalDiffFactor": 1,
  "factorM1": 3,
  "factorM2": 0,
  "factorM3": 2,
  "victoriesRateBonus": 2,
  "doNotBetOnTeams": {} */
};
