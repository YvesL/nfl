const fs = require("fs");

const { rightPad, getProcessArgv, getSingleProcessArgv, fromId } = require("./utils");
const { exec } = require("child_process");

const noCache = getSingleProcessArgv("--no-cache");
const onlyOutputNet = getSingleProcessArgv("--net");
const onlyOutputNetPerBet = getSingleProcessArgv("--net-per-bet");
const onlyOutputRate = getSingleProcessArgv("--rate");
let log = true;
if (onlyOutputRate || onlyOutputNet || onlyOutputNetPerBet) {
  log = false;
}
const MIN_N_BETS = 40;
let MAX_N_BETS = 200;
// ignore this season for counting MIN_N_BETS
const INCOMPLETE_SEASON = 2023;

// seconds per simulation
const PER_SIMULATION_TIME = 120;


let FOLDER;
let POINTS_FOR_VICTORY;
let numSimulationsPerProcess;
let COMPETITION_ARG;
let league;
if (getSingleProcessArgv("--nfl")) {
  MAX_N_BETS = 400;
  league = 'nfl';
} else if (getSingleProcessArgv("--nba")) {
  MAX_N_BETS = 400;
  league = 'nba'
} else if (getSingleProcessArgv("--ncaa")) {
  MAX_N_BETS = 400;
  league = 'ncaa'
} else if (getSingleProcessArgv("--khl")) {
  league = 'khl'
} else if (getSingleProcessArgv("--ligue1")) {
  league = 'ligue1'
} else if (getSingleProcessArgv("--bundesliga")) {
  league = 'bundesliga'
} else if (getSingleProcessArgv("--bundesliga2")) {
  league = 'bundesliga2'
} else if (getSingleProcessArgv("--seriea")) {
  league = 'seriea'
} else if (getSingleProcessArgv("--serieb")) {
  league = 'serieb'
} else if (getSingleProcessArgv("--superlig")) {
  league = 'superlig'
} else if (getSingleProcessArgv("--liga")) {
  league = 'liga'
} else if (getSingleProcessArgv("--premiere")) {
  league = 'premiere'
} else if (getSingleProcessArgv("--championship")) {
  league = 'championship'
} else if (getSingleProcessArgv("--eredivisie")) {
  league = 'eredivisie'
} else if (getSingleProcessArgv("--primeira")) {
  league = 'primeira'
} else if (getSingleProcessArgv("--oneliga")) {
  league = 'oneliga'
} else if (getSingleProcessArgv("--superliga")) {
  league = 'superliga'
} else if (getSingleProcessArgv("--jupiler")) {
  league = 'jupiler'
} else if (getSingleProcessArgv("--botola")) {
  league = 'botola'
} else if (getSingleProcessArgv("--leagueone")) {
  league = 'leagueone'
} else if (getSingleProcessArgv("--leaguetwo")) {
  league = 'leaguetwo'
} else if (getSingleProcessArgv("--north")) {
  league = 'north'
} else if (getSingleProcessArgv("--south")) {
  league = 'south'
} else {
  throw new Error("no competition");
}

FOLDER = `/${league}`;
COMPETITION_ARG = `--${league}`;
POINTS_FOR_VICTORY = require(`./${league}/env.js`).POINTS_FOR_VICTORY;
numSimulationsPerProcess = require(`./${league}/env.js`).getNumSimulationsPerProcess()

let defaultOdd = getProcessArgv("--default-odd");
if (defaultOdd) {
  defaultOdd = parseFloat(defaultOdd, 10);
} else {
  defaultOdd = null;
}

// --days over 200 is useless
// a season is
// NBA: october-april or december-june for COVID
// NFL: september-february

const daysToRun = getProcessArgv("--days");
const seasons = getProcessArgv("--seasons")
  .split(",")
  .map((a) => parseInt(a));
if (log) {
  console.log(new Date().toString());
  console.log(`${seasons.length} season(s) ${seasons.join(", ")}`);
  console.log("Will search bets on a " + daysToRun + " days period");
  const time = numSimulationsPerProcess * PER_SIMULATION_TIME / (60000);
  console.log("Approx. time peer process (min) :" + Math.round(time * 100) / 100+ ", simulations per processes : " + numSimulationsPerProcess);
  console.log("Number of parallel processes (total for all seasons)     :" + (POINTS_FOR_VICTORY.length * seasons.length));
}

const startTime = new Date().getTime();
let matrix = {};

let seasonsOver = 0;
for (let h = 0; h < seasons.length; h += 1) {
  let childProcesses = 0;
  const season = seasons[h];
  matrix[season] = {};
  if (log) console.log(`Season ${season}, launching ${POINTS_FOR_VICTORY.length} child processes`);
  for (let i = 0; i < POINTS_FOR_VICTORY.length; i += 1) {
    const pointsForVictory = POINTS_FOR_VICTORY[i];
    const script = `node predictForSeasonChildProcess.js --childProcess ${i} --daysToRun ${daysToRun} --season ${season} --pointsForVictory ${pointsForVictory} ${
      defaultOdd ? "--default-odd " + defaultOdd : ""
    } ${noCache ? "--no-cache" : ""} ${COMPETITION_ARG}`;
    if (log) console.log(script);
    exec(
      script,
      { maxBuffer: 1024 * 1000 * 4 },
      function (error, stdout, stderr) {
        if (error) {
          console.log(error);
          console.log(stderr);
          throw new Error("error in child process");
        }
        childProcesses += 1;
        let resultFromChildProcess = fs.readFileSync(
          `./tmp/result-${league}-${season}-${i}.json`,
          "utf8"
        );
        resultFromChildProcess = JSON.parse(resultFromChildProcess);
        if (log) console.log(`process ${season}/${i} over, ${resultFromChildProcess.fromMemory} from memory, ${numSimulationsPerProcess - resultFromChildProcess.fromMemory} processed`);
        matrix[season] = {
          ...matrix[season],
          ...resultFromChildProcess.matrix[season],
        };
        if (childProcesses === POINTS_FOR_VICTORY.length) {
          if (seasonsOver === seasons.length - 1) {
            if (log) console.log("all child processes ended");
            end();
          } else {
            seasonsOver += 1;
          }
        }
      }
    );
  }
}

const add = (accumulator, a) => {
  return accumulator + a;
};

const allSeasons = {
  net: {},
  netPerBet: {},
  rate: {},
};
const end = async () => {
  const crossSeasonFor = (func, season) => {
    const r = {};
    const keys = Object.keys(matrix[season]);
    /* if (keys.length < 5) {
      console.log(keys);
      throw new Error(
        "Too few results (" + keys.length + " for season " + season + ")"
      );
    } */
    keys.forEach((id) => {
      r[id] = func(matrix[season][id].results);
    });

    return r;
  };

  const endTime = new Date().getTime();
  let s = `season(s): ${seasons.join(",")}, sport: ${COMPETITION_ARG}\n`;
  s +=
    "Finished " +
    new Date().toISOString() +
    " in " +
    Math.round((1000 * (endTime - startTime)) / 60000) / 1000 +
    "minutes" +
    "\n";

  s += "Seasons " + seasons.join(",") + "\n";

  s += `Ran ${seasons.length * POINTS_FOR_VICTORY.length * numSimulationsPerProcess} simulations (total) across ${POINTS_FOR_VICTORY.length}x${seasons.length} processes\n`;

  const logKeyAllSeasons = (id, key) => {
    let i = 0;
    return (
      seasons
        .map((season) => {
          const a =
            Math.round(matrix[season][id].results[key] * 100) / 100;
          i += a;
          return `${season}:${rightPad(
            "" + a + (key === "net" ? "x" : ""),
            4
          )}`;
        })
        .join(" ") +
      ` avg:${Math.round(((i === 0 ? 1 : i) * 100) / seasons.length) / 100}${
        key === "net" ? "x" : ""
      }`
    );
  };

  s += "\n=== SORT BY NET:\n";

  seasons.forEach((season) => {
    const crossSeasonsByNetPerBet = crossSeasonFor((a) => {
      return a.netPerBet;
    }, season);
    Object.keys(crossSeasonsByNetPerBet).forEach((a) => {
      if (allSeasons.netPerBet[a]) {
        allSeasons.netPerBet[a] = allSeasons.netPerBet[a] + crossSeasonsByNetPerBet[a];
      } else {
        allSeasons.netPerBet[a] = crossSeasonsByNetPerBet[a];
      }
    });

    const crossSeasonsByNet = crossSeasonFor((a) => {
      // We want to avoid that 2021:40x and 2021:-10x 
      // ends up averaging on 15x
      if (a.net < 5) {
        return (a.net - 5)
      } else {
        return a.net
      }
    }, season);

    Object.keys(crossSeasonsByNet).forEach((a) => {
      if (allSeasons.net[a]) {
        allSeasons.net[a] = allSeasons.net[a] + crossSeasonsByNet[a];
      } else {
        allSeasons.net[a] = crossSeasonsByNet[a];
      }
    });

    const crossSeasonsByRate = crossSeasonFor(
      (a) => (a.rate === Infinity ? 1 : a.rate),
      season
    );

    Object.keys(crossSeasonsByRate).forEach((a) => {
      if (!!allSeasons.rate[a]) {
        allSeasons.rate[a] = allSeasons.rate[a].concat(crossSeasonsByRate[a]);
      } else {
        allSeasons.rate[a] = [crossSeasonsByRate[a]];
      }
    });
  });

  Object.keys(allSeasons.rate).forEach((a) => {
    const l = allSeasons.rate[a].length;
    allSeasons.rate[a] = allSeasons.rate[a].reduce(add, 0) / l;
  });

  /*
    Will drop some IDs if they do not have enough bets
    see MIN_N_BETS and MAX_N_BETS constants
  */
  Object.keys(allSeasons.net).forEach((id) => {
    try {
      (seasons.filter(s => s !== INCOMPLETE_SEASON)).forEach((season) => {
        if (matrix[season][id]) {
          if (
            matrix[season][id].results.wins + matrix[season][id].results.looses < MIN_N_BETS
          ) {
            delete allSeasons.net[id];
            delete allSeasons.netPerBet[id];
            delete allSeasons.rate[id];
          } else if (
            matrix[season][id].results.wins + matrix[season][id].results.looses > MAX_N_BETS
          ) {
            delete allSeasons.net[id];
            delete allSeasons.netPerBet[id];
            delete allSeasons.rate[id];
          }
        } else {
          delete allSeasons.net[id];
          delete allSeasons.netPerBet[id];
          delete allSeasons.rate[id];
          s += 'CRITICAL did not find ' + id + ' for season ' + season
        }
      });
    } catch (err) {
      console.log(id, !!matrix[seasons[0]][id], !!matrix[seasons[1]][id])
      throw err
    }
  });

  const top20CrossSeasonsByNet = Object.keys(allSeasons.net)
    .sort((id1, id2) => {
      return allSeasons.net[id1] - allSeasons.net[id2];
    })
    .reverse();

  const top20CrossSeasonsByRate = Object.keys(allSeasons.rate)
    .sort((id1, id2) => {
      return allSeasons.rate[id1] - allSeasons.rate[id2];
    })
    .reverse();

  const top20CrossSeasonsByNetPerBet = Object.keys(allSeasons.netPerBet)
    .sort((id1, id2) => {
      return allSeasons.netPerBet[id1] - allSeasons.netPerBet[id2];
    })
    .reverse();

  const padEarchLog = seasons.length * 8 + 10;

  const line = (id) => {
    return `${rightPad(id, 28)}: net = ${rightPad(
      `${logKeyAllSeasons(id, "net")}`,
      padEarchLog
    )}, wins: ${rightPad(
      `${logKeyAllSeasons(id, "wins")}`,
      padEarchLog
    )}, looses: ${rightPad(
      `${logKeyAllSeasons(id, "looses")}`,
      padEarchLog
    )}\n`;
  };

  if (!top20CrossSeasonsByNet.length) {
    throw new Error('No ID satisfies the MIN_N_BETS criteria')
  }

  const winnerConfigNet = top20CrossSeasonsByNet[0];
  if (onlyOutputNet && winnerConfigNet) {
    console.log(JSON.stringify({
      ...fromId(winnerConfigNet),
      ...matrix[seasons[0]][winnerConfigNet]
    }, null, 2));
    process.exit(0);
  }
  const avgNet = Math.round(1000 * allSeasons.net[winnerConfigNet]) / 1000;
  s += `\nWinner config: ${winnerConfigNet} (avg net ${avgNet})\nNet results : \n`;
  s +=
    seasons
      .map((season) => {
        const results =
          matrix[season][top20CrossSeasonsByNet[0]].results;
        return season + " : " + results.net + "x";
      })
      .join("\n") + "\n";
  s += `Full config and results ${seasons[0]}\n`;

  s +=
    JSON.stringify({
      ...fromId(top20CrossSeasonsByNet[0]),
      ...matrix[seasons[0]][top20CrossSeasonsByNet[0]]
    }, null, 2) +
    "\n";

  s += `Top 10 :\n`;
  top20CrossSeasonsByNet.slice(0, 10).forEach((id, i) => {
    s += line(id);
  });
  s += "\n";

  s += "\n=== SORT BY RATE (wins/looses):\n";
  const winnferConfigRate = top20CrossSeasonsByRate[0];
  if (onlyOutputRate) {
    console.log(JSON.stringify({
      ...fromId(winnferConfigRate),
      ...matrix[seasons[0]][winnferConfigRate]
    }, null, 2));
    process.exit(0);
  }
  const avgRate = Math.round(1000 * allSeasons.rate[winnferConfigRate]) / 1000;
  s += `\nWinner config: ${winnferConfigRate} (avg rate ${avgRate})\n`;
  s += `Full config and results ${seasons[0]}\n`;
  s +=
    JSON.stringify({
      ...fromId(top20CrossSeasonsByRate[0]),
      ...matrix[seasons[0]][top20CrossSeasonsByRate[0]]
    }, null, 2) +
    "\n";

  s += `Top 10 :\n`;
  top20CrossSeasonsByRate.slice(0, 10).forEach((id) => {
    s += `${rightPad(id, 28)}: rate = ${rightPad(
      `${logKeyAllSeasons(id, "rate")}`,
      padEarchLog
    )}, wins: ${rightPad(
      `${logKeyAllSeasons(id, "wins")}`,
      padEarchLog
    )}, looses: ${rightPad(
      `${logKeyAllSeasons(id, "looses")}`,
      padEarchLog
    )}\n`;
  });
  s += "\n";

  s += "\n=== SORT BY NET PER BET : net / (wins+looses):\n";
  const winnferConfigNetPerBet = top20CrossSeasonsByNetPerBet[0];
  if (onlyOutputNetPerBet) {
    console.log(JSON.stringify({
      ...fromId(winnferConfigNetPerBet),
      ...matrix[seasons[0]][winnferConfigNetPerBet]
    }, null, 2));
    process.exit(0);
  }
  const avgNetPerBet = Math.round(1000 * allSeasons.netPerBet[winnferConfigNetPerBet]) / 1000;
  s += `\nWinner config: ${winnferConfigNetPerBet} (avg net per bet ${avgNetPerBet})\n`;s += `Full config and results ${seasons[0]}\n`;
  s +=
    JSON.stringify({
      ...fromId(top20CrossSeasonsByNetPerBet[0]),
      ...matrix[seasons[0]][top20CrossSeasonsByNetPerBet[0]]
    }, null, 2) +
    "\n";

  s += `Top 10 :\n`;
  top20CrossSeasonsByNetPerBet.slice(0, 10).forEach((id) => {
    s += `${rightPad(id, 28)}: netperbet = ${rightPad(
      `${logKeyAllSeasons(id, "netPerBet")}`,
      padEarchLog
    )}, wins: ${rightPad(
      `${logKeyAllSeasons(id, "wins")}`,
      padEarchLog
    )}, looses: ${rightPad(
      `${logKeyAllSeasons(id, "looses")}`,
      padEarchLog
    )}\n`;
  });
  s += "\n";

  console.log(s);
  fs.writeFileSync(
    `./${FOLDER}/result-${new Date().toISOString().slice(0, 19)}.txt`,
    s,
    "utf8"
  );

  process.exit();
};
