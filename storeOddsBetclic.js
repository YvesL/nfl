const fs = require("fs");
const https = require("https");

const findArg = (param) => {
  return process.argv.findIndex((arg) => arg === param) !== -1
}

const SEASON = 2022;
let HOURS_OFFSET_PARIS = 0;
let COMPETITION_ID;

let games = {};
let url;
let path;
let teamsCompleteName;
if (findArg('--bundesliga')) {
  league = 'bundesliga';
  COMPETITION_ID = '5';
} else if (findArg('--bundesliga2')) {
  league = 'bundesliga2';
  COMPETITION_ID = '29';
} else if (findArg('--seriea')) {
  league = 'seriea';
  COMPETITION_ID = '6';
} else if (findArg('--serieb')) {
  league = 'serieb';
  COMPETITION_ID = '30';
} else if (findArg('--superlig')) {
  league = 'superlig';
  COMPETITION_ID = '37';
} else if (findArg('--liga')) {
  league = 'liga';
  COMPETITION_ID = '7';
} else if (findArg('--premiere')) {
  league = 'premiere';
  COMPETITION_ID = '3';
} else if (findArg('--championship')) {
  league = 'championship';
  COMPETITION_ID = '28';
} else if (findArg('--eredivisie')) {
  league = 'eredivisie';
  COMPETITION_ID = '21';
} else if (findArg('--primeira')) {
  league = 'primeira';
  COMPETITION_ID = '32';
} else if (findArg('--oneliga')) {
  league = 'oneliga';
  COMPETITION_ID = '220';
} else if (findArg('--superliga')) {
  league = 'superliga';
  COMPETITION_ID = '5710';
} else if (findArg('--jupiler')) {
  league = 'jupiler';
  COMPETITION_ID = '26';
} else if (findArg('--botola')) {
  league = 'botola';
  COMPETITION_ID = '3142';
} else if (findArg('--nba')) {
  HOURS_OFFSET_PARIS = -8;
  league = 'nba';
  COMPETITION_ID = '13';
} else if (findArg('--nfl')) {
  HOURS_OFFSET_PARIS = -8;
  league = 'nfl';
  COMPETITION_ID = '84';
} else {
  throw new Error('unknown league, provide for example --bundesliga')
}
path = `${__dirname}/${league}/games-wh-${SEASON}.json`;
games = JSON.parse(fs.readFileSync(path, "utf8"));
teamsCompleteName = require(`${__dirname}/${league}/utils`).teamsCompleteName;

const options = {
  host: 'offer.cdn.betclic.fr',
  method: 'GET',
  path: `/api/pub/v2/competitions/${COMPETITION_ID}?application=2&countrycode=fr&fetchMultipleDefaultMarkets=true&language=fr&sitecode=frfr`,
  headers: {
    'Content-Type': 'text/html',
  },
};

const main = async () => {
  const json = await new Promise((resolve) => {
    const req = https.request(
      options,
      (res, err) => {
        if (res.statusCode !== 200) {
          throw new Error('status code !== 200 ', res.statusCode)
        }

        let data = ''
        res.on('data', d => {
          data += d.toString('utf8')
        })
        res.on('end', () => {
          resolve(JSON.parse(data));
        })
      }
    );
    req.end("")
  });

  let news = 0;
  let updates = 0;
  if (json === null) {
    console.log(`competition ${league} not found, maybe no games in coming days`);
    process.exit();
  }
  console.log("found", json.unifiedEvents.length, "games");
  json.unifiedEvents.forEach((e) => {
    if (e.isLive) {
      console.log("skipping live event " + e.name);
      return;
    }
    const team1 = e.name.split(" - ")[0];
    const team1Short = teamsCompleteName[team1];
    const team2 = e.name.split(" - ")[1];
    const team2Short = teamsCompleteName[team2];
    const ignoreTeams = ['NBA 2022-2023 Specials', 'undefined', undefined]
    if (
      ignoreTeams.includes(team1Short) ||
      ignoreTeams.includes(team2Short)
    ) {
      console.log("skipping special team ");
      return;
    }
    if (!team1Short || !team2Short) {
      throw new Error("Missing team for " + team1 + " or " + team2);
    }

    if (!e.markets || !e.markets.length) {
      console.log("skipping no market event " + e.name);
      return;
    }

    let oddTeam1;
    let oddTeam2;
    let oddDraw;
    let oddTeam1OrDraw;
    let oddTeam1Or2;
    let oddTeam2OrDraw;
    if (league === 'nba' || league === 'nfl') {
      oddTeam1 = e.markets[0].selections[0].odds;
      oddTeam2 = e.markets[0].selections[1].odds;
      if (
        typeof oddTeam1 !== "number" ||
        typeof oddTeam2 !== "number"
      ) {
        throw new Error(
          "odds are incorrect " + day + oddTeam1 + " / " + oddTeam2
        );
      }
    } else {
      oddTeam1 = e.markets[0].selections[0].odds;
      oddTeam2 = e.markets[0].selections[2].odds;
      oddDraw = e.markets[0].selections[1].odds;
      oddTeam1OrDraw = e.markets[1].selections[0].odds;
      oddTeam1Or2 = e.markets[1].selections[1].odds;
      oddTeam2OrDraw = e.markets[1].selections[2].odds;
      if (
        typeof oddTeam1 !== "number" ||
        typeof oddDraw !== "number" ||
        typeof oddTeam2 !== "number" ||
        typeof oddTeam1OrDraw !== "number" ||
        typeof oddTeam1Or2 !== "number" ||
        typeof oddTeam2OrDraw !== "number"
      ) {
        throw new Error(
          "odds are incorrect " + day + oddTeam1 + " / " + oddTeam2
        );
      }
    }
    
    /*
      There are at least 8 hours diff between France (where the XHR request)
      is emitted from, and USA
    */
    const d = new Date(new Date(e.date).getTime() + (1000 * 60 * 60 * HOURS_OFFSET_PARIS)).toISOString();
    const day = d.slice(0, 10);
    if (
      !games[team1Short] ||
      !games[team1Short][day]
    ) {
      console.log(`${day} ${team1Short}-${team2Short} game not found, creating it`);
      if (!games[team1Short]) games[team1Short] = {};
      games[team1Short][day] = {
        op: team2Short,
        wk: Object.keys(games[team1Short] || {}).length,
        date: day
      }
    }
    if (
      games[team1Short][day].op !== team2Short
    ) {
      throw new Error(`Found other oponent for this day, ${games[team1Short][day].op} VS ${team2Short} on ${day}`)
    }

    if (
      !games[team2Short] ||
      !games[team2Short][day]
    ) {
      console.log(`${day} ${team1Short}-${team2Short} game not found, creating it`);
      if (!games[team2Short]) games[team2Short] = {};
      games[team2Short][day] = {
        op: team1Short,
        wk: Object.keys(games[team2Short] || {}).length,
        date: day
      }
    }
    if (
      games[team2Short][day].op !== team1Short
    ) {
      console.log(games[team2Short][day])
      throw new Error(`Found other oponent for this day, ${games[team1Short][day].op} VS ${team2Short} on ${day}`)
    }

    if (!games[team1Short][day].odds) {
      news += 1;
      games[team1Short][day].odds = {};
    } else {
      updates += 1;
    }
    if (!games[team2Short][day].odds) {
      games[team2Short][day].odds = {};
    }
    games[team1Short][day].home = true;
    games[team2Short][day].home = false;
    if (league === 'nba' || league === 'nfl') {
      games[team1Short][day].odds.betclic = [oddTeam1, oddTeam2];
      games[team2Short][day].odds.betclic = [oddTeam2, oddTeam1];
    } else {
      games[team1Short][day].odds.betclic = [oddTeam1, oddTeam2, oddDraw, oddTeam1OrDraw, oddTeam2OrDraw, oddTeam1Or2];
      games[team2Short][day].odds.betclic = [oddTeam2, oddTeam1, oddDraw, oddTeam2OrDraw, oddTeam1OrDraw, oddTeam1Or2];
    }
  });
  fs.writeFileSync(
    `${path}`,
    JSON.stringify(games, null, 2),
    "utf8"
  );
  return `${path} updated ${news} news and ${updates} updates ! `;
};

if (process.argv.findIndex((arg) => arg === "--long") !== -1) {
  console.log("started to index cotes every 30 minutes");
  try {
    fs.readFileSync(`${__dirname}/indexing-${YEAR}.txt`, "utf8");
  } catch (err) {
    fs.writeFileSync(`${__dirname}/indexing-${YEAR}.txt`, "", "utf8");
  }
  const func = async () => {
    let line;
    let d = new Date().toISOString().slice(0, 16);
    try {
      const a = await main();
      console.log(a);
      line = `${d} ${a}`;
    } catch (err) {
      console.log(err);
      line = `${d} error: ${err.Error}`;
    }
    let text = fs.readFileSync(`${__dirname}/indexing-${YEAR}.txt`, "utf8");
    text += "\n" + line;
    fs.writeFileSync(`${__dirname}/indexing-${YEAR}.txt`, text, "utf8");
  };
  setInterval(func, 1000 * 60 * 30);
  setInterval(
    () => {
      process.exit();
    },
    1000 * 60 * 60 * 48 // 2 days restart
  );
  func();
} else {
  const func = async () => {
    const a = await main();
    console.log(a);
    process.exit();
  };
  func();
}
