const puppeteer = require("puppeteer");
const { parse } = require("node-html-parser");
const fs = require("fs");
const { getProcessArgv } = require('./utils')

const findArg = (param) => {
  return process.argv.findIndex((arg) => arg === param) !== -1
}

let SEASON = 2022;
if (findArg('--season') && ["2021"].includes(getProcessArgv('--season'))) {
  SEASON = parseInt(getProcessArgv('--season'));
}

let games;
let url;
let path;
let teamsCompleteName;
let league;
if (findArg('--bundesliga')) {
  league = 'bundesliga';
  teamsCompleteName = require(__dirname + "/bundesliga/utils").teamsCompleteName;
  path = `${__dirname}/bundesliga/games-wh-${SEASON}.json`;
  games = JSON.parse(fs.readFileSync(path, "utf8"));
  url = `https://www.live-result.com/football/Germany/Bundesliga-I/results`;
  if (SEASON === 2021) {
    url = `https://www.live-result.com/football/Germany/Bundesliga-I/2021-2022/results`;
  }
} else if (findArg('--bundesliga2')) {
  league = 'bundesliga2';
  teamsCompleteName = require(__dirname + "/bundesliga2/utils").teamsCompleteName;
  path = `${__dirname}/bundesliga2/games-wh-${SEASON}.json`;
  games = JSON.parse(fs.readFileSync(path, "utf8"));
  url = `https://www.live-result.com/football/Germany/Bundesliga-II/results`;
  if (SEASON === 2021) {
    url = `https://www.live-result.com/football/Germany/Bundesliga-II/2021-2022/results`;
  }
} else if (findArg('--seriea')) {
  teamsCompleteName = require(__dirname + "/seriea/utils").teamsCompleteName;
  path = `${__dirname}/seriea/games-wh-${SEASON}.json`;
  games = JSON.parse(fs.readFileSync(path, "utf8"));
  url = `https://www.live-result.com/football/Italy/Serie-A/results`;
  if (SEASON === 2021) {
    url = `https://www.live-result.com/football/Italy/Serie-A/2021-2022/results`;
  }
} else if (findArg('--serieb')) {
  teamsCompleteName = require(__dirname + "/serieb/utils").teamsCompleteName;
  path = `${__dirname}/serieb/games-wh-${SEASON}.json`;
  games = JSON.parse(fs.readFileSync(path, "utf8"));
  url = `https://www.live-result.com/football/Italy/Serie-B/results`;
  if (SEASON === 2021) {
    url = `https://www.live-result.com/football/Italy/Serie-B/2021-2022/results`;
  }
} else if (findArg('--superlig')) {
  teamsCompleteName = require(__dirname + "/superlig/utils").teamsCompleteName;
  path = `${__dirname}/superlig/games-wh-${SEASON}.json`;
  games = JSON.parse(fs.readFileSync(path, "utf8"));
  url = `https://www.live-result.com/football/Turkiye/Super-Lig/results`;
  if (SEASON === 2021) {
    url = `https://www.live-result.com/football/Turkiye/Super-Lig/2021-2022/results`;
  }
} else if (findArg('--liga')) {
  teamsCompleteName = require(__dirname + "/liga/utils").teamsCompleteName;
  path = `${__dirname}/liga/games-wh-${SEASON}.json`;
  games = JSON.parse(fs.readFileSync(path, "utf8"));
  url = `https://www.live-result.com/football/Spain/LaLiga/results`;
  if (SEASON === 2021) {
    url = `https://www.live-result.com/football/Spain/LaLiga/2021-2022/results`;
  }
} else if (findArg('--premiere')) {
  teamsCompleteName = require(__dirname + "/premiere/utils").teamsCompleteName;
  path = `${__dirname}/premiere/games-wh-${SEASON}.json`;
  games = JSON.parse(fs.readFileSync(path, "utf8"));
  url = `https://www.live-result.com/football/England/Premier-League/results`;
  if (SEASON === 2021) {
    url = `https://www.live-result.com/football/England/Premier-League/2021-2022/results`;
  }
} else if (findArg('--championship')) {
  teamsCompleteName = require(__dirname + "/championship/utils").teamsCompleteName;
  path = `${__dirname}/championship/games-wh-${SEASON}.json`;
  games = JSON.parse(fs.readFileSync(path, "utf8"));
  url = `https://www.live-result.com/football/England/Championship/results`;
  if (SEASON === 2021) {
    url = `https://www.live-result.com/football/England/Championship/2021-2022/results`;
  }
} else if (findArg('--liga')) {
  teamsCompleteName = require(__dirname + "/liga/utils").teamsCompleteName;
  path = `${__dirname}/liga/games-wh-${SEASON}.json`;
  games = JSON.parse(fs.readFileSync(path, "utf8"));
  url = `https://www.live-result.com/football/Spain/LaLiga/results`;
  if (SEASON === 2021) {
    url = `https://www.live-result.com/football/Spain/LaLiga/2021-2022/results`;
  }
} else if (findArg('--eredivisie')) {
  teamsCompleteName = require(__dirname + "/eredivisie/utils").teamsCompleteName;
  path = `${__dirname}/eredivisie/games-wh-${SEASON}.json`;
  games = JSON.parse(fs.readFileSync(path, "utf8"));
  url = `https://www.live-result.com/football/Netherlands/Eredivisie/results`;
  if (SEASON === 2021) {
    url = `https://www.live-result.com/football/Netherlands/Eredivisie/2021-2022/results`;
  }
} else if (findArg('--primeira')) {
  teamsCompleteName = require(__dirname + "/primeira/utils").teamsCompleteName;
  path = `${__dirname}/primeira/games-wh-${SEASON}.json`;
  games = JSON.parse(fs.readFileSync(path, "utf8"));
  url = `https://www.live-result.com/football/Portugal/Primeira-Liga/results`;
  if (SEASON === 2021) {
    url = `https://www.live-result.com/football/Portugal/Primeira-Liga/2021-2022/results`;
  }
} else if (findArg('--oneliga')) {
  teamsCompleteName = require(__dirname + "/oneliga/utils").teamsCompleteName;
  path = `${__dirname}/oneliga/games-wh-${SEASON}.json`;
  games = JSON.parse(fs.readFileSync(path, "utf8"));
  url = `https://www.live-result.com/football/Czech-Republic/One-Liga/results`;
  if (SEASON === 2021) {
    url = `https://www.live-result.com/football/Czech-Republic/One-Liga/2021-2022/results`;
  }
} else if (findArg('--superliga')) {
  teamsCompleteName = require(__dirname + "/superliga/utils").teamsCompleteName;
  path = `${__dirname}/superliga/games-wh-${SEASON}.json`;
  games = JSON.parse(fs.readFileSync(path, "utf8"));
  url = `https://www.live-result.com/football/Serbia/Super-Lig/results`;
  if (SEASON === 2021) {
    url = `https://www.live-result.com/football/Serbia/Super-Lig/2021-2022/results`;
  }
} else if (findArg('--jupiler')) {
  teamsCompleteName = require(__dirname + "/jupiler/utils").teamsCompleteName;
  path = `${__dirname}/jupiler/games-wh-${SEASON}.json`;
  games = JSON.parse(fs.readFileSync(path, "utf8"));
  url = `https://www.live-result.com/football/Belgium/First-Division-A/results`;
  if (SEASON === 2021) {
    url = `https://www.live-result.com/football/Belgium/First-Division-A/2021-2022/results`;
  }
} else if (findArg('--leagueone')) {
  teamsCompleteName = require(__dirname + "/leagueone/utils").teamsCompleteName;
  path = `${__dirname}/leagueone/games-wh-${SEASON}.json`;
  games = JSON.parse(fs.readFileSync(path, "utf8"));
  url = `https://www.live-result.com/football/England/First-League/results`;
  if (SEASON === 2021) {
    url = `https://www.live-result.com/football/England/First-League/2021-2022/results`;
  }
} else if (findArg('--leaguetwo')) {
  teamsCompleteName = require(__dirname + "/leaguetwo/utils").teamsCompleteName;
  path = `${__dirname}/leaguetwo/games-wh-${SEASON}.json`;
  games = JSON.parse(fs.readFileSync(path, "utf8"));
  url = `https://www.live-result.com/football/England/League-Two/results`;
  if (SEASON === 2021) {
    url = `https://www.live-result.com/football/England/League-Two/2021-2022/results`;
  }
} else {
  throw new Error('unknown league, provide for example --bundesliga')
}

let i = 0;
const parseTeamAndYear = async (teamIndex, season) => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  console.log(url)
  await page.goto(url);

  await new Promise((r) => {
    setTimeout(r, 1000);
  });

  let html;
  try {
    html = await page.evaluate(() => {
      let date;
      document.querySelectorAll("div.matches-list-date").forEach(div => {
        let nextSibling;
        const b = div.textContent;
        date = `${b.slice(6,11)}-${b.slice(3,5)}-${b.slice(0,2)}`;
        if (date.length === 10) {
          for (let i = 0; i < 30; i += 1) {
            nextSibling = (nextSibling || div).nextElementSibling;
            if (nextSibling && (nextSibling.tagName === 'a' || nextSibling.tagName === 'A')) {
              nextSibling.setAttribute('date', date)
            } else {
              return;
            }
          }
        }
      });
      return document.getElementsByClassName('matches-list')[0].innerHTML;
    });
  } catch (err) {
    console.log(err);
    html = null;
  }

  if (html) {
    const root = parse(html);
    const lines = root.querySelectorAll("a.matches-list-match");
    let date;
    lines.forEach(a => {
      date = a.attributes.date;
      if (date.length !== 10) {
        return;
      }

      let team1 = a.querySelector('.team1').querySelector('span').childNodes[0].rawText;
      let team2 = a.querySelector('.team2').querySelector('span').childNodes[0].rawText;
      if (!teamsCompleteName[team1]) {
        throw new Error('Unknown team ' + team1);
      }
      if (!teamsCompleteName[team2]) {
        throw new Error('Unknown team ' + team2);
      }
      team1 = teamsCompleteName[team1];
      team2 = teamsCompleteName[team2];
      const score = a.querySelector('.match-title').querySelector('.has-score').childNodes[0].rawText;
      const scoreTeam1 = parseInt(score.split(':')[0], 10);
      const scoreTeam2 = parseInt(score.split(':')[1], 10);
      if (typeof scoreTeam1 < 0 || typeof scoreTeam1 > 20) {
        throw new Error('Invalid score team 1')
      }
      if (typeof scoreTeam2 < 0 || typeof scoreTeam2 > 20) {
        throw new Error('Invalid score team 2')
      }
      if (!games[team1]) {
        games[team1] = {};
        console.warn('games not found for team1 ' + team1)
      }
      if (!games[team2]) {
        games[team2] = {}
        console.warn('games not found for team2 ' + team2)
      }      

      console.log(date, team1, scoreTeam1, scoreTeam2, team2);

      if (games[team1][date]) {
        i += 1;
        games[team1][date].home = true;
        games[team1][date].score = [scoreTeam1, scoreTeam2];
        games[team1][date].victory = scoreTeam1 > scoreTeam2;
      } else {
        i += 1;
        games[team1][date] = {
          date: date,
          odds: {},
          home: true,
          op: team2,
          score: [scoreTeam1, scoreTeam2],
          victory: scoreTeam1 > scoreTeam2,
          wk: Object.keys(games[team1]).length
        }
      }

      if (games[team2][date]) {
        i += 1;
        games[team2][date].home = false;
        games[team2][date].score = [scoreTeam2, scoreTeam1];
        games[team2][date].victory = scoreTeam1 < scoreTeam2;
      } else {
        i += 1;
        games[team2][date] = {
          date: date,
          odds: {},
          home: false,
          op: team1,
          score: [scoreTeam2, scoreTeam1],
          victory: scoreTeam1 < scoreTeam2,
          wk: Object.keys(games[team2]).length
        }
      }
      
    });

    await browser.close();

    console.log(i / 2, 'new scores recorded');
    fs.writeFileSync(
      path,
      JSON.stringify(games, null, 2),
      'utf8'
    )
    return;
  }
};

parseTeamAndYear(0, '2022');
