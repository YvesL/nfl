module.exports.getProcessArgv = (param) => {
  const index = process.argv.findIndex((arg) => arg === param);
  if (index === -1) {
    return undefined;
  }

  return process.argv[index + 1];
};

module.exports.rightPad = (str, length) => {
  let s = str;
  for (let i = 0; i <= length - str.length; i += 1) {
    s += " ";
  }
  return s;
};

module.exports.IGNORE_OP = {
  'all': [],
};

module.exports.teamsCompleteName = {
  "Port Vale": "Port Vale",
  "Mansfield": "Mansfield",
  "Barrow": "Barrow",
  "Northampton": "Northampton",
  "Bradford City": "Bradford City",
  "Bradford": "Bradford City",
  "Scunthorpe": "Scunthorpe",
  "Bristol Rovers": "Bristol Rovers",
  "Exeter": "Exeter",
  "Harrogate": "Harrogate",
  "Sutton": "Sutton",
  "Sutton Utd": "Sutton",
  "Hartlepool": "Hartlepool",
  "Hartlepool United": "Hartlepool",
  "Colchester": "Colchester",
  "Leyton Orient": "Leyton Orient",
  "Tranmere": "Tranmere",
  "Mansfield": "Mansfield",
  "Forest Green": "Forest Green",
  "Newport": "Newport",
  "Newport County": "Newport",
  "Rochdale": "Rochdale",
  "Oldham": "Oldham",
  "Crawley": "Crawley",
  "Crawley Town": "Crawley",
  "Stevenage": "Stevenage",
  "Salford": "Salford",
  "Salford City": "Salford",
  "Walsall": "Walsall",
  "Swindon": "Swindon",
  "Swindon Town": "Swindon",
  "Carlisle": "Carlisle",
  "Carlisle United": "Carlisle",
  "Crewe": "Crewe",
  "Crewe Alexandra": "Crewe",
  "Gillingham": "Gillingham",
  "Grimsby": "Grimsby",
  "Doncaster": "Doncaster",
  "Stockport County": "Stockport County",
  "Stockport": "Stockport County",
  "AFC Wimbledon": "AFC Wimbledon",
  "Afc Wimbledon": "AFC Wimbledon",
  "Bury": "Bury",
  "Lincoln": "Lincoln",
  "Macclesfield": "Macclesfield",
  "Cambridge Utd": "Cambridge Utd",
  "MK Dons": "MK Dons",
  "Morecambe": "Morecambe",
  "Cheltenham": "Cheltenham",
  "Notts Co": "Notts Co",
  "Yeovil": "Yeovil",
  "Plymouth": "Plymouth",
  "Plymouth": "Plymouth",
  "Plymouth": "Plymouth",
};
