const puppeteer = require('puppeteer');
const fs = require('fs');
const { parseDate, getProcessArgv, ukToEuOddFormat } = require('./utils');

const findArg = (param) => {
  return process.argv.findIndex((arg) => arg === param) !== -1;
};
const onlyFirstPage = findArg('--ofp');
const ukOddFormat = findArg('--uk');

let HOURS_OFFSET_PARIS = 0;
const YEARS_BASED_ON_MONTH = {
  1: {
    Jan: 1,
    Feb: 1,
    Mar: 1,
    Apr: 1,
    May: 1,
    Jun: 1,
  },
  2: {
    Jan: 1,
    Feb: 1,
    Mar: 1,
    Apr: 1,
    May: 1,
    Jun: 1,
    Jul: 1,
    Aug: 1,
  },
};
let YEAR_BASED_ON_MONTH;
let CURRENT_SEASON = 2022;
let SEASON = 2022;
if (
  findArg('--season') &&
  ['2018', '2019', '2020', '2021'].includes(getProcessArgv('--season'))
) {
  SEASON = parseInt(getProcessArgv('--season'));
}

let games;
let url;
let urlNextMatches;
let path;
let teamsCompleteName;
let league;
if (findArg('--bundesliga')) {
  league = 'bundesliga';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/germany/bundesliga/results/`;
  urlNextMatches = 'https://www.oddsportal.com/soccer/germany/bundesliga/';
  if (SEASON === 2021) {
    url =
      'https://www.oddsportal.com/soccer/germany/bundesliga-2021-2022/results/';
  }
} else if (findArg('--bundesliga2')) {
  league = 'bundesliga2';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/germany/2-bundesliga/results/`;
  urlNextMatches = 'https://www.oddsportal.com/soccer/germany/2-bundesliga/';
  if (SEASON === 2021) {
    url =
      'https://www.oddsportal.com/soccer/germany/2-bundesliga-2021-2022/results/';
  }
} else if (findArg('--seriea')) {
  league = 'seriea';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/italy/serie-a/results/`;
  urlNextMatches = 'https://www.oddsportal.com/soccer/italy/serie-a/';
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/soccer/italy/serie-a-2021-2022/results/';
  }
} else if (findArg('--serieb')) {
  league = 'serieb';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/italy/serie-b/results/`;
  urlNextMatches = 'https://www.oddsportal.com/soccer/italy/serie-b/';
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/soccer/italy/serie-b-2021-2022/results/';
  }
} else if (findArg('--superlig')) {
  league = 'superlig';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/turkey/super-lig/results/`;
  urlNextMatches = 'https://www.oddsportal.com/soccer/turkey/1-lig/';
  if (SEASON === 2021) {
    url =
      'https://www.oddsportal.com/soccer/turkey/super-lig-2021-2022/results/';
  }
} else if (findArg('--liga')) {
  league = 'liga';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/spain/laliga/results/`;
  urlNextMatches = 'https://www.oddsportal.com/soccer/spain/laliga/';
  if (SEASON === 2021) {
    url = `https://www.oddsportal.com/soccer/spain/laliga-2021-2022/results/`;
  }
} else if (findArg('--premiere')) {
  league = 'premiere';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/england/premier-league/results/`;
  urlNextMatches = 'https://www.oddsportal.com/soccer/england/premier-league/';
  if (SEASON === 2021) {
    url =
      'https://www.oddsportal.com/soccer/england/premier-league-2021-2022/results/';
  }
} else if (findArg('--championship')) {
  league = 'championship';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/england/championship/results/`;
  urlNextMatches = 'https://www.oddsportal.com/soccer/england/championship/';
  if (SEASON === 2021) {
    url =
      'https://www.oddsportal.com/soccer/england/championship-2021-2022/results/';
  } else if (SEASON === 2019) {
    url =
      'https://www.oddsportal.com/soccer/england/championship-2019-2020/results/';
  }
} else if (findArg('--eredivisie')) {
  league = 'eredivisie';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/netherlands/eredivisie/results/`;
  urlNextMatches = 'https://www.oddsportal.com/soccer/netherlands/eredivisie/';
  if (SEASON === 2021) {
    url =
      'https://www.oddsportal.com/soccer/netherlands/eredivisie-2021-2022/results/';
  }
} else if (findArg('--ligue1')) {
  league = 'ligue1';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/france/ligue-1/results/`;
  urlNextMatches = 'https://www.oddsportal.com/soccer/france/ligue-1/';
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/soccer/france/ligue-1-2021-2022/results/';
  }
} else if (findArg('--primeira')) {
  league = 'primeira';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/portugal/liga-portugal/results/`;
  urlNextMatches = 'https://www.oddsportal.com/soccer/portugal/liga-portugal/';
  if (SEASON === 2021) {
    url =
      'https://www.oddsportal.com/soccer/portugal/liga-portugal-2021-2022/results/';
  }
} else if (findArg('--oneliga')) {
  league = 'oneliga';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/czech-republic/1-liga/results/`;
  urlNextMatches = 'https://www.oddsportal.com/soccer/czech-republic/1-liga/';
  if (SEASON === 2021) {
    url =
      'https://www.oddsportal.com/soccer/czech-republic/1-liga-2021-2022/results/';
  }
} else if (findArg('--superliga')) {
  league = 'superliga';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/serbia/super-liga/results/`;
  urlNextMatches = 'https://www.oddsportal.com/soccer/serbia/super-liga/';
  if (SEASON === 2021) {
    url =
      'https://www.oddsportal.com/soccer/serbia/super-liga-2021-2022/results/';
  }
} else if (findArg('--jupiler')) {
  league = 'jupiler';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/belgium/jupiler-pro-league/results/`;
  urlNextMatches =
    'https://www.oddsportal.com/soccer/belgium/jupiler-pro-league/';
  if (SEASON === 2021) {
    url =
      'https://www.oddsportal.com/soccer/belgium/jupiler-pro-league-2021-2022/results/';
  }
} else if (findArg('--leagueone')) {
  league = 'leagueone';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/england/league-one/results/`;
  urlNextMatches = 'https://www.oddsportal.com/soccer/england/league-one/';
  if (SEASON === 2021) {
    url =
      'https://www.oddsportal.com/soccer/england/league-one-2021-2022/results/';
  } else if (SEASON === 2018) {
    url =
      'https://www.oddsportal.com/soccer/england/league-one-2018-2019/results/';
  } else if (SEASON === 2019) {
    url =
      'https://www.oddsportal.com/soccer/england/league-one-2019-2020/results/';
  }
} else if (findArg('--leaguetwo')) {
  league = 'leaguetwo';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/england/league-two/results/`;
  urlNextMatches = `https://www.oddsportal.com/soccer/england/league-two/`;
  if (SEASON === 2021) {
    url =
      'https://www.oddsportal.com/soccer/england/league-two-2021-2022/results/';
  } else if (SEASON === 2019) {
    url =
      'https://www.oddsportal.com/soccer/england/league-two-2019-2020/results/';
  } else if (SEASON === 2018) {
    url =
      'https://www.oddsportal.com/soccer/england/league-two-2018-2019/results/';
  }
} else if (findArg('--north')) {
  league = 'north';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/england/national-league-north/results/`;
  urlNextMatches = `https://www.oddsportal.com/soccer/england/national-league-north/`;
  if (SEASON === 2021) {
    url =
      'https://www.oddsportal.com/soccer/england/national-league-north-2021-2022/results/';
  } else if (SEASON === 2019) {
    url =
      'https://www.oddsportal.com/soccer/england/national-league-north-2019-2020/results/';
  } else if (SEASON === 2018) {
    url =
      'https://www.oddsportal.com/soccer/england/national-league-north-2018-2019/results/';
  }
} else if (findArg('--south')) {
  league = 'south';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/england/national-league-south/results/`;
  urlNextMatches = `https://www.oddsportal.com/soccer/england/national-league-south/`;
  if (SEASON === 2021) {
    url =
      'https://www.oddsportal.com/soccer/england/national-league-south-2021-2022/results/';
  } else if (SEASON === 2019) {
    url =
      'https://www.oddsportal.com/soccer/england/national-league-south-2019-2020/results/';
  } else if (SEASON === 2018) {
    url =
      'https://www.oddsportal.com/soccer/england/national-league-south-2018-2019/results/';
  }
} else if (findArg('--botola')) {
  league = 'botola';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/morocco/botola-pro/results/`;
  if (SEASON === 2021) {
    url =
      'https://www.oddsportal.com/soccer/morocco/botola-pro-2021-2022/results/';
  }
} else if (findArg('--3liga')) {
  league = '3liga';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/germany/3-liga/results/`;
  urlNextMatches = `https://www.oddsportal.com/soccer/germany/3-liga/`;
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/soccer/germany/3-liga-2021-2022/results/';
  }
} else if (findArg('--ligue2')) {
  league = 'ligue2';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/france/ligue-2/results/`;
  urlNextMatches = `https://www.oddsportal.com/soccer/france/ligue-2/`;
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/soccer/france/ligue-2-2021-2022/results/';
  }
} else if (findArg('--superleague2')) {
  league = 'superleague2';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/greece/super-league-2/results/`;
  urlNextMatches = `https://www.oddsportal.com/soccer/greece/super-league-2/`;
  if (SEASON === 2021) {
    url =
      'https://www.oddsportal.com/soccer/greece/super-league-2-2021-2022/results/';
  }
} else if (findArg('--seriecgroupea')) {
  league = 'seriecgroupea';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/italy/serie-c-group-a/results/`;
  urlNextMatches = `https://www.oddsportal.com/soccer/italy/serie-c-group-a/`;
} else if (findArg('--seriecgroupeb')) {
  league = 'seriecgroupeb';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/italy/serie-c-group-b/results/`;
  urlNextMatches = `https://www.oddsportal.com/soccer/italy/serie-c-group-b/`;
} else if (findArg('--seriecgroupec')) {
  league = 'seriecgroupec';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/italy/serie-c-group-c/results/`;
  urlNextMatches = `https://www.oddsportal.com/soccer/italy/serie-c-group-c/`;
} else if (findArg('--segundarfefgroupe1')) {
  league = 'segundarfefgroupe1';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/spain/segunda-rfef-group-1/results/`;
  urlNextMatches = `https://www.oddsportal.com/soccer/spain/segunda-rfef-group-1/`;
} else if (findArg('--segundarfefgroupe2')) {
  league = 'segundarfefgroupe2';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/spain/segunda-rfef-group-2/results/`;
  urlNextMatches = `https://www.oddsportal.com/soccer/spain/segunda-rfef-group-2/`;
} else if (findArg('--segundarfefgroupe3')) {
  league = 'segundarfefgroupe3';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/spain/segunda-rfef-group-3/results/`;
  urlNextMatches = `https://www.oddsportal.com/soccer/spain/segunda-rfef-group-3/`;
} else if (findArg('--segundarfefgroupe4')) {
  league = 'segundarfefgroupe4';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/spain/segunda-rfef-group-4/results/`;
  urlNextMatches = `https://www.oddsportal.com/soccer/spain/segunda-rfef-group-4/`;
} else if (findArg('--segundarfefgroupe5')) {
  league = 'segundarfefgroupe5';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/soccer/spain/segunda-rfef-group-5/results/`;
  urlNextMatches = `https://www.oddsportal.com/soccer/spain/segunda-rfef-group-5/`;
} else if (findArg('--russiapremierleague')) {
  league = 'russiapremierleague';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/football/russia/premier-league/results/`;
  urlNextMatches = `https://www.oddsportal.com/football/russia/premier-league/`;
} else if (findArg('--parvaliga')) {
  league = 'parvaliga';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/football/bulgaria/parva-liga/results/`;
  urlNextMatches = `https://www.oddsportal.com/football/bulgaria/parva-liga/`;
} else if (findArg('--ligamx')) {
  league = 'ligamx';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/football/mexico/liga-mx/results/`;
  urlNextMatches = `https://www.oddsportal.com/football/mexico/liga-mx/`;
} else if (findArg('--francenationale')) {
  league = 'francenationale';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/football/france/national/results/`;
  urlNextMatches = `https://www.oddsportal.com/football/france/national/`;
} else if (findArg('--austria2liga')) {
  league = 'austria2liga';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1];
  url = `https://www.oddsportal.com/football/austria/2-liga/results/`;
  urlNextMatches = `https://www.oddsportal.com/football/austria/2-liga/`;
} else {
  throw new Error('unknown league, provide for example --bundesliga');
}
teamsCompleteName = require(__dirname + `/${league}/utils`).teamsCompleteName;
path = `${__dirname}/${league}/games-wh-${SEASON}.json`;
games = JSON.parse(fs.readFileSync(path, 'utf8'));

let i = 1;
let j = 0;
const go = async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  let html = '';
  if (i === 1 && SEASON === CURRENT_SEASON) {
    console.log('Processing (incoming) ' + urlNextMatches);
    await page.goto(urlNextMatches);
    await page.setViewport({
      width: 1200,
      height: 3000,
    });
    await new Promise((r) => {
      setTimeout(r, 8000);
    });
    html = await page.evaluate(() => {
      const html2 = document.querySelectorAll('.mobile-next-matches')[0]
        .parentElement.parentElement.parentElement.parentElement;
      document.body.innerHTML = '';
      document.body.appendChild(html2);
      let lines = '';
      for (let i = 0; i < html2.childNodes.length; i += 1) {
        if (
          !html2.childNodes[i].attributes ||
          !html2.childNodes[i].attributes.set
        ) {
          continue;
        }
        html2.childNodes[i].className = '';
        for (let j = 0; j < html2.childNodes[i].childNodes.length; j += 1) {
          console.log(html2.childNodes[i].class);
          html2.childNodes[i].childNodes[j].className = '';
          for (
            let k = 0;
            k < html2.childNodes[i].childNodes[j].childNodes.length;
            k += 1
          ) {
            html2.childNodes[i].childNodes[j].childNodes[k].className = '';
            const innerText =
              html2.childNodes[i].childNodes[j].childNodes[k].innerText;
            if (!innerText) {
              continue;
            }
            let line = innerText
              .replace(/\n\n/g, ',,,')
              //.replace(/\n–\n/g, ' - ')
              .replace(',,,–,,,', ' - ')
              .replace(/\n/g, ' - ')
              .replace(' - – - ', ' - ')
              .replace(/\n\n/g, '\n');

            if (
              line.length < 15 &&
              !line.includes('Yesterday') &&
              !line.includes('Tomorrow') &&
              !line.includes('Today') &&
              !line.includes('2022') &&
              !line.includes('2023')
            ) {
              continue;
            }
            if (line.includes("B's")) {
              continue;
            }
            if (line.includes('award')) {
              continue;
            }
            if (!line.length || line.length < 5) {
              continue;
            }
            let dateLine = true;
            if (line.length > 15) {
              dateLine = false;
            }
            line = line.split(',,,');
            if (!dateLine) {
              line.splice(2, 0, 'null');
            }
            console.log(1, line);
            lines += line.join(',,,') + '\n';
          }
        }
      }
      console.log(lines);
      return lines;
    });
    html += '\n';
    console.log(html);
  }

  console.log('Processing (past) ' + url + `#/page/${i}/`);
  await page.goto(url + `#/page/${i}/`);
  await page.setViewport({
    width: 1200,
    height: 3000,
  });
  await new Promise((r) => {
    setTimeout(r, 5000);
  });
  const k = i;
  setTimeout(() => {
    if (k === i) {
      console.log('stuck ?');
      console.log('stuck at ', i, 'saving games');
      fs.writeFileSync(path, JSON.stringify(games, null, 2), 'utf8');
      process.exit(0);
    }
  }, 15000);
  const html2 = await page.evaluate(() => {
    let html2;
    try {
      html2 = document.querySelectorAll('.mobile-next-matches')[0].parentElement
        .parentElement.parentElement.parentElement;
    } catch (err) {
      console.log(err);
      return 'ignore';
    }
    document.body.innerHTML = '';
    document.body.appendChild(html2);
    let lines = '';
    for (let i = 0; i < html2.childNodes.length; i += 1) {
      if (
        !html2.childNodes[i].attributes ||
        !html2.childNodes[i].attributes.set
      ) {
        continue;
      }
      html2.childNodes[i].className = '';
      for (let j = 0; j < html2.childNodes[i].childNodes.length; j += 1) {
        console.log(html2.childNodes[i].class);
        html2.childNodes[i].childNodes[j].className = '';
        for (
          let k = 0;
          k < html2.childNodes[i].childNodes[j].childNodes.length;
          k += 1
        ) {
          html2.childNodes[i].childNodes[j].childNodes[k].className = '';
          const innerText =
            html2.childNodes[i].childNodes[j].childNodes[k].innerText;
          if (!innerText) {
            continue;
          }
          let line = html2.childNodes[i].childNodes[j].childNodes[k].innerText
            //.replace(/\n\n/g, ',,,')
            //.replace(/\n–\n/g, ' - ')
            .replace(',,,–,,,', ' - ')
            .replace('––', '–')
            .replace('\n–\n', ' - ')
            .replace(/\n\n/g, '\n')
            .replace(/\n/g, ',,,');

          if (
            line.length < 15 &&
            !line.includes('Yesterday') &&
            !line.includes('Tomorrow') &&
            !line.includes('Today') &&
            !line.includes('2022') &&
            !line.includes('2023')
          ) {
            continue;
          }
          if (line.includes("B's")) {
            continue;
          }
          if (line.includes('award')) {
            continue;
          }
          if (!line.length) {
            continue;
          }
          lines += line + '\n';
        }
      }
    }
    console.log(lines);
    return lines;
  });
  await new Promise((r) => {
    setTimeout(r, 1000);
  });
  if (html2 === 'ignore' || (onlyFirstPage && i === 2)) {
    await browser.close();
    console.log(j, 'games');
    fs.writeFileSync(path, JSON.stringify(games, null, 2), 'utf8');
    process.exit(0);
  }
  html += html2;
  const lines = html.split('\n');
  let date;
  let playoffs = false;
  let dateLine = false;
  lines.forEach((l) => {
    let gameDate;
    let futureGame;
    let teams;
    let team1;
    let team2;
    let score;
    let odd1;
    let oddDraw;
    let odd2;
    console.log(l);
    if (!l.length) {
      console.log('skip empty line');
      return;
    }

    l.split(',,,').forEach((a, i) => {
      if (i === 0) {
        if (a.length === 5) {
          if (!date) return;
          dateLine = false;
          const dateWithHour = `${date}T${a}`;
          gameDate = new Date(
            new Date(dateWithHour).getTime() +
              HOURS_OFFSET_PARIS * 60 * 60 * 1000
          )
            .toISOString()
            .slice(0, 10);
        } else {
          dateLine = true;
          console.log('-------');
          console.log(l);
          console.log('-------');
          date = parseDate(a, 0, 0, SEASON, YEAR_BASED_ON_MONTH);
          if (date === 'ignore') {
            date = null;
            console.log('Skipping date ', a);
            return;
          }
          console.log('Valid date', date);
        }
      }
      if (dateLine) return;
      if (i === 1) {
        //Mexico
        if (a === 'MLS All-Stars') return;
        if (l.split(',,,')[3] === ':') futureGame = false;
        else futureGame = true;
        console.log('futur game ????', futureGame);
        if (futureGame) team1 = a.trim().split(' - ')[0];
        else team1 = a.trim();
        if (!teamsCompleteName[team1]) {
          if (team1 === 'Team LeBron') return;
          if (team1 === 'Team Durant') return;
          console.warn('--------');
          console.warn(00000, `"${team1}": "${team1}",`);
          console.warn('--------');
          throw new Error('Unknown team ' + team1);
          process.exit(1);
        }
        team1 = teamsCompleteName[team1];
        if (futureGame) {
          team2 = a.trim().split(' - ')[1];
          if (!teamsCompleteName[team2]) {
            if (team2 === 'Team LeBron') return;
            if (team2 === 'Team Durant') return;
            console.warn('--------');
            console.warn(00000, `"${team2}": "${team2}",`);
            console.warn('--------');
            throw new Error('Unknown team ' + team2);
            process.exit(1);
          }
          team2 = teamsCompleteName[team2];
          teams = [team1, team2];
        }
      }
      if (i === 2) {
        if (futureGame) {
          if (a.includes('post')) {
            playoffs = true;
            console.log('skip because of postponed');
            return;
          }
          if (a.includes('canc')) {
            playoffs = true;
            console.log('skip because of canceled');
            return;
          }
          if (a.includes('Leg')) {
            playoffs = true;
            console.log('skip because of Leg');
            return;
          }
          if (a.includes('Play')) {
            playoffs = true;
            console.log('skip because of Playoffs');
            return;
          }
          if (a.includes('Champion')) {
            playoffs = true;
            console.log('skip because of Championship');
            return;
          }
          playoffs = false;
          if (a.includes('null') || !a.includes(':')) {
            console.log('Future game');
            score = null;
          } else {
            score = a.split(':').map((a) => parseInt(a, 10));
            console.log(score);
            score.forEach((s) => {
              if (!Number.isInteger(s))
                throw new Error('Invalid score not integer ' + s);
              if (s < 0 || s > 12) {
                throw new Error('Invalid score ' + s);
                process.exit(1);
              }
            });
          }
        } else {
          score = [parseInt(a, 10)];
        }
      }
      if (i === 3) {
        if (futureGame) {
          if (ukOddFormat) odd1 = ukToEuOddFormat(a);
          else odd1 = parseFloat(a, 10);
        }
      }
      if (i === 4) {
        if (futureGame) {
          if (ukOddFormat) oddDraw = ukToEuOddFormat(a);
          else oddDraw = parseFloat(a, 10);
        } else {
          score = score.concat(parseInt(a, 10));
        }
      } else if (i === 5) {
        if (futureGame) {
          if (ukOddFormat) odd2 = ukToEuOddFormat(a);
          else odd2 = parseFloat(a, 10);
        } else {
          team2 = a.trim();
          if (!teamsCompleteName[team2]) {
            if (team2 === 'Team LeBron') return;
            if (team2 === 'Team Durant') return;
            console.warn('--------');
            console.warn(00000, `"${team2}": "${team2}",`);
            console.warn('--------');
            throw new Error('Unknown team ' + team2);
            process.exit(1);
          }
          team2 = teamsCompleteName[team2];
          teams = [team1, team2];
        }
      } else if (i === 6) {
        if (!futureGame) {
          if (ukOddFormat) odd1 = ukToEuOddFormat(a);
          else odd1 = parseFloat(a, 10);
        }
      } else if (i === 7) {
        if (!futureGame) {
          if (ukOddFormat) oddDraw = ukToEuOddFormat(a);
          else oddDraw = parseFloat(a, 10);
        }
      } else if (i === 8) {
        if (!futureGame) {
          if (ukOddFormat) odd2 = ukToEuOddFormat(a);
          else odd2 = parseFloat(a, 10);
        }
      }
    });
    if (Number.isNaN(odd1) || Number.isNaN(odd2)) {
      console.log('skip because of odd not found');
      playoffs = true;
      return;
    }
    if (!playoffs && !dateLine) {
      console.log(
        gameDate,
        teams.join('-'),
        score ? score.join('-') : 'future',
        odd1,
        oddDraw,
        odd2
      );
      // bug with this game
      if (gameDate === '2022-03-18' && team1 === 'Boch') {
        return;
      }
      if (!games[team1]) {
        games[team1] = {};
      }
      if (!games[team1][gameDate]) {
        console.log('creating new game', gameDate, team1, team2);
        games[team1][gameDate] = {
          date: gameDate,
          op: team2,
          home: true,
        };
      }
      const oddsTeam1 = [odd1, odd2, oddDraw];
      games[team1][gameDate] = {
        ...games[team1][gameDate],
        odds: {
          ...games[team1][gameDate].odds,
          oddsportal: oddsTeam1,
        },
      };

      /* if (score && (!Number.isInteger(score[0]) || !Number.isInteger(score[1]))) {
        console.log(gameDate, team1, team2);
        console.log(score);
        throw 'a';
      } */
      if (score && score.length === 2) {
        games[team1][gameDate].score = score;
        games[team1][gameDate].victory = score[0] > score[1];
      }
      if (!games[team2]) {
        games[team2] = {};
      }
      if (!games[team2][gameDate]) {
        console.log('creating new game', gameDate, team1, team2);
        games[team2][gameDate] = {
          date: gameDate,
          op: team1,
          home: false,
        };
      }
      const oddsTeam2 = [odd2, odd1, oddDraw];
      games[team2][gameDate] = {
        ...games[team2][gameDate],
        odds: {
          ...games[team2][gameDate].odds,
          oddsportal: oddsTeam2,
        },
      };
      if (score) {
        games[team2][gameDate].score = [score[1], score[0]];
        games[team2][gameDate].victory = score[0] < score[1];
      }
      j += 1;
    }
  });
  console.log('end of page ' + i);
  await browser.close();
  i += 1;
  go();
};
go();
