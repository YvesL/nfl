module.exports.getStartDate = (season) => {
  if (season === "2020") return '2020-12-01';
  if (season === "2021") return '2021-10-01';
  if (season === "2022") return '2022-10-03';
  return `${season}-10-01`;
}

const POINTS_FOR_VICTORY = [0, 16, 24];
//const POINTS_FOR_VICTORY = [1];
module.exports.POINTS_FOR_VICTORY = POINTS_FOR_VICTORY;

// How many points for a defeat ? (matches n-1, n-2 and n-3 are taken into consideration)
const POINTS_FOR_DEFEAT = [0, 16, 24];
//const POINTS_FOR_DEFEAT = [41];
module.exports.POINTS_FOR_DEFEAT = POINTS_FOR_DEFEAT;

// Only bet if a score reaches a minimum
const BET_IF_SCORE_SUPERIOR_TO = [0, 30, 60];
//const BET_IF_SCORE_SUPERIOR_TO = [175];
module.exports.BET_IF_SCORE_SUPERIOR_TO = BET_IF_SCORE_SUPERIOR_TO;

// Only bet if a score reaches a minimum
//const BET_IF_ODD_SUPERIOR_TO = [1.8, 2.1, 2.2, 2.3];
const BET_IF_ODD_SUPERIOR_TO = [1.8, 2, 2.2, 2.4, 2.6];
module.exports.BET_IF_ODD_SUPERIOR_TO = BET_IF_ODD_SUPERIOR_TO;

const POINTS_PER_GOAL_SCORED = [-1, -0.5, 0, 0.5];
module.exports.POINTS_PER_GOAL_SCORED = POINTS_PER_GOAL_SCORED;

const POINTS_PER_GOAL_TAKEN = [-1, -0.5, 0, 0.5];
module.exports.POINTS_PER_GOAL_TAKEN = POINTS_PER_GOAL_TAKEN;

// Weight for match n-1, n-2, and n-3
//const FACTOR_M1 = [1, 2];
const FACTOR_M1 = [1, 2];
module.exports.FACTOR_M1 = FACTOR_M1;

//const FACTOR_M2 = [0, 1];
const FACTOR_M2 = [1];
module.exports.FACTOR_M2 = FACTOR_M2;

const FACTOR_M3 = [1];
//const FACTOR_M3 = [1.0];
module.exports.FACTOR_M3 = FACTOR_M3;

const VICTORIES_RATE_BONUS = [0];
//const VICTORIES_RATE_BONUS = [40];
module.exports.VICTORIES_RATE_BONUS = VICTORIES_RATE_BONUS;

const HOME_BONUS = [0];
//const HOME_BONUS = [20];
module.exports.HOME_BONUS = HOME_BONUS;

const BET_ON_DRAW = [false];
//const BET_ON_DRAW = [true];
module.exports.BET_ON_DRAW = BET_ON_DRAW;

const DO_NOT_BET_ON_TEAMS = {};
module.exports.DO_NOT_BET_ON_TEAMS = DO_NOT_BET_ON_TEAMS;

module.exports.getNumSimulationsPerProcess = () => {
  /*
    Do not count POINTS_FOR_VICTORY because there is
    one process per season per POINTS_FOR_VICTORY
  */
  return  POINTS_FOR_DEFEAT.length *
  BET_IF_SCORE_SUPERIOR_TO.length *
  BET_IF_ODD_SUPERIOR_TO.length *
  POINTS_PER_GOAL_SCORED.length *
  POINTS_PER_GOAL_TAKEN.length *
  FACTOR_M1.length *
  FACTOR_M2.length *
  FACTOR_M3.length *
  VICTORIES_RATE_BONUS.length *
  HOME_BONUS.length *
  BET_ON_DRAW.length;
}

module.exports.bestConfig = {
  "pointsForVictory": 16,
  "pointsForDefeat": 0,
  "betIfScoreSuperiorTo": 0,
  "betIfOddSuperiorTo": 2.4,
  "pointsPerGoalScored": 0,
  "pointsPerGoalTaken": 0,
  "factorM1": 1,
  "factorM2": 0,
  "factorM3": 1,
  "victoriesRateBonus": 0,
  "homeBonus": 0,
  "betOnDraw": false
};
