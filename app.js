const fs = require("fs");
const http = require("http");
const express = require("express");

const app = express();
const router = express.Router();

app.use(router);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const getNumberOfBets = (output) => {
  const filteredAndSorted = output
    .filter(o => !o.day && o.outcome)
    .sort((o, p) => o.date > p.date ? 1 : -1);
  return filteredAndSorted;
}

const getStats = (year, ls) => {
  const weeks = {};
  const season = {
    net: 0,
    bets: 0,
    looses: 0,
    wins: 0,
    winsOdds: 0,
    winsNet: 0,
  };
  const start = `${year}-08-01`;
  for (let i = 0; i < 320; i += 1) {
    const d = new Date(new Date(start).getTime() + (24 * 60 * 60 * 1000) * i);
    if (d.getDay() === 2) {
      weeks[d.toISOString().slice(0, 10)] = {
        [`/api/total/${year}`]: { net: 0, bets: 0, wins: 0, looses: 0, winsOdds: 0 },
      };
    }
  }
  
  ls.forEach(league => {
    const bettingDays = getNumberOfBets(results[league].output);
    bettingDays.forEach(a => {
      const index = Object.keys(weeks).findIndex(w => new Date(w).getTime() >= new Date(a.date).getTime());
      const endOfWeek = Object.keys(weeks)[index];
      if (!weeks[endOfWeek][league])
        weeks[endOfWeek][league] = {
          net: 0,
          bets: 0,
          looses: 0,
          wins: 0,
          winsOdds: 0,
          winsNet: 0,
        };
      if (a.net > 0) {
        weeks[endOfWeek][league].wins += 1;
        weeks[endOfWeek][league].winsOdds += a.net;
        weeks[endOfWeek][league].winsNet += a.net;
        weeks[endOfWeek][`/api/total/${year}`].winsOdds += a.net;
        season.winsOdds += a.net;
        weeks[endOfWeek][`/api/total/${year}`].winsNet += a.net;
        season.winsNet += a.net;
        weeks[endOfWeek][`/api/total/${year}`].wins += 1;
        season.wins += 1;
      } else {
        weeks[endOfWeek][league].looses += 1;
        weeks[endOfWeek][`/api/total/${year}`].looses += 1;
        season.looses += 1;
      }
      weeks[endOfWeek][league].bets += 1;
      weeks[endOfWeek][league].net += Math.round(1000 * a.net) / 1000;
      weeks[endOfWeek][`/api/total/${year}`].bets += 1;
      season.bets += 1;
      weeks[endOfWeek][`/api/total/${year}`].net += Math.round(1000 * a.net) / 1000;
      season.net += Math.round(1000 * a.net) / 1000;
    });
  });

  return { weeks, season };
};

const results = {};
const statsWeekly = {};
const statsSeaon = {};
const leagues = {
  "/api/premiere/2021": ['premiere', '2021'],
  "/api/premiere/2022": ['premiere', '2022'],
  "/api/championship/2021": ['championship', '2021'],
  "/api/championship/2022": ['championship', '2022'],
  "/api/leagueone/2021": ['leagueone', '2021'],
  "/api/leagueone/2022": ['leagueone', '2022'],
  "/api/leaguetwo/2021": ['leaguetwo', '2021'],
  "/api/leaguetwo/2022": ['leaguetwo', '2022'],
  "/api/bundesliga/2021": ['bundesliga', '2021'],
  "/api/bundesliga/2022": ['bundesliga', '2022'],
  "/api/bundesliga2/2021": ['bundesliga2', '2021'],
  "/api/bundesliga2/2022": ['bundesliga2', '2022'],
  "/api/eredivisie/2021": ['eredivisie', '2021'],
  "/api/eredivisie/2022": ['eredivisie', '2022'],
  "/api/oneliga/2021": ['oneliga', '2021'],
  "/api/oneliga/2022": ['oneliga', '2022'],  
  "/api/serieb/2021": ['serieb', '2021'],
  "/api/serieb/2022": ['serieb', '2022'],
  "/api/seriea/2021": ['seriea', '2021'],
  "/api/seriea/2022": ['seriea', '2022'],
  "/api/liga/2021": ['liga', '2021'],
  "/api/liga/2022": ['liga', '2022'],
  "/api/bundesliga2/2021": ['bundesliga2', '2021'],
  "/api/bundesliga2/2022": ['bundesliga2', '2022'],

  /* "/api/seriea/2021": ['seriea', '2021'],
  "/api/seriea/2022": ['seriea', '2022'],
  "/api/primeira/2021": ['primeira', '2021'],
  "/api/primeira/2022": ['primeira', '2022'],
  "/api/superlig/2021": ['superlig', '2021'],
  "/api/superlig/2022": ['superlig', '2022'],
  "/api/superliga/2021": ['superliga', '2021'],
  "/api/superliga/2022": ['superliga', '2022'],
  "/api/jupiler/2021": ['jupiler', '2021'],
  "/api/jupiler/2022": ['jupiler', '2022'],  */ 
}

const getOutput = (league, year) => {
  let run = require('./runSoccer.js').run;
  if (league.includes('nba')) {
    run = require('./run.js').run;
  }
  let bestConfig = require(`./${league}/env.js`).bestConfig;
  if (bestConfig[year]) {
    bestConfig = bestConfig[year]
  }
  const getStartDate = require(`./${league}/env.js`).getStartDate;
  let games = JSON.parse(fs.readFileSync(`./${league}/games-wh-${year}.json`, 'utf8'));
  const r = run(
    games,
    null,
    bestConfig.pointsForVictory,
    bestConfig.pointsForDefeat,
    bestConfig.betIfScoreSuperiorTo,
    bestConfig.betIfOddSuperiorTo,
    bestConfig.pointsPerGoalScored,
    bestConfig.pointsPerGoalTaken,
    bestConfig.factorM1,
    bestConfig.factorM2,
    bestConfig.factorM3,
    bestConfig.victoriesRateBonus,
    bestConfig.homeBonus,
    bestConfig.betOnDraw,
    year,
    getStartDate(year),
    280,
    bestConfig.doNotBetOnTeams,
    false
  );

  return {
    ...r,
    output: JSON.parse(`{ "a": [${r.output
      .replace(/\r/gm,'')
      .replace(/\u001b\[.*?m/g,'')
    }] }`).a
  }
}
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Request-Method",
    "POST, GET, PUT, DELETE, PATCH"
  );
  res.header(
    "Access-Control-Allow-Headers",
    "Accept, Content-Type, Authorization, X-Requested-With"
  );
  res.header("Strict-Transport-Security", "max-age=63072000");
  next();
});

let i = 0;
setInterval(() => {
  if (!Object.keys(leagues)[i]) {
    i = 0;
  }
  const r = getOutput(
    leagues[Object.keys(leagues)[i]][0],
    leagues[Object.keys(leagues)[i]][1]
  );
  results[Object.keys(leagues)[i]] = r;
  console.log('Processing ' + Object.keys(leagues)[i]);
  if (Object.keys(results).length === Object.keys(leagues).length) {
    console.log('All those leagues were processed :')
    Object.keys(leagues).forEach(l => {
      console.log(l);
    });
    const stats2022 = getStats(
      2022,
      Object.keys(leagues).filter((a) => a.includes('2022'))
    )
    statsWeekly[2022] = stats2022.weeks;
    statsSeaon[2022] = stats2022.season;

    results['/api/total/2022'] = statsWeekly[2022]['/api/total/2022'];
    const stats2021 = getStats(
      2021,
      Object.keys(leagues).filter((a) => a.includes('2021'))
    );
    statsWeekly[2021] = stats2021.weeks;
    statsSeaon[2021] = stats2021.season;
    results['/api/total/2021'] = statsWeekly[2021]['/api/total/2021'];
  }
  i += 1;
}, 1000);

app.use('/stats/2021', (req, res) => {
  res.json(statsWeekly[2021]);
});
app.use('/stats/2022', (req, res) => {
  res.json(statsWeekly[2022]);
});

app.use('/api/total/2021', (req, res) => {
  res.json({
    results: statsSeaon[2021],
    // we don't want to display each bet
    output: []
  });
});
app.use('/api/total/2022', (req, res) => {
  res.json({
    results: statsSeaon[2022],
    // we don't want to display each bet
    output: []
  });
});
Object.keys(leagues).forEach(l => {
  app.use(l, (req, res) => {
    if (!results[l]) {
      res.sendStatus(404);
      return;
    }
    res.json(results[l]);
  });
});

const start = () => {
  console.log('express.app created !')
  port = process.env.PORT || "3000";
  app.set("port", port);
  console.log(`port is ${port} !`)
  
  /**
   * Create HTTP server.
  */
  const server = http.createServer(app);
  
  /**
   * Listen on provided port, on all network interfaces.
  */
  server.listen(port);
  server.on("error", (err) => {
    console.error(err)
  });
  server.on("listening", () => {
    var addr = server.address();
    var bind = typeof addr === "string" ? "pipe " + addr : "port " + addr.port;
  });
}
start();