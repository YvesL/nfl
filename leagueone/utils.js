module.exports.getProcessArgv = (param) => {
  const index = process.argv.findIndex((arg) => arg === param);
  if (index === -1) {
    return undefined;
  }

  return process.argv[index + 1];
};

module.exports.rightPad = (str, length) => {
  let s = str;
  for (let i = 0; i <= length - str.length; i += 1) {
    s += " ";
  }
  return s;
};

module.exports.IGNORE_OP = {
  'all': [],
};

module.exports.teamsCompleteName = {
  "Wigan": "Wigan",
  "Accrington": "Accrington",
  "Sheffield Wed": "Sheffield Wed",
  "Sheffield Wednesday": "Sheffield Wed",
  "Bristol Rovers": "Bristol Rovers",
  "Fleetwood": "Fleetwood",
  "Fleet Town": "Fleetwood",
  "Charlton": "Charlton",
  "Charlton Athletic": "Charlton",
  "Burton": "Burton",
  "Burton Albion": "Burton",
  "Cambridge Utd": "Cambridge Utd",
  "Cambridge United": "Cambridge Utd",
  "Bolton": "Bolton",
  "Doncaster": "Doncaster",
  "Exeter": "Exeter",
  "Exeter City": "Exeter",
  "Peterborough": "Peterborough",
  "Peterborough United": "Peterborough",
  "Forest Green": "Forest Green",
  "Wycombe": "Wycombe",
  "Wycombe Wanderers": "Wycombe",
  "Ipswich": "Ipswich",
  "Cheltenham": "Cheltenham",
  "Cheltenham Town": "Cheltenham",
  "Plymouth": "Plymouth",
  "Lincoln": "Lincoln",
  "Derby": "Derby",
  "Derby County": "Derby",
  "MK Dons": "MK Dons",
  "Milton Keynes Dons": "MK Dons",
  "Morecambe": "Morecambe",
  "Portsmouth": "Portsmouth",
  "Oxford Utd": "Oxford Utd",
  "Oxford": "Oxford Utd",
  "Port Vale": "Port Vale",
  "Shrewsbury": "Shrewsbury",
  "Barnsley": "Barnsley",
  "Shrewsbury": "Shrewsbury",
  "AFC Wimbledon": "AFC Wimbledon",
  "Gillingham": "Gillingham",
  "Rotherham": "Rotherham",
  "Crewe": "Crewe",
  "Sunderland": "Sunderland",
  "Blackpool": "Blackpool",
  "Bradford City": "Bradford City",
  "Rochdale": "Rochdale",
  "Coventry": "Coventry",
  "Luton": "Luton",
  "Scunthorpe": "Scunthorpe",
  "Walsall": "Walsall",
  "Southend": "Southend",
  "Tranmere": "Tranmere",
  "Tranmere": "Tranmere",
  "Tranmere": "Tranmere",
  "Tranmere": "Tranmere",
};
