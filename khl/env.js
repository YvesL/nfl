const startOfSeason = (season) => {
  console.log("startOfSeason", season);
  if (season === "2020") {
    return "08-30";
  }
  if (season === "2021") {
    return "08-30";
  }
  if (season === "2022") {
    return "08-24";
  }
  throw new Error("unknown season");
};

module.exports.getStartDate = (season) =>
  `${parseInt(season) - 1}-${startOfSeason(season)}`;

module.exports.POINTS_FOR_VICTORY = [35, 40, 45, 55, 60, 65, 70, 75];

// How many points for a defeat ? (matches n-1, n-2 and n-3 are taken into consideration)
//module.exports.POINTS_FOR_DEFEAT = [12, 14, 15, 16, 18];
module.exports.POINTS_FOR_DEFEAT = [5, 10, 15, 20, 25, 30];

// Only bet if a score reaches a minimum
/* module.exports.BET_IF_SCORE_SUPERIOR_TO = [
  18, 20, 22, 24, 26, 28, 30, 50, 70, 90, 110, 130,
]; */
module.exports.BET_IF_SCORE_SUPERIOR_TO = [80, 120, 130, 140, 150, 170];

// Only bet if a score reaches a minimum
module.exports.BET_IF_ODD_SUPERIOR_TO = [1.1, 1.2, 1.3];
//module.exports.BET_IF_SCORE_SUPERIOR_TO = [22, 24];

// Take into consideration the goal diff
// if diff factor is 2 and the score is 10-20 (defeat),
// there is a negative bonus of -10*2 = -20 for current team (looser team)
//module.exports.GOAL_DIFF_FACTOR = [1.7, 1.8, 1.9, 2.0, 2.2, 2.4, 2.6];
module.exports.GOAL_DIFF_FACTOR = [4, 6, 8, 10, 12, 14, 16];

// Weight for match n-1, n-2, and n-3
module.exports.FACTOR_M1 = [1, 1.1, 1.2, 1.3];
//module.exports.FACTOR_M1 = [4.1, 4.2];

module.exports.FACTOR_M2 = [0.9, 1, 1.1];
//module.exports.FACTOR_M2 = [1.2, 1.4];

module.exports.FACTOR_M3 = [0.9, 1, 1.1];
//module.exports.FACTOR_M3 = [1.0, 1.2, 1.6];

module.exports.bestConfig = {
  pointsForVictory: 55,
  pointsForDefeat: 20,
  betIfScoreSuperiorTo: 200,
  goalDiffFactor: 14,
  factorM1: 1.3,
  factorM2: 1,
  factorM3: 1,
};
