const Writable = require("stream").Writable;
const readline = require("readline");

module.exports.getProcessArgv = (param) => {
  const index = process.argv.findIndex((arg) => arg === param);
  if (index === -1) {
    return undefined;
  }

  return process.argv[index + 1];
};

module.exports.rightPad = (str, length) => {
  let s = str;
  for (let i = 0; i <= length - str.length; i += 1) {
    s += " ";
  }
  return s;
};

const teams2021 = [
  "TRAKTOR CHELYABINSK",
  "SALAVAT YULAEV UFA",
  "LOKOMOTIV YAROSLAVL",
  "ADMIRAL VLADIVOSTOK",
  "SEVERSTAL CHEREPOVETS",
  "KUNLUN RED STAR",
  "NEFTEKHIMIK",
  "AMUR KHABAROVSK",
  "DINAMO MINSK",
  "BARYS NUR SULTAN",
  "DINAMO RIGA",
  "METALLURG MAGNITOGORSK",
  "SKA SAINT PETERSBURG",
  "JOKERIT",
  "AK BARS KAZAN",
  "HC DYNAMO MOSCOW",
  "TORPEDO NIZHNY NOVGOROD",
  "AVANGARD OMSK",
  "HC SOCHI",
  "VITYAZ PODOLSK",
  "SPARTAK MOSCOW",
  "AVTOMOB. YEKATERINBURG",
  "CSKA MOSCOW",
  "SIBIR NOVOSIBIRSK",
  "BOBROVA",
  "TARASOVA",
  "KHARLAMOVA",
];

const TEAMS = {
  2019: teams2021,
  2020: teams2021.filter(
    (t) => !["BOBROVA", "TARASOVA", "KHARLAMOVA"].includes(t)
  ),
  2021: teams2021.filter(
    (t) =>
      !["ADMIRAL VLADIVOSTOK", "BOBROVA", "TARASOVA", "KHARLAMOVA"].includes(t)
  ),
  2022: teams2021.filter(
    (t) => !["BOBROVA", "TARASOVA", "KHARLAMOVA"].includes(t)
  ),
};
module.exports.TEAMS = TEAMS;

module.exports.simplePrompt = (text) => {
  return new Promise((resolve) => {
    var mutableStdout = new Writable({
      write: function (chunk, encoding, callback) {
        if (!this.muted) process.stdout.write(chunk, encoding);
        callback();
      },
    });

    mutableStdout.muted = false;

    var rl = readline.createInterface({
      input: process.stdin,
      output: mutableStdout,
      terminal: true,
    });

    rl.question(text, function () {
      resolve("");
      console.log("");
      rl.close();
    });

    mutableStdout.muted = true;
  });
};

module.exports.teamsCompleteName = {
  "Traktor Chelyabinsk": "TRAKTOR CHELYABINSK",
  "traktor-chelyabinsk": "TRAKTOR CHELYABINSK",
  tra: "TRAKTOR CHELYABINSK",
  "Salavat Yulaev": "SALAVAT YULAEV UFA",
  "salavat-yulaev-ufa": "SALAVAT YULAEV UFA",
  sal: "SALAVAT YULAEV UFA",
  "Lokomotiv Yaroslavl": "LOKOMOTIV YAROSLAVL",
  "lokomotiv-yaroslavl": "LOKOMOTIV YAROSLAVL",
  yar: "LOKOMOTIV YAROSLAVL",
  "Admiral Vladivostok": "ADMIRAL VLADIVOSTOK",
  "Severstal Tcherepovets": "SEVERSTAL CHEREPOVETS",
  "Severstal Cherepovec": "SEVERSTAL CHEREPOVETS",
  "severstal-cherepovets": "SEVERSTAL CHEREPOVETS",
  che: "SEVERSTAL CHEREPOVETS",
  "Kunlun Red Star": "KUNLUN RED STAR",
  "kunlun-red-star": "KUNLUN RED STAR",
  rsk: "KUNLUN RED STAR",
  "Neftehimik Niznekamsk": "NEFTEKHIMIK",
  "neftekhimik-nizhnekamsk": "NEFTEKHIMIK",
  nef: "NEFTEKHIMIK",
  "Amur Khabarovsk": "AMUR KHABAROVSK",
  "amur-khabarovsk": "AMUR KHABAROVSK",
  amu: "AMUR KHABAROVSK",
  "Dinamo Minsk": "DINAMO MINSK",
  "dinamo-minsk": "DINAMO MINSK",
  min: "DINAMO MINSK",
  "Barys Nur-Sultan": "BARYS NUR SULTAN",
  "barys-astana": "BARYS NUR SULTAN",
  "Dinamo Riga": "DINAMO RIGA",
  "dinamo-riga": "DINAMO RIGA",
  rig: "DINAMO RIGA",
  "Metallurg Magnitogorsk": "METALLURG MAGNITOGORSK",
  "metallurg-magnitogorsk": "METALLURG MAGNITOGORSK",
  mem: "METALLURG MAGNITOGORSK",
  "SKA St-Pétersbourg": "SKA SAINT PETERSBURG",
  "ska-st-petersburg": "SKA SAINT PETERSBURG",
  ska: "SKA SAINT PETERSBURG",
  "Jokerit Helsinki": "JOKERIT",
  "jokerit-helsinki": "JOKERIT",
  jok: "JOKERIT",
  "Ak Bars Kazan": "AK BARS KAZAN",
  "ak-bars-kazan": "AK BARS KAZAN",
  bar: "AK BARS KAZAN",
  "Dynamo Moscou": "HC DYNAMO MOSCOW",
  "dynamo-moscow": "HC DYNAMO MOSCOW",
  dyn: "HC DYNAMO MOSCOW",
  "Torpedo Nizhny Novgorod": "TORPEDO NIZHNY NOVGOROD",
  "torpedo-nizhny-novgorod": "TORPEDO NIZHNY NOVGOROD",
  tor: "TORPEDO NIZHNY NOVGOROD",
  "Avangard Omsk": "AVANGARD OMSK",
  "avangard-omsk": "AVANGARD OMSK",
  oms: "AVANGARD OMSK",
  Sotchi: "HC SOCHI",
  sochi: "HC SOCHI",
  soc: "HC SOCHI",
  Vityaz: "VITYAZ PODOLSK",
  podolsk: "VITYAZ PODOLSK",
  vit: "VITYAZ PODOLSK",
  "Spartak Moscou": "SPARTAK MOSCOW",
  "hc-spartak": "SPARTAK MOSCOW",
  spa: "SPARTAK MOSCOW",
  "Avtomobilist Ekaterinbourg": "AVTOMOB. YEKATERINBURG",
  "avtomobilist-yekaterinburg": "AVTOMOB. YEKATERINBURG",
  avt: "AVTOMOB. YEKATERINBURG",
  "CSKA Moscou": "CSKA MOSCOW",
  "cska-moscow": "CSKA MOSCOW",
  csk: "CSKA MOSCOW",
  "Sibir Novosibirsk": "SIBIR NOVOSIBIRSK",
  "sibir-novosibirsk": "SIBIR NOVOSIBIRSK",
  sib: "SIBIR NOVOSIBIRSK",
  bobrova: "BOBROVA",
  tarasova: "TARASOVA",
  kharlamova: "KHARLAMOVA",
};
