const fs = require("fs");
const https = require("https");
const teamsCompleteName = require("./utils").teamsCompleteName;
const getProcessArgv = require("./utils").getProcessArgv;
const getStartDate = require("./env").getStartDate;

const HEADER_REFERER = "https://oddspedia.com/ice-hockey/russia/khl";
const HEADER_AUTHORITY = "oddspedia.com";
const QS_SPORT = "ice-hockey";
const QS_LEAGUE = "khl";
const QS_CATEGORY = "russia";
const QS_GEO_CODE = "";

const getHeaders = () => {
  const randomGa = Math.round(Math.random() * 1000);
  const randomGa2 = Math.round(Math.random() * 1000);
  return {
    accept: "application/json, text/plain, */*",
    "accept-encoding": "utf8",
    "accept-language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7",
    referer: HEADER_REFERER,
    "sec-ch-ua":
      '"Chromium";v="92", " Not A;Brand";v="99", "Google Chrome";v="92"',
    "sec-ch-ua-mobile": "?0",
    "sec-fetch-dest": "empty",
    "sec-fetch-mode": "cors",
    "sec-fetch-site": "same-origin",
    "user-agent":
      "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36" +
      Math.round() * 10,
    authority: HEADER_AUTHORITY,
    cookie: `timezone=d2023e6487da73444065d9d7487e1d9aNsyTsYo3kiJLifLxepWp18FccQoVa36poHxd9PNPLe6aIzJ5Yfl50sKeJGOItuVErhVi5ue%2BJV36gJAiwnB8sg%3D%3D; country=4e348f56482ea0e58a1d72abdf120b80hmhRkzcPGYZgRyIZDQ%2BjfVzOADdyp7pJCFwbgTeQewpIC0IQdJN1CLmzMRbBK78cnrCG1NI1J1TewsPPkIcc7w%3D%3D; odds_format=8a0e50d55988c4e9c424e2ab01a6341f66GU0dcmk2doEx%2Fy56jICii3ChjINy93p4la%2FKMpMflA8DsU3cirKwabLc5mNc7EkQG7eYOiZIwr1B7IyPvprg%3D%3D; first_visit=a68545ee96a86b0651a98636f6877771%2Fp75CM8BRjrLHhQXoo%2FUhlqQ0u%2Fq1%2FkL4d25dZX9UQDt55u2AaXMl4NCguvOE2eTpG%2BjbzCdayGgANz2o8yg7A%3D%3D; country_state=fd89ee81e6b64f72c145b737b796a83d%2FoAyjyNKQuLMyiLZ8Z3cu7RaWPQYLhe562h9ke1STnPL1Nhw36sA71SObswffZ25x5DUX2Y0orBbASdFdiIbnA%3D%3D`,
  };
};

const mustIgnoreGame = (game) => {
  if (game.league_round_name && game.league_round_name.includes("Playoffs")) {
    return true;
  }
  if (["Postponed", "Canceled"].includes(game.special_status)) {
    return true;
  }
  if (["Quarterfinal", "Semifinal", "Final"].includes(game.league_round_name)) {
    return true;
  }
  return false;
};

const SEASON = getProcessArgv("--season");
const daysToRun = parseInt(getProcessArgv("--days"));
const games = JSON.parse(fs.readFileSync(`${__dirname}/games-${SEASON}.json`));
const startDateSeason = getStartDate(SEASON);
if (
  !SEASON ||
  typeof daysToRun !== "number" ||
  typeof startDateSeason !== "string"
) {
  throw new Error("need --season ex:2021 --days ex:250 parameters");
}
const day = 1000 * 60 * 60 * 24;

console.log("startDateSeason", startDateSeason);

// at_id 9784
const cotes = {};
let i = 0;
let n = 0;
let retries = 0;
const daysPerRequest = 10;
const main = async (i) => {
  await new Promise((r) => setTimeout(r, 1000));
  /* if (i > 0) {
    console.log(JSON.stringify(cotes, null, 1));
    return;
  } */
  // example: 2021-12-01:00:00
  const startDate = new Date(
    new Date(startDateSeason).getTime() + i * day * daysPerRequest
  ).toISOString();
  // example: 2021-12-11:00:00
  const endDate = new Date(
    new Date(startDate).getTime() + day * daysPerRequest
  ).toISOString();
  console.log(startDate, "- until -", endDate);

  // GAMES
  const qsGames = {
    sport: QS_SPORT,
    league: QS_LEAGUE,
    category: QS_CATEGORY,
    excludeSpecialStatus: "0",
    sortBy: "0",
    perPageDefault: "100",
    startDate,
    endDate,
    geoCode: QS_GEO_CODE,
    status: "all",
    popularLeaguesOnly: "0",
    round: "",
    page: "1",
    perPage: "100",
    language: "en",
  };
  const pathGames = `/api/v1/getMatchList?${new URLSearchParams(
    qsGames
  ).toString()}`;
  const s1 = await new Promise((resolve, reject) => {
    const req = https.get(
      {
        headers: getHeaders(),
        host: "oddspedia.com",
        path: pathGames,
      },
      (res) => {
        console.log("res.statusCode (games)", res.statusCode);
        if (res.statusCode !== 200) {
          if (retries < 10) {
            retries += 1;
            console.log(`status code not 200 (${res.statusCode}), will retry`);
            main(i);
            return;
          } else {
            console.log(res.statusCode);
            console.log(res.responseText);
            throw new Error("status code not 200");
          }
        }
        let d = "";
        res.on("data", (a) => {
          d += a.toString("utf8");
        });
        res.on("end", () => {
          resolve(d);
        });
      }
    );
    req.end();
  });

  const json1 = JSON.parse(s1);
  const gameIds = json1.data.matchList.map((a) => a.id);
  if (gameIds.length > 99) {
    console.log(`Period: ${startDate} - ${endDate}`);
    throw new Error(
      "received more than 99 games, maybe some game are missed, reduce period"
    );
  }
  // ODDS
  const qsOdds = {
    sport: QS_SPORT,
    category: QS_CATEGORY,
    league: QS_LEAGUE,
    geoCode: QS_GEO_CODE,
    geoState: "",
    wettsteuer: "0",
    // must do this to be sure to get all odds
    startDate, //: new Date(new Date(startDate).getTime() - day * 4).toISOString(),
    endDate, //: new Date(new Date(startDate).getTime() + day * 4).toISOString(),
    ot: "200",
    inplay: "0",
    hasActiveOdds: "0",
    language: "en",
  };
  const pathOdds = `/api/v1/getMaxOdds?${new URLSearchParams(
    qsOdds
  ).toString()}`;
  const s2 = await new Promise((resolve, reject) => {
    const req = https.get(
      {
        headers: getHeaders(),
        host: "oddspedia.com",
        //params: params,
        path: pathOdds,
      },
      (res) => {
        console.log("res.statusCode (odds)", res.statusCode);
        if (res.statusCode !== 200) {
          if (retries < 10) {
            retries += 1;
            console.log(`status code not 200 (${res.statusCode}), will retry`);
            main(i);
            return;
          } else {
            console.log(res.statusCode);
            console.log(res.responseText);
            throw new Error("status code not 200");
          }
        }
        let d = "";
        res.on("data", (a) => {
          d += a.toString("utf8");
        });
        res.on("end", () => {
          resolve(d);
        });
      }
    );
    req.end();
  });

  const json2 = JSON.parse(s2);
  if (!!json2.message && typeof json2.message[0] === "string") {
    console.log(
      `No data for period ${startDate}-${endDate}, message: ${json2.message[0]} `
    );
    if (i < daysToRun / daysPerRequest) {
      i += 1;
      main(i);
    } else {
      over();
    }
    return;
  }

  let j = 0;
  const handleGame = async () => {
    if (j === gameIds.length) {
      if (i < daysToRun / daysPerRequest) {
        i += 1;
        main(i);
      } else {
        over();
      }
      return;
    }
    const gameId = gameIds[j];
    const game = json1.data.matchList.find((m) => m.id === gameId);

    // ignore because of postponed/cancel/playoffs
    if (mustIgnoreGame(game)) {
      j += 1;
      handleGame();
      return;
    }

    if (game.md.startsWith("2020-11-16")) {
      console.log(game);
    }
    // odd is not here, the game is postponed/canceled probably
    if (!json2.data[gameId] || json2.data[gameId][0].odd_name !== "Home") {
      j += 1;
      handleGame();
      return;
    }

    let team1 = "";
    if (teamsCompleteName[game.ht_slug.toLowerCase()]) {
      team1 = teamsCompleteName[game.ht_slug.toLowerCase()];
    } else if (teamsCompleteName[game.ht_abbr.toLowerCase()]) {
      team1 = teamsCompleteName[game.ht_abbr.toLowerCase()];
    }

    let team2 = "";
    if (teamsCompleteName[game.at_slug.toLowerCase()]) {
      team2 = teamsCompleteName[game.at_slug.toLowerCase()];
    } else if (teamsCompleteName[game.at_abbr.toLowerCase()]) {
      team2 = teamsCompleteName[game.at_abbr.toLowerCase()];
    }

    if (!Object.values(teamsCompleteName).find((abb) => abb === team1)) {
      throw new Error(`Team ${game.ht_slug} not found`);
    }
    if (!Object.values(teamsCompleteName).find((abb) => abb === team2)) {
      throw new Error(`Team ${game.at_slug} not found`);
    }
    const offset = new Date().getTimezoneOffset();
    const offsetInMs = -(offset * 60 * 1000);
    let date = new Date(new Date(game.md).getTime() + offsetInMs)
      .toISOString()
      .slice(0, 10);
    if (typeof date !== "string") {
      console.log(game);
      throw new Error("Invalid date for game");
    }
    if (date.length !== 10) {
      console.log(game);
      throw new Error("Invalid date for game");
    }

    if (game.special_status) {
      throw new Error(
        `Game ${date} ${team1} - ${team2} Game has a unhandled special status "${game.special_status}"`
      );
    }

    if (!games[team1][date]) {
      console.log(game);
      throw new Error(
        `Game ${date} ${team1} - ${team2} does not exist in games-${SEASON}.${team1} file`
      );
    }
    if (!games[team2][date]) {
      console.log(game);
      throw new Error(
        `Game ${date} ${team1} - ${team2} does not exist in games-${SEASON}.${team2} file`
      );
    }
    console.log(`date: ${date}, game: ${team1} - ${team2}`);
    const cote1 = parseFloat(json2.data[gameId][0].value, 10);
    const cote2 = parseFloat(json2.data[gameId][1].value, 10);
    if (typeof cote1 !== "number" || typeof cote2 !== "number") {
      throw new Error(`Game ${date} ${team1} - ${team2} one odd is not number`);
    }
    if (!cotes[date]) {
      cotes[date] = [];
    }
    cotes[date].push({
      [team1]: cote1,
      [team2]: cote2,
    });
    n += 1;
    j += 1;
    handleGame();
  };
  handleGame();

  try {
    gameIds.forEach(async (gameId) => {});
  } catch (err) {
    console.log(err);
    throw new Error("");
  }
};

const over = () => {
  let missings = 0;
  Object.keys(games).forEach((team) => {
    Object.keys(games[team]).forEach((date) => {
      if (!cotes[date]) {
        cotes[date] = [];
      }
      const found = cotes[date].find((a) => Object.keys(a).includes(team));
      if (!found) {
        missings += 1;
        console.warn(
          `${date} ${team}-${games[team][date].op} missing game, adding had odds 1.2,1.2`
        );
        cotes[date].push({
          [team]: 1.2,
          [games[team][date].op]: 1.2,
        });
      }
    });
  });
  console.log("retries due to failed requests: " + retries);
  console.log("missing games (odd not found): " + missings);
  console.log("over, " + n + " games processed");
  fs.writeFileSync(
    `${__dirname}/cotes-${SEASON}.json`,
    JSON.stringify(cotes, null, 2),
    "utf8"
  );
};
main(i);

const findTrueDateOfPostponnedGame = (date, team1, team2) => {
  return new Promise((resolve, reject) => {
    const init = new Date(new Date(date).getTime() - day * 20)
      .toISOString()
      .slice(0, 10);
    let found = false;
    for (let i = 0; i < 200; i += 1) {
      if (found) {
        return;
      }
      let d = new Date(new Date(init).getTime() + day * i)
        .toISOString()
        .slice(0, 10);
      if (games[team1][d] && games[team1][d].op === team2) {
        found = true;
        resolve(d);
      }
      if (i === 199) {
        console.log(
          `Could not find real date of postponned event ${date} ${team1} - ${team2}`
        );
        process.exit();
      }
    }
  });
};

const findOdd = async (
  qsOdds,
  qsGames,
  date,
  gameId,
  geoCode,
  ht_abbr,
  at_abbr
) => {
  const startDate = new Date(new Date(date).getTime() - day * 2).toISOString();
  const endDate = new Date(new Date(date).getTime() + day * 2).toISOString();
  console.log(`Will look ${startDate} - until - ${endDate}`);
  const pathGames = `/api/v1/getMatchList?${new URLSearchParams({
    ...qsGames,
    geoCode: geoCode,
    startDate,
    endDate,
  }).toString()}`;
  const s1 = await new Promise((resolve, reject) => {
    const req = https.get(
      {
        headers: getHeaders(),
        host: "oddspedia.com",
        //params: params,
        path: pathGames,
      },
      (res) => {
        console.log("res.statusCode (findOdd, games)", res.statusCode);
        if (res.statusCode !== 200) {
          resolve(undefined);
          return;
        }
        let d = "";
        res.on("data", (a) => {
          d += a.toString("utf8");
        });
        res.on("end", () => {
          resolve(d);
        });
      }
    );
    req.end();
  });
  if (!s1) {
    return undefined;
  }
  const json1 = JSON.parse(s1);
  const game1 = json1.data.matchList.find((g) => {
    return g.ht_abbr === ht_abbr && g.at_abbr === at_abbr;
  });
  const game2 = json1.data.matchList.find((g) => {
    return g.at_abbr === ht_abbr && g.ht_abbr === at_abbr;
  });
  if (!game1 && !game2) {
    return undefined;
  }
  if (game2) {
    console.log(
      `Found postponned game ${date} ${ht_abbr}-${at_abbr} but home/away is inverted`
    );
  }

  // find the new gameId
  const pathOdds = `/api/v1/getMaxOdds?${new URLSearchParams({
    ...qsOdds,
    geoCode: geoCode,
    startDate: startDate,
    endDate: endDate,
  }).toString()}`;
  const s2 = await new Promise((resolve, reject) => {
    const req = https.get(
      {
        headers: getHeaders(),
        host: "oddspedia.com",
        //params: params,
        path: pathOdds,
      },
      (res) => {
        console.log("res.statusCode (findOdd, odds)", res.statusCode);
        if (res.statusCode !== 200) {
          resolve(undefined);
          return;
        }
        let d = "";
        res.on("data", (a) => {
          d += a.toString("utf8");
        });
        res.on("end", () => {
          resolve(d);
        });
      }
    );
    req.end();
  });
  if (!s2) {
    return undefined;
  }

  const json = JSON.parse(s2);
  if (game1) {
    if (!json.data[game1.id]) {
      return undefined;
    }
    return [
      { value: json.data[game1.id][0].value },
      { value: json.data[game1.id][1].value },
    ];
  } else {
    if (!json.data[game2.id]) {
      return undefined;
    }
    return [
      { value: json.data[game2.id][1].value },
      { value: json.data[game2.id][0].value },
    ];
  }
};
