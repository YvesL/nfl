module.exports.getProcessArgv = (param) => {
  const index = process.argv.findIndex((arg) => arg === param);
  if (index === -1) {
    return undefined;
  }

  return process.argv[index + 1];
};

module.exports.rightPad = (str, length) => {
  let s = str;
  for (let i = 0; i <= length - str.length; i += 1) {
    s += " ";
  }
  return s;
};

module.exports.IGNORE_OP = {
  'all': [],
};

module.exports.teamsCompleteName = {
  "Vizela": "Vizela",
  "Estoril": "Estoril",
  "Estoril Praia": "Estoril",
  "Rio Ave": "Rio Ave",
  "Braga": "Braga",
  "Famalicao": "Famalicao",
  "Portimonense": "Portimonense",
  "Arouca": "Arouca",
  "FC Arouca": "Arouca",
  "Pacos Ferreira": "Pacos Ferreira",
  "Paços Ferreira": "Pacos Ferreira",
  "FC Porto": "FC Porto",
  "Porto": "FC Porto",
  "Vitoria Guimaraes": "Vitoria Guimaraes",
  "Sporting": "Sporting CP",
  "Sporting CP": "Sporting CP",
  "Sporting Lissabon": "Sporting CP",
  "Casa Pia": "Casa Pia",
  "Maritimo": "Maritimo",
  "Benfica": "Benfica",
  "Santa Clara": "Santa Clara",
  "Chaves": "Chaves",
  "Boavista": "Boavista",
  "Gil Vicente": "Gil Vicente",
  "BSAD": "BSAD",
  "Moreirense": "Moreirense",
  "Tondela": "Tondela",
};
