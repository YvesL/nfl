const puppeteer = require("puppeteer");
const fs = require("fs");
const { parseDate, getProcessArgv } = require('./utils');

const findArg = (param) => {
  return process.argv.findIndex((arg) => arg === param) !== -1
}
const onlyFirstPage = findArg('--ofp');

let HOURS_OFFSET_PARIS = 0;
const YEARS_BASED_ON_MONTH = {
  1: {
    'Jan': 1,
    'Feb': 1,
    'Mar': 1,
    'Apr': 1,
    'May': 1,
    'Jun': 1,
  },
  2: {
    'Jan': 1,
    'Feb': 1,
    'Mar': 1,
    'Apr': 1,
    'May': 1,
    'Jun': 1,
    'Jul': 1,
    'Aug': 1,
  },
}
let YEAR_BASED_ON_MONTH;
let CURRENT_SEASON = 2022;
let SEASON = 2022;
if (findArg('--season') && ["2018", "2019", "2020", "2021"].includes(getProcessArgv('--season'))) {
  SEASON = parseInt(getProcessArgv('--season'));
}

let games;
let url;
let urlNextMatches;
let path;
let teamsCompleteName;
let league;
if (findArg('--nba')) {
  league = 'nba';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[2]
  HOURS_OFFSET_PARIS = -8;
  url = `https://www.oddsportal.com/basketball/usa/nba/results/`;
  urlNextMatches = 'https://www.oddsportal.com/basketball/usa/nba/';
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/basketball/usa/nba-2021-2022/results/'
  } else if (SEASON === 2020) {
    url = 'https://www.oddsportal.com/basketball/usa/nba-2020-2021/results/'
  }
} else if (findArg('--ncaa')) {
  league = 'ncaa';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[2]
  HOURS_OFFSET_PARIS = -8;
  url = `https://www.oddsportal.com/basketball/usa/ncaa/results/`;
  urlNextMatches = 'https://www.oddsportal.com/basketball/usa/ncaa/';
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/basketball/usa/ncaa-2021-2022/results/'
  } else if (SEASON === 2020) {
    url = 'https://www.oddsportal.com/basketball/usa/ncaa-2020-2021/results/'
  }
} else if (findArg('--nfl')) {
  league = 'nfl';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[2]
  HOURS_OFFSET_PARIS = -8;
  url = `https://www.oddsportal.com/american-football/usa/nfl/results/`;
  urlNextMatches = 'https://www.oddsportal.com/american-football/usa/nfl/';
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/american-football/usa/nfl-2021-2022/results/'
  } else if (SEASON === 2020) {
    url = 'https://www.oddsportal.com/american-football/usa/nfl-2020-2021/results/'
  }
} else {
  throw new Error('unknown league, provide for example --bundesliga')
}
teamsCompleteName = require(__dirname + `/${league}/utils`).teamsCompleteName;
path = `${__dirname}/${league}/games-wh-${SEASON}.json`;
games = JSON.parse(fs.readFileSync(path, "utf8"));

let i = 1;
let j = 0;
const go = async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  let html = '';
  if (i === 1 && SEASON === CURRENT_SEASON) {
    console.log('Processing (incoming) ' + urlNextMatches);
    await page.goto(urlNextMatches);
    await page.setViewport({
      width: 600,
      height: 3000
    });
    await new Promise((r) => {
      setTimeout(r, 2000);
    });
    html = await page.evaluate(() => {
      const html2 = document.querySelectorAll('table#tournamentTable')[0].innerHTML;
      document.body.innerHTML = '<table>' + html2 + '</table>';
      document.querySelectorAll('script').forEach(a => a.remove())
      document.querySelectorAll('.table-dummyrow').forEach(a => a.remove())
      document.querySelectorAll('.ico-event-info').forEach(a => a.remove())
      document.querySelectorAll('table tbody a').forEach(a => {
        const innerText = a.innerText;
        a.outerHTML = innerText
      });
      let s = '';
      document.querySelectorAll('tr').forEach((tr, i) => {
        if (i === 0) return;
        const ths = tr.querySelectorAll('th');
        if (ths.length) {
          ths.forEach(th => {
            s += `${th.innerText},,,`;
          });
        } else {
          const tds = tr.querySelectorAll('td');
          console.log(tds.length);
          tds.forEach((td, i) => {
            /*
              Only 5 rows when game is future (score = null)
              6 rows when game is passed
            */
            if (tds.length === 5 && i ===2) s += 'null,,,';
            s += `${td.innerText},,,`;
          });
        }
        s += '\n';
      });
      return s;
    });
    html += '\n';
    console.log(html);
  }


  console.log('Processing (past) ' + url + `#/page/${i}/`)
  await page.goto(url + `#/page/${i}/`);
  await page.setViewport({
    width: 600,
    height: 3000
  });
  await new Promise((r) => {
    setTimeout(r, 3000);
  });
  const k = i;
  setTimeout(() => {
    if (k === i) {
      console.log('stuck ?')
      console.log('stuck at ', i, "saving games");
      fs.writeFileSync(
        path,
        JSON.stringify(games, null, 2),
        'utf8'
      )
      process.exit(0);
    }
  }, 15000);
  html += await page.evaluate(() => {
    const html2 = document.querySelectorAll('div#tournamentTable')[0].innerHTML;
    document.body.innerHTML = html2;
    document.querySelectorAll('.table-dummyrow').forEach(a => a.remove())
    document.querySelectorAll('.ico-event-info').forEach(a => a.remove())
    document.querySelectorAll('#tournamentTable tbody a').forEach(a => {
      const innerText = a.innerText;
      a.outerHTML = innerText
    });
    let s = '';
    document.querySelectorAll('tr').forEach((tr, i) => {
      if (i === 0) return;
      const ths = tr.querySelectorAll('th');
      if (ths.length) {
        ths.forEach(th => {
          s += `${th.innerText},,,`;
        });
      } else {
        tr.querySelectorAll('td').forEach(td => {
          s += `${td.innerText},,,`;
        });
      }
      s += '\n';
    });
    return s;
  });
  await new Promise((r) => {
    setTimeout(r, 1000);
  });
  console.log(html);
  const lines = html.split('\n');
  let date;
  let playoffs = false;
  let dateLine = false;
  if(lines.length === 1 || (onlyFirstPage && i === 2)) {
    await browser.close();
    console.log(lines.length);
    console.log(j, 'games');
    fs.writeFileSync(
      path,
      JSON.stringify(games, null, 2),
      'utf8'
    )
    process.exit(0);
  }
  lines.forEach(l => {
    let gameDate;
    let teams;
    let team1;
    let team2;
    let score;
    let odd1;
    let oddDraw;
    let odd2;
    console.log(l)
    if (l.split(',,,').length < 2) {
      console.log('skip empty line')
      return;
    }
    l.split(',,,').forEach((a, i) => {
      if (i === 0) {
        if (a.length === 5) {
          if (!date) return;
          dateLine = false;
          const dateWithHour = `${date}T${a}`;
          gameDate = new Date(new Date(dateWithHour).getTime() + HOURS_OFFSET_PARIS * 60 * 60 * 1000).toISOString().slice(0,10)
        } else {
          dateLine = true;
          console.log('-------')
          console.log(l)
          console.log('-------')
          date = parseDate(
            a,
            0, 
            0,
            SEASON,
            YEAR_BASED_ON_MONTH
          );
          if (date === "ignore") {
            date = null;
            console.log('Skipping date ', a)
            return;
          }
          console.log('Valid date', date)
        }
      }
      if (dateLine) return;
      if (i === 1) {
        team1 = a.trim().split(' - ')[0];
        if (!teamsCompleteName[team1]) {
          if (team1 === "Team LeBron") return;
          if (team1 === "Team Durant") return;
          console.warn('--------')
          console.warn(00000, `"${team1}": "${team1}",`)
          console.warn('--------')
          throw new Error('Unknown team ' + team1)
          process.exit(1)
        }
        team1 = teamsCompleteName[team1];
        team2 = a.trim().split(' - ')[1];
        if (!teamsCompleteName[team2]) {
          if (team2 === "Team LeBron") return;
          if (team2 === "Team Durant") return;
          console.warn('--------')
          console.warn(00000, `"${team2}": "${team2}",`)
          console.warn('--------')
          throw new Error('Unknown team ' + team2)
          process.exit(1)
        }
        team2 = teamsCompleteName[team2];
        teams = [team1, team2]
      }
      if (i === 2) {
        if (a.includes('post')) {
          playoffs = true;
          console.log('skip because of postponed')
          return;
        }
        if (a.includes('canc')) {
          playoffs = true;
          console.log('skip because of canceled')
          return;
        }
        if (a.includes('Leg')) {
          playoffs = true;
          console.log('skip because of Leg')
          return;
        }
        if (a.includes('Play')) {
          playoffs = true;
          console.log('skip because of Playoffs')
          return;
        }
        if (a.includes('Champion')) {
          playoffs = true;
          console.log('skip because of Championship')
          return;
        }
        playoffs = false;
        console.log(i, a);
        if (
          a.includes('null') ||
          !a.includes(":")
          ) { 
            console.log('Future game');
            score = null;
        } else {
          score = a.split(':').map(a => parseInt(a, 10));
          console.log(score)
          score.forEach(s => {
            if (!Number.isInteger(s)) throw new Error('Invalid score not integer ' + s);
            if (s < 0 || s > 200) {
              throw new Error('Invalid score ' + s);
              process.exit(1);
            }
          })
        }
      }
      if (i === 3) {
        odd1 = parseFloat(a, 10);
      }
      if (i === 4) {
        odd2 = parseFloat(a, 10);
        oddDraw = null;
      }
    });
    if (
      Number.isNaN(odd1) ||
      Number.isNaN(odd2)
    ) {
      console.log('skip because of odd not found')
      playoffs = true;
      return;
    }
    if (!playoffs && !dateLine) {
      console.log(gameDate, teams.join('-'), score ? score.join('-') : 'future', odd1, odd2);
      // bug with this game
      if (gameDate === "2022-03-18" && team1 === "Boch") {
        return;
      }
      if (!games[team1]) {
        games[team1] = {}
      }
      if (!games[team1][gameDate]) {
        console.log('creating new game', gameDate, team1, team2)
        games[team1][gameDate] = {
          date: gameDate,
          op: team2,
          home: true,
        }
      }
      const oddsTeam1 = [odd1,odd2]
      games[team1][gameDate] = {
        ...games[team1][gameDate],
        odds: {
          ...games[team1][gameDate].odds,
          oddsportal: oddsTeam1
        }
      }
      
      /* if (score && (!Number.isInteger(score[0]) || !Number.isInteger(score[1]))) {
        console.log(gameDate, team1, team2);
        console.log(score);
        throw 'a';
      } */
      if (score && score.length === 2) {
        games[team1][gameDate].score = score;
        games[team1][gameDate].victory = score[0] > score[1];
      }
      if (!games[team2]) {
        games[team2] = {}
      }
      if (!games[team2][gameDate]) {
        console.log('creating new game', gameDate, team1, team2)
        games[team2][gameDate] = {
          date: gameDate,
          op: team1,
          home: false,
        }
      }
      const oddsTeam2 = [odd2,odd1];
      games[team2][gameDate] = {
        ...games[team2][gameDate],
        odds: {
          ...games[team2][gameDate].odds,
          oddsportal: oddsTeam2
        }
      }
      if (score) {
        games[team2][gameDate].score = [score[1], score[0]];
        games[team2][gameDate].victory = score[0] < score[1];
      }
      j += 1;
    }
  });
  console.log('end of page ' + i);
  await browser.close();
  i += 1
  go();
}
go();