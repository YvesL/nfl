module.exports.getProcessArgv = (param) => {
  const index = process.argv.findIndex((arg) => arg === param);
  if (index === -1) {
    return undefined;
  }

  return process.argv[index + 1];
};

module.exports.rightPad = (str, length) => {
  let s = str;
  for (let i = 0; i <= length - str.length; i += 1) {
    s += " ";
  }
  return s;
};

module.exports.IGNORE_OP = {
  'all': [],
};

module.exports.teamsCompleteName = {
  "Sp. Subotica": "FK Spartak Subotica",
  "FK Spartak Subotica": "FK Spartak Subotica",
  "Spartak Subotica": "FK Spartak Subotica",
  "Radnik Surdulica": "Radnik Surdulica",
  "Radnik": "Radnik Surdulica",
  "Red Star Belgrade": "Red Star Belgrade",
  "Crvena zvezda": "Red Star Belgrade",
  "Vojvodina": "Vojvodina",
  "Vozdovac": "Vozdovac",
  "FK Vozdovac": "Vozdovac",
  "FK Radnicki 1923": "FK Radnicki 1923",
  "Radnicki 1923": "FK Radnicki 1923",
  "TSC Backa Topola": "TSC Backa Topola",
  "Radnicki Nis": "Radnicki Nis",
  "Mladost Lucani": "Mladost Lucani",
  "Mladost": "Mladost Lucani",
  "Kolubara": "Kolubara",
  "Javor": "Javor",
  "Napredak": "Napredak",
  "Cukaricki": "Cukaricki",
  "Partizan Belgrade": "Partizan Belgrade",
  "Partizan": "Partizan Belgrade",
  "Novi Pazar": "Novi Pazar",
  "FK Metalac": "FK Metalac",
  "Metalac": "FK Metalac",
  "FK Proleter Novi Sad": "FK Proleter Novi Sad",
  "Proleter": "FK Proleter Novi Sad",
  "FK Mladost Novi Sad": "FK Mladost Novi Sad",
  "Mladost GAT": "FK Mladost Novi Sad",
};
