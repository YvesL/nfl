module.exports.getStartDate = (season) => {
  if (season === "2020") return '2020-09-12';
  if (season === "2021") return '2021-08-10';
  if (season === "2022") return '2022-07-25';
  return `${season}-08-05`;
}

const POINTS_FOR_VICTORY = [0, 8, 16, 24];
//const POINTS_FOR_VICTORY = [1];
module.exports.POINTS_FOR_VICTORY = POINTS_FOR_VICTORY;

const POINTS_FOR_DEFEAT = [0, 8, 16, 24];
//const POINTS_FOR_DEFEAT = [1];
module.exports.POINTS_FOR_DEFEAT = POINTS_FOR_DEFEAT;

const BET_IF_SCORE_SUPERIOR_TO = [0, 8, 14, 20, 26, 32, 38];
//const BET_IF_SCORE_SUPERIOR_TO = [1];
module.exports.BET_IF_SCORE_SUPERIOR_TO = BET_IF_SCORE_SUPERIOR_TO;

const BET_IF_ODD_SUPERIOR_TO = [1.2, 1.3, 1.4, 1.6, 1.8, 2];
//const BET_IF_ODD_SUPERIOR_TO = [1.3];
module.exports.BET_IF_ODD_SUPERIOR_TO = BET_IF_ODD_SUPERIOR_TO;

const POINTS_PER_GOAL_SCORED = [-8, -4, 0, 4, 8];
module.exports.POINTS_PER_GOAL_SCORED = POINTS_PER_GOAL_SCORED;

const POINTS_PER_GOAL_TAKEN = [-8, -4, 0, 4, 8];
module.exports.POINTS_PER_GOAL_TAKEN = POINTS_PER_GOAL_TAKEN;

const FACTOR_M1 = [1, 2, 3];
module.exports.FACTOR_M1 = FACTOR_M1;

const FACTOR_M2 = [0, 1];
module.exports.FACTOR_M2 = FACTOR_M2;

const FACTOR_M3 = [0, 1];
module.exports.FACTOR_M3 = FACTOR_M3;

const VICTORIES_RATE_BONUS = [0];
module.exports.VICTORIES_RATE_BONUS = VICTORIES_RATE_BONUS;

const HOME_BONUS = [0];
module.exports.HOME_BONUS = HOME_BONUS;

const BET_ON_DRAW = [false];
module.exports.BET_ON_DRAW = BET_ON_DRAW;

module.exports.getNumSimulationsPerProcess = () => {
  /*
    Do not count POINTS_FOR_VICTORY because there is
    one process per season per POINTS_FOR_VICTORY
  */
  return  POINTS_FOR_DEFEAT.length *
  BET_IF_SCORE_SUPERIOR_TO.length *
  BET_IF_ODD_SUPERIOR_TO.length *
  POINTS_PER_GOAL_SCORED.length *
  POINTS_PER_GOAL_TAKEN.length *
  FACTOR_M1.length *
  FACTOR_M2.length *
  FACTOR_M3.length *
  VICTORIES_RATE_BONUS.length *
  HOME_BONUS.length *
  BET_ON_DRAW.length;
}

module.exports.bestConfig = {
  "pointsForVictory": 32,
  "pointsForDefeat": 0,
  "betIfScoreSuperiorTo": 12,
  "betIfOddSuperiorTo": 2,
  "goalDiffFactor": 3,
  "factorM1": 1,
  "factorM2": 1,
  "factorM3": 0,
  "victoriesRateBonus": 40,
  "homeBonus": 0,
  "betOnDraw": false,
};
