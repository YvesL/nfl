season(s): 2022, sport: --championship
Finished 2022-10-26T14:47:59.290Z in 31.09minutes
Seasons 2022
Ran 691200 simulations (total) across 5x1 processes

=== SORT BY NET:

Winner config: 0/9/0/2.2/1/1/1/0/0/60/false
Net results : 
2022 : 44.5x
Full config and results 2022
{
  "pointsForVictory": 0,
  "pointsForDefeat": 9,
  "betIfScoreSuperiorTo": 0,
  "betIfOddSuperiorTo": 2.2,
  "goalDiffFactor": 1,
  "factorM1": 1,
  "factorM2": 1,
  "factorM3": 0,
  "victoriesRateBonus": 0,
  "homeBonus": 60,
  "betOnDraw": "false",
  "results": {
    "net": 44.5,
    "wins": 53,
    "winsNet": 109.5,
    "looses": 65,
    "rate": 0.45
  }
}
Top 10 :
0/9/0/2.2/1/1/1/0/0/60/false : net = 2022:44.5x avg:44.5x, wins: 2022:53    avg:53  , looses: 2022:65    avg:65  
0/9/0/2.2/1/1/1/0/0/40/false : net = 2022:44.5x avg:44.5x, wins: 2022:53    avg:53  , looses: 2022:65    avg:65  
0/9/0/2.2/1/1/1/0/0/24/false : net = 2022:44.5x avg:44.5x, wins: 2022:53    avg:53  , looses: 2022:65    avg:65  
0/15/0/2.2/2/1/1/0/0/60/false: net = 2022:44.18x avg:44.18x, wins: 2022:53    avg:53  , looses: 2022:65    avg:65  
0/15/0/2.2/2/1/1/0/0/40/false: net = 2022:44.18x avg:44.18x, wins: 2022:53    avg:53  , looses: 2022:65    avg:65  
0/15/0/2.2/2/1/1/0/0/24/false: net = 2022:44.18x avg:44.18x, wins: 2022:53    avg:53  , looses: 2022:65    avg:65  
0/9/0/1.9/1/1/1/0/0/60/false : net = 2022:40.86x avg:40.86x, wins: 2022:58    avg:58  , looses: 2022:74    avg:74  
0/9/0/1.9/1/1/1/0/0/40/false : net = 2022:40.86x avg:40.86x, wins: 2022:58    avg:58  , looses: 2022:74    avg:74  
0/9/0/1.9/1/1/1/0/0/24/false : net = 2022:40.86x avg:40.86x, wins: 2022:58    avg:58  , looses: 2022:74    avg:74  
0/15/0/1.9/2/1/1/0/0/60/false: net = 2022:40.54x avg:40.54x, wins: 2022:58    avg:58  , looses: 2022:74    avg:74  


=== SORT BY RATE (wins/looses):

Winner config (rate wins/looses): 0/15/30/2.2/1/2/1/1/0/0/false
Full config and results 2022
{
  "pointsForVictory": 0,
  "pointsForDefeat": 15,
  "betIfScoreSuperiorTo": 30,
  "betIfOddSuperiorTo": 2.2,
  "goalDiffFactor": 1,
  "factorM1": 2,
  "factorM2": 1,
  "factorM3": 1,
  "victoriesRateBonus": 0,
  "homeBonus": 0,
  "betOnDraw": "false",
  "results": {
    "net": 13.2,
    "wins": 7,
    "winsNet": 16.2,
    "looses": 3,
    "rate": 0.7
  }
}
Top 10 :
0/15/30/2.2/1/2/1/1/0/0/false: rate = 2022:0.7   avg:0.7 , wins: 2022:7     avg:7   , looses: 2022:3     avg:3   
0/0/20/1.6/3/1/1/1/0/0/false : rate = 2022:0.65  avg:0.65, wins: 2022:11    avg:11  , looses: 2022:6     avg:6   
0/0/20/1.5/3/1/1/1/0/0/false : rate = 2022:0.65  avg:0.65, wins: 2022:11    avg:11  , looses: 2022:6     avg:6   
0/0/20/1.2/3/1/1/1/0/0/false : rate = 2022:0.65  avg:0.65, wins: 2022:11    avg:11  , looses: 2022:6     avg:6   
0/0/20/1.1/3/1/1/1/0/0/false : rate = 2022:0.65  avg:0.65, wins: 2022:11    avg:11  , looses: 2022:6     avg:6   
16/15/20/1.6/3/1/1/1/0/0/false: rate = 2022:0.65  avg:0.65, wins: 2022:11    avg:11  , looses: 2022:6     avg:6   
16/15/20/1.5/3/1/1/1/0/0/false: rate = 2022:0.65  avg:0.65, wins: 2022:11    avg:11  , looses: 2022:6     avg:6   
16/15/20/1.2/3/1/1/1/0/0/false: rate = 2022:0.65  avg:0.65, wins: 2022:11    avg:11  , looses: 2022:6     avg:6   
16/15/20/1.1/3/1/1/1/0/0/false: rate = 2022:0.65  avg:0.65, wins: 2022:11    avg:11  , looses: 2022:6     avg:6   
9/9/20/1.6/3/1/1/1/0/0/false : rate = 2022:0.65  avg:0.65, wins: 2022:11    avg:11  , looses: 2022:6     avg:6   

