const startOfSeason = (season) => {
  if (season === "2020") {
    return "09-12";
  }
  if (season === "2021") {
    return "08-10";
  }
  if (season === "2022") {
    return "07-25";
  }
  return "08-05";
};

module.exports.getStartDate = (season) =>
  `${season}-${startOfSeason(season)}`;

const POINTS_FOR_VICTORY = [0, 12, 24, 32];
//const POINTS_FOR_VICTORY = [1];
module.exports.POINTS_FOR_VICTORY = POINTS_FOR_VICTORY;

const POINTS_FOR_DEFEAT = [0, 12, 24, 32];
//const POINTS_FOR_DEFEAT = [32];
module.exports.POINTS_FOR_DEFEAT = POINTS_FOR_DEFEAT;

const BET_IF_SCORE_SUPERIOR_TO = [0, 8, 12, 16];
//const BET_IF_SCORE_SUPERIOR_TO = [1];
module.exports.BET_IF_SCORE_SUPERIOR_TO = BET_IF_SCORE_SUPERIOR_TO;

const BET_IF_ODD_SUPERIOR_TO = [2, 2.2, 2.6, 3, 3.4];
//const BET_IF_ODD_SUPERIOR_TO = [1.3];
module.exports.BET_IF_ODD_SUPERIOR_TO = BET_IF_ODD_SUPERIOR_TO;

const GOAL_DIFF_FACTOR = [1, 2, 3, 4];
module.exports.GOAL_DIFF_FACTOR = GOAL_DIFF_FACTOR;

const FACTOR_M1 = [1];
module.exports.FACTOR_M1 = FACTOR_M1;

const FACTOR_M2 = [0, 1];
module.exports.FACTOR_M2 = FACTOR_M2;

const FACTOR_M3 = [0, 1];
module.exports.FACTOR_M3 = FACTOR_M3;

const VICTORIES_RATE_BONUS = [0, 20, 40, 60, 100, 140];
module.exports.VICTORIES_RATE_BONUS = VICTORIES_RATE_BONUS;

const HOME_BONUS = [0, 24, 40, 60, 80];
module.exports.HOME_BONUS = HOME_BONUS;

const BET_ON_DRAW = [false];
module.exports.BET_ON_DRAW = BET_ON_DRAW;

const DO_NOT_BET_ON_TEAMS = {}
module.exports.DO_NOT_BET_ON_TEAMS = DO_NOT_BET_ON_TEAMS;

module.exports.getNumSimulationsPerProcess = () => {
  /*
    Do not count POINTS_FOR_VICTORY because there is
    one process per season per POINTS_FOR_VICTORY
  */
  return  POINTS_FOR_DEFEAT.length *
  BET_IF_SCORE_SUPERIOR_TO.length *
  BET_IF_ODD_SUPERIOR_TO.length *
  GOAL_DIFF_FACTOR.length *
  FACTOR_M1.length *
  FACTOR_M2.length *
  FACTOR_M3.length *
  VICTORIES_RATE_BONUS.length *
  HOME_BONUS.length *
  BET_ON_DRAW.length;
}

module.exports.bestConfig = {
  "pointsForVictory": 1,
  "pointsForDefeat": 32,
  "betIfScoreSuperiorTo": 1,
  "betIfOddSuperiorTo": 3.4,
  "goalDiffFactor": 3,
  "factorM1": 1,
  "factorM2": 0,
  "factorM3": 1,
  "victoriesRateBonus": 0,
  "homeBonus": 0,
  "betOnDraw": false,
  doNotBetOnTeams: {}
  // 2018 17/4
  // 2019 17/3
  /* "pointsForVictory": 25,
  "pointsForDefeat": 5,
  "betIfScoreSuperiorTo": 125,
  "betIfOddSuperiorTo": 1.1,
  "goalDiffFactor": 12,
  "factorM1": 2,
  "factorM2": 0,
  "factorM3": 0,
  "victoriesRateBonus": 2,
  doNotBetOnTeams: {} */
};
