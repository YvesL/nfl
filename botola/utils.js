module.exports.getProcessArgv = (param) => {
  const index = process.argv.findIndex((arg) => arg === param);
  if (index === -1) {
    return undefined;
  }

  return process.argv[index + 1];
};

module.exports.rightPad = (str, length) => {
  let s = str;
  for (let i = 0; i <= length - str.length; i += 1) {
    s += " ";
  }
  return s;
};

module.exports.IGNORE_OP = {
  'all': [],
};

module.exports.teamsCompleteName = {
  "FAR Rabat": "FAR Rabat",
  "Union Touarga": "Union Touarga",
  "Wydad": "Wydad",
  "Olympique Khouribga": "Olympique Khouribga",
  "IR Tanger": "IR Tanger",
  "Maghreb Fez": "Maghreb Fez",
  "Mouloudia Oujda": "Mouloudia Oujda",
  "Jeunesse Sportive Soualem": "Jeunesse Sportive Soualem",
  "Raja Casablanca": "Raja Casablanca",
  "Olympique de Safi": "Olympique de Safi",
  "Difaa El Jadidi": "Difaa El Jadidi",
  "Hassania Agadir": "Hassania Agadir",
  "Chabab Mohammedia": "Chabab Mohammedia",
  "Moghreb Tetouan": "Moghreb Tetouan",
  "Mat M Asso Tetouan": "Moghreb Tetouan",
  "FUS Rabat": "FUS Rabat",
  "Raja Casablanca": "Raja Casablanca",
  "Youssoufia Berrechid": "Youssoufia Berrechid",
  "Berkane": "Berkane",
  "Rapide Oued Zem": "Rapide Oued Zem",
};
