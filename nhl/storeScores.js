const fs = require("fs");
const https = require("https");

const { XSCORES_AUTHORIZATION_HEADER } = require("../.env.js");
const { TEAMS, getProcessArgv } = require(__dirname + "/utils");

const SEASON = getProcessArgv("--season");

// visit https://www.xscores.com/hockey/kontinental-hockey-league/khl/regular-season/results/2019-2020
// to know how to build query

const SPORT = "4";
const LEAGUE_CODE = {
  2019: "46165",
  2020: "50301",
  2021: "55428",
  2022: "57439",
};
if (!LEAGUE_CODE[SEASON]) {
  throw new Error("unknon league code for season");
}
const path = `/m_leagueresult1?sport=${SPORT}&league_code=${
  LEAGUE_CODE[SEASON]
}&round=0&hasId=&country_name=KONTINENTAL%20HOCKEY%20LEAGUE&seasonName=${`${
  parseInt(SEASON) - 1
}-${parseInt(SEASON)}`}&order=asc&filter=results`;

console.log(path);
const parseTeamAndYear = async () => {
  const s = await new Promise((resolve, reject) => {
    const req = https.get(
      {
        headers: {
          accept: "*/*",
          "accept-language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7",
          authorization: XSCORES_AUTHORIZATION_HEADER,
          "sec-fetch-dest": "empty",
          "sec-fetch-mode": "cors",
          "sec-fetch-site": "same-site",
          "sec-gpc": "1",
        },
        host: "api-amazon.xscores.com",
        path: path,
      },
      (res) => {
        if (res.statusCode !== 200) {
          console.log(res.statusCode);
          throw new Error("status code not 200");
        }
        let d = "";
        res.on("data", (a) => {
          d += a.toString("utf8");
        });
        res.on("end", () => {
          resolve(d);
        });
      }
    );
    req.end();
  });
  console.log(s.slice(0, 300));
  const json = JSON.parse(`{ "a": ${s} }`);
  const games = {};
  let i = 0;
  json.a[0].scores.forEach((game) => {
    if (
      typeof game.home !== "string" ||
      !TEAMS[SEASON].find((a) => {
        return a === game.home;
      })
    ) {
      console.log(game);
      throw new Error("team not found -" + game.home + "- " + game.matchDate);
    }
    if (!games[game.home]) {
      games[game.home] = {};
    }
    const score1 = parseInt(game.score.split("-")[0], 10);
    const score2 = parseInt(game.score.split("-")[1], 10);
    games[game.home][game.matchDate] = {
      wk: game.leagueSort,
      date: game.matchDate,
      op: game.away,
      victory: score1 > score2,
      draw: score1 === score2,
      score: [score1, score2],
    };
    if (!games[game.away]) {
      games[game.away] = {};
    }
    games[game.away][game.matchDate] = {
      wk: game.leagueSort,
      date: game.matchDate,
      op: game.home,
      victory: score2 > score1,
      draw: score1 === score2,
      score: [score2, score1],
    };
    i += 1;
  });
  console.log(i, "matchs processed");
  fs.writeFileSync(
    `${__dirname}/games-${SEASON}.json`,
    JSON.stringify(games, null, 2),
    `utf8`
  );
};

parseTeamAndYear();
