const startOfSeason = (season) => {
  return "10-01";
};

module.exports.getStartDate = (season) =>
  `${parseInt(season) - 1}-${startOfSeason()}`;

module.exports.POINTS_FOR_VICTORY = [50, 52, 54, 56, 58, 60];

// How many points for a defeat ? (matches n-1, n-2 and n-3 are taken into consideration)
module.exports.POINTS_FOR_DEFEAT = [12, 14, 15, 16, 18];
//module.exports.POINTS_FOR_DEFEAT = [14, 16, 18];

// Only bet if a score reaches a minimum
module.exports.BET_IF_SCORE_SUPERIOR_TO = [
  18, 20, 22, 24, 26, 28, 30, 50, 70, 90, 110, 130,
];
//module.exports.BET_IF_SCORE_SUPERIOR_TO = [22, 24];

// Take into consideration the goal diff
// if diff factor is 2 and the score is 10-20 (defeat),
// there is a negative bonus of -10*2 = -20 for current team (looser team)
module.exports.GOAL_DIFF_FACTOR = [1.7, 1.8, 1.9, 2.0, 2.2, 2.4, 2.6];
//module.exports.GOAL_DIFF_FACTOR = [1.7, 1.8, 1.9];

// Weight for match n-1, n-2, and n-3
module.exports.FACTOR_M1 = [3.8, 3.9, 4.0, 4.1, 4.2, 4.3];
//module.exports.FACTOR_M1 = [4.1, 4.2];

module.exports.FACTOR_M2 = [2.2, 2.3, 2.4, 2.5];
//module.exports.FACTOR_M2 = [1.2, 1.4];

module.exports.FACTOR_M3 = [2.2, 2.3, 2.4, 2.5];
//module.exports.FACTOR_M3 = [1.0, 1.2, 1.6];
