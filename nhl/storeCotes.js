const puppeteer = require("puppeteer");
const fs = require("fs");
const { teamsCompleteName } = require(__dirname + "/utils");

const YEAR = 2022;

try {
  fs.readFileSync(`${__dirname}/cotes-${YEAR}.json`);
} catch (err) {
  fs.writeFileSync(`${__dirname}/cotes-${YEAR}.json`, "{}", "utf8");
}

const main = async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  const url1 =
    "https://offer.cdn.betclic.fr/api/pub/v2/competitions/83?application=2&countrycode=fr&fetchMultipleDefaultMarkets=true&language=fr&sitecode=frfr";
  console.log("Loading", url1);

  await page.goto(url1);
  await new Promise((r) => {
    setTimeout(r, 500);
  });

  let nth = 1;

  let html;
  try {
    html = await page.evaluate((nth) => {
      return document.body.innerHTML;
      //return document.body.childNodes[0].innerHTML;
    }, nth || 0);
  } catch (err) {
    console.log("ERROR");
    console.log(err);
    html = null;
    process.exit();
  }
  html = html.replace(/-->/g, "").replace(/<!--/g, "");
  console.log(html.slice(0, 300));
  let json;
  try {
    json = JSON.parse(html);
  } catch (e) {
    try {
      html = await page.evaluate((nth) => {
        return document.body.childNodes[0].innerHTML;
      }, nth || 0);
    } catch (err) {
      console.log("ERROR (2)");
      console.log(err);
      html = null;
      process.exit();
    }
    try {
      json = JSON.parse(html);
    } catch (e) {
      console.log("ERROR (3)");
      console.log(e);
      throw new Error("Could not parse");
    }
  }

  const cotesFile2 = fs.readFileSync(`${__dirname}/cotes-${YEAR}.json`, "utf8");
  let cotes2 = JSON.parse(cotesFile2);

  let news = 0;
  let updates = 0;
  json.unifiedEvents.forEach((e) => {
    console.log("-------");
    if (e.isLive) {
      console.log("skipping live event " + e.name);
      return;
    }
    const team1 = e.name.split(" - ")[0];
    const team1Short = teamsCompleteName[team1];
    const team2 = e.name.split(" - ")[1];
    const team2Short = teamsCompleteName[team2];
    if (!team1Short || !team2Short) {
      throw new Error("Missing team for " + team1 + " or " + team2 + "--" + e.name);
    }
    const oddTeam1 = e.markets[0].selections[0].odds;
    const oddDraw = e.markets[0].selections[1].odds;
    const oddTeam2 = e.markets[0].selections[2].odds;
    if (
      typeof oddTeam1 !== "number" ||
      typeof oddDraw !== "number" ||
      typeof oddTeam2 !== "number"
    ) {
      throw new Error(
        "odds are incorrect " +
          day +
          oddTeam1 +
          " / " +
          oddDraw +
          " / " +
          oddTeam2
      );
    }
    const offset = new Date().getTimezoneOffset();
    const offsetInMs = -(offset * 60 * 1000);
    const d = new Date(new Date(e.date).getTime() + offsetInMs).toISOString();
    const day = d.slice(0, 10);
    console.log(day);
    console.log(`${team1Short} ${oddTeam1} - ${oddTeam2} ${team2Short}`);
    console.log("live:", e.isLive);
    if (cotes2[day]) {
      const foundIndex = cotes2[day].findIndex((obj) => {
        return [team1Short, team2Short].includes(Object.keys(obj)[0]);
      });
      if (foundIndex === -1) {
        news += 1;
        cotes2 = {
          ...cotes2,
          [day]: cotes2[day].concat([
            {
              [team1Short]: oddTeam1,
              draw: oddDraw,
              [team2Short]: oddTeam2,
            },
          ]),
        };
      } else {
        updates += 1;
        cotes2 = {
          ...cotes2,
          [day]: cotes2[day]
            .filter((a, i) => i !== foundIndex)
            .concat([
              {
                [team1Short]: oddTeam1,
                draw: oddDraw,
                [team2Short]: oddTeam2,
              },
            ]),
        };
      }
    } else {
      news += 1;
      cotes2 = {
        ...cotes2,
        [day]: [
          {
            [team1Short]: oddTeam1,
            draw: oddDraw,
            [team2Short]: oddTeam2,
          },
        ],
      };
    }
  });
  fs.writeFileSync(
    `${__dirname}/cotes-${YEAR}.json`,
    JSON.stringify(cotes2, null, 2),
    "utf8"
  );
  return `${__dirname}/cotes-${YEAR}.json updated ${news} news and ${updates} updates ! `;
};

if (process.argv.findIndex((arg) => arg === "--long") !== -1) {
  console.log("started to index cotes every 30 minutes");
  try {
    fs.readFileSync(`${__dirname}/indexing-${YEAR}.txt`, "utf8");
  } catch (err) {
    fs.writeFileSync(`${__dirname}/indexing-${YEAR}.txt`, "", "utf8");
  }
  const func = async () => {
    let line;
    let d = new Date().toISOString().slice(0, 16);
    try {
      const a = await main();
      console.log(a);
      line = `${d} ${a}`;
    } catch (err) {
      console.log(err);
      line = `${d} error: ${err.Error}`;
    }
    let text = fs.readFileSync(`${__dirname}/indexing-${YEAR}.txt`, "utf8");
    text += "\n" + line;
    fs.writeFileSync(`${__dirname}/indexing-${YEAR}.txt`, text, "utf8");
  };
  setInterval(func, 1000 * 60 * 30);
  setInterval(
    () => {
      process.exit();
    },
    1000 * 60 * 60 * 48 // 2 days restart
  );
  func();
} else {
  const func = async () => {
    const a = await main();
    console.log(a);
    process.exit();
  };
  func();
}
