const Writable = require("stream").Writable;
const readline = require("readline");

module.exports.getProcessArgv = (param) => {
  const index = process.argv.findIndex((arg) => arg === param);
  if (index === -1) {
    return undefined;
  }

  return process.argv[index + 1];
};

module.exports.rightPad = (str, length) => {
  let s = str;
  for (let i = 0; i <= length - str.length; i += 1) {
    s += " ";
  }
  return s;
};

const teams2022 = [
  "tor",
  "nyr",
  "phi",
  "sea",
  "cgy",
  "ana",
  "ari",
  "tb",
  "fla",
  "pit",
  "dal",
  "wsh",
  "col",
  "nj",
  "mtl",
  "sj",
  "buf",
  "van",
  "det",
  "cbj",
  "nsh",
  "la",
  "min",
  "wpg",
  "edm",
  "bos",
  "vgk",
  "stl",
  "chi",
  "nyi",
  "ott",
  "car",
];

const TEAMS = {
  2020: teams2022.filter((a) => !["sea"].includes(a)),
  2021: teams2022.filter((a) => !["sea"].includes(a)),
  2022: teams2022,
};
module.exports.TEAMS = TEAMS;

module.exports.simplePrompt = (text) => {
  return new Promise((resolve) => {
    var mutableStdout = new Writable({
      write: function (chunk, encoding, callback) {
        if (!this.muted) process.stdout.write(chunk, encoding);
        callback();
      },
    });

    mutableStdout.muted = false;

    var rl = readline.createInterface({
      input: process.stdin,
      output: mutableStdout,
      terminal: true,
    });

    rl.question(text, function () {
      resolve("");
      console.log("");
      rl.close();
    });

    mutableStdout.muted = true;
  });
};

module.exports.teamsCompleteName = {
  "TOR Maple Leafs": "tor",
  "NY Rangers": "nyr",
  "PHI Flyers": "phi",
  "SEA Kraken": "sea",
  "OTT Senators": "ott",
  "CGY Flames": "cgy",
  "ANA Ducks": "ana",
  "AZ Coyotes": "ari",
  "TB Lightning": "tb",
  "FLA Panthers": "fla",
  "PIT Penguins": "pit",
  "DAL Stars": "dal",
  "WSH Capitals": "wsh",
  "COL Avalanche": "col",
  "NJ Devils": "nj",
  "SEA Kraken": "sea",
  "MTL Canadiens": "mtl",
  "SJ Sharks": "sj",
  "BUF Sabres": "buf",
  "VAN Canucks": "van",
  "DET Red Wings": "det",
  "CBJ Blue Jackets": "cbj",
  "NSH Predators": "nsh",
  "LA Kings": "la",
  "MIN Wild": "min",
  "WPG Jets": "wpg",
  "EDM Oilers": "edm",
  "ANA Ducks": "ana",
  "PHI Flyers": "phi",
  "BOS Bruins": "bos",
  "VGK Golden Knights": "vgk",
  "STL Blues": "stl",
  "CHI Blackhawks": "chi",
  "NY Islanders": "nyi",
  "CAR Hurricanes": "car",
};
