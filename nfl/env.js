module.exports.getStartDate = (season) => {
  return `${season}-09-01`;
}

const POINTS_FOR_VICTORY = [0, 24, 40];
//const POINTS_FOR_VICTORY = [1];
module.exports.POINTS_FOR_VICTORY = POINTS_FOR_VICTORY;

// How many points for a defeat ? (matches n-1, n-2 and n-3 are taken into consideration)
const POINTS_FOR_DEFEAT = [0, 24, 40];
//const POINTS_FOR_DEFEAT = [41];
module.exports.POINTS_FOR_DEFEAT = POINTS_FOR_DEFEAT;

// Only bet if a score reaches a minimum
const BET_IF_SCORE_SUPERIOR_TO = [50, 60, 70, 80, 90];
//const BET_IF_SCORE_SUPERIOR_TO = [175];
module.exports.BET_IF_SCORE_SUPERIOR_TO = BET_IF_SCORE_SUPERIOR_TO;

// Only bet if a score reaches a minimum
//const BET_IF_ODD_SUPERIOR_TO = [1.8, 2.1, 2.2, 2.3];
const BET_IF_ODD_SUPERIOR_TO = [1.4, 1.5, 1.6, 1.7, 1.8];
module.exports.BET_IF_ODD_SUPERIOR_TO = BET_IF_ODD_SUPERIOR_TO;

const POINTS_PER_GOAL_SCORED = [-6, -3, 0, 3];
module.exports.POINTS_PER_GOAL_SCORED = POINTS_PER_GOAL_SCORED;

const POINTS_PER_GOAL_TAKEN = [-6, -3, 0, 3];
module.exports.POINTS_PER_GOAL_TAKEN = POINTS_PER_GOAL_TAKEN;

// Weight for match n-1, n-2, and n-3
//const FACTOR_M1 = [1, 2];
const FACTOR_M1 = [1, 2];
module.exports.FACTOR_M1 = FACTOR_M1;

//const FACTOR_M2 = [0, 1];
const FACTOR_M2 = [0, 1];
module.exports.FACTOR_M2 = FACTOR_M2;

const FACTOR_M3 = [0, 1];
//const FACTOR_M3 = [1.0];
module.exports.FACTOR_M3 = FACTOR_M3;

const VICTORIES_RATE_BONUS = [0, 40];
//const VICTORIES_RATE_BONUS = [40];
module.exports.VICTORIES_RATE_BONUS = VICTORIES_RATE_BONUS;

const HOME_BONUS = [0];
//const HOME_BONUS = [20];
module.exports.HOME_BONUS = HOME_BONUS;

const BET_ON_DRAW = [false];
//const BET_ON_DRAW = [true];
module.exports.BET_ON_DRAW = BET_ON_DRAW;

const DO_NOT_BET_ON_TEAMS = {};
module.exports.DO_NOT_BET_ON_TEAMS = DO_NOT_BET_ON_TEAMS;

module.exports.getNumSimulationsPerProcess = () => {
  /*
    Do not count POINTS_FOR_VICTORY because there is
    one process per season per POINTS_FOR_VICTORY
  */
  return  POINTS_FOR_DEFEAT.length *
  BET_IF_SCORE_SUPERIOR_TO.length *
  BET_IF_ODD_SUPERIOR_TO.length *
  POINTS_PER_GOAL_SCORED.length *
  POINTS_PER_GOAL_TAKEN.length *
  FACTOR_M1.length *
  FACTOR_M2.length *
  FACTOR_M3.length *
  VICTORIES_RATE_BONUS.length *
  HOME_BONUS.length *
  BET_ON_DRAW.length;
}

module.exports.bestConfig = {
  "pointsForVictory": 60,
  "pointsForDefeat": 0,
  "betIfScoreSuperiorTo": 10,
  "betIfOddSuperiorTo": 1,
  "goalDiffFactor": 4,
  "factorM1": 2,
  "factorM2": 1.6,
  "factorM3": 1,
  "victoriesRateBonus": 0,
};
