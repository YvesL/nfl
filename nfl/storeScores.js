const puppeteer = require("puppeteer");
const { parse } = require("node-html-parser");
const fs = require("fs");

const { TEAMS, IGNORE_OP, getProcessArgv } = require(__dirname + "/utils");

const SEASON = getProcessArgv("--season");

const gamesByTeams = {};

const months = {
  Jan: "01",
  Feb: "02",
  Mar: "03",
  Apr: "04",
  May: "05",
  Jun: "06",
  Jul: "07",
  Aug: "08",
  Sep: "09",
  Oct: "10",
  Nov: "11",
  Dec: "12",
};

let i = 0;
const parseTeamAndYear = async (teamIndex, season) => {
  const team = TEAMS[SEASON][teamIndex];
  const ignoreOps = IGNORE_OP[SEASON];
  console.log("Processing team", TEAMS[SEASON][i], "for", season);

  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  const url = `https://www.espn.com/nfl/team/schedule/_/name/${team}/season/${season}`;
  console.log(url);

  await page.goto(url);

  await new Promise((r) => {
    setTimeout(r, 1000);
  });

  let html;
  try {
    html = await page.evaluate(() => {
      return document.getElementsByClassName("Table__TBODY")[0].innerHTML;
    });
  } catch (err) {
    console.log(err);
    html = null;
  }

  if (html) {
    const root = parse(html);

    gamesByTeams[team] = {};
    const lines = root.querySelectorAll("tr");
    let titleLine = false;
    let regularSeason = false;
    lines.forEach((line, i) => {
      const titles = line.querySelectorAll(".Table__Title");

      if (titles[0] && titles[0].childNodes && titles[0].childNodes[0]) {
        console.log(titles[0].childNodes[0].rawText);
        if (titles[0].childNodes[0].rawText === "Preseason") {
          regularSeason = false;
          titleLine = true;
        } else if (titles[0].childNodes[0].rawText === "Postseason") {
          regularSeason = false;
          titleLine = true;
        } else if (titles[0].childNodes[0].rawText === "Regular Season") {
          regularSeason = true;
          titleLine = true;
        } else {
          titleLine = false;
        }
      } else {
        titleLine = false;
      }

      if (!regularSeason) {
        return;
      }
      if (titleLine) {
        return;
      }

      const obj = {};
      let validLine = true;
      line.childNodes.forEach((cn, j) => {
        if (!validLine) {
          return;
        }
        // WEEK
        if (j === 0) {
          if (cn.rawText === "W") {
            validLine = false;
            return;
          }
          obj.wk = cn.rawText;
          console.log("wk", obj.wk);
        }
        // DATE
        if (j === 1) {
          let month = months[cn.rawText.substr(5, 3)];
          let season = SEASON;
          if (["01", "02"].includes(month)) {
            season = (parseInt(season) + 1).toString();
          }
          let day = cn.rawText.substr(9, 2);
          if (day.length === 1) day = `0${day}`;
          const date = `${season}-${month}-${day}`;
          if (date.length === 10) {
            obj.date = date;
            gamesByTeams[team][obj.date] = obj;
          } else {
            // Probably a BYE WEEK
            validLine = false;
            return;
          }
        }
        // OPONENT
        if (j === 2) {
          const opLogoEl = cn.querySelector(".opponent-logo");
          if (opLogoEl) {
            const imgEl = opLogoEl.querySelector("img");
            let op = imgEl
              .getAttribute("src")
              .replace(
                "https://a.espncdn.com/combiner/i?img=/i/teamlogos/nfl/500/",
                ""
              )
              .replace(".png&w=40&h=40", "");
            if (op.endsWith("/")) {
              op = op.slice(0, 2);
            }
            if (op.length !== 2 && op.length !== 3) {
              throw new Error(
                "Invalid opponent: " + op + "team:" + team,
                " line: ",
                i
              );
            }
            if (ignoreOps.find((o) => o === op)) {
              console.log("wk", obj.wk, "ignoring op ", op);
              validLine = false;
              return;
            }
            obj.op = op;
          }
        }

        // SCORE
        if (j === 3) {
          const a = cn.querySelector(".fw-bold");
          if (a && a.childNodes[0]) {
            const looseOrWin = a.childNodes[0].rawText;
            if (looseOrWin === "W") {
              obj.victory = true;
            }
            if (looseOrWin === "L") {
              obj.victory = false;
            }
            const b = cn.querySelector(".AnchorLink");
            try {
              obj.score = b.childNodes[0].rawText
                .split("-")
                .map((c) => parseInt(c));
            } catch (err) {
              console.log("could not get score");
              console.log(err);
            }
          }
        }
      });
      const wk = parseInt(obj.wk);
      if (isNaN(wk) || typeof wk !== "number" || wk === 0) {
        delete gamesByTeams[team][obj.date];
        return;
      } else if (!obj.hasOwnProperty("victory")) {
      } else if (!obj.hasOwnProperty("victory")) {
      } else if (!obj.op) {
        throw new Error("No oponent " + team + " " + obj);
      }
      // score are always in the format superior - inferior
      // change this so it is team - oponent
      if (obj.victory === false) {
        obj.score = [obj.score[1], obj.score[0]];
      }
      if (typeof obj.op === "undefined") {
        delete gamesByTeams[team][obj.date];
        return;
      }
      if (!TEAMS[SEASON].includes(obj.op)) {
        console.log(
          `WARNING it seems that team ${obj.op} is missing from the list for season ${SEASON}`
        );
      }
    });

    await browser.close();
  }
  if (i < TEAMS[SEASON].length - 1) {
    i += 1;
    parseTeamAndYear(i, SEASON);
  } else {
    const gamesInOrder = {};
    Object.keys(gamesByTeams).forEach((teamIndex) => {
      gamesInOrder[teamIndex] = {};
      const games = gamesByTeams[teamIndex];
      Object.keys(games)
        .sort((k1, k2) => {
          return (
            new Date(games[k1].date).getTime() -
            new Date(games[k2].date).getTime()
          );
        })
        .forEach((date) => {
          gamesInOrder[teamIndex][date] = games[date];
        });
    });
    fs.writeFileSync(
      `${__dirname}/games-wh-${SEASON}.json`,
      JSON.stringify(gamesInOrder, null, 2)
    );

    const teams = Object.keys(gamesInOrder);
    console.log(teams.length, "teams processed");
    let matchs = 0;
    teams.forEach((t) => {
      console.log(t, !!gamesInOrder[t]);
      matchs += Object.keys(gamesInOrder[t]).length;
    });
    console.log(matchs, "matchs processed");
  }
};

parseTeamAndYear(0, SEASON);
