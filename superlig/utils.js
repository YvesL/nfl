module.exports.getProcessArgv = (param) => {
  const index = process.argv.findIndex((arg) => arg === param);
  if (index === -1) {
    return undefined;
  }

  return process.argv[index + 1];
};

module.exports.rightPad = (str, length) => {
  let s = str;
  for (let i = 0; i <= length - str.length; i += 1) {
    s += " ";
  }
  return s;
};

module.exports.IGNORE_OP = {
  'all': [],
};

module.exports.teamsCompleteName = {
 "Samsunspor": "Samsunspor",
 "Pendikspor": "Pendikspor",
 "Kasimpasa": "Kasimpasa",
 "Ankaragucu": "Ankaragucu",
 "Boluspor": "Boluspor",
 "Keciorengucu": "Keciorengucu",
 "Besiktas": "Besiktas",
 "Hatayspor": "Hatayspor",
 "Kayserispor": "Kayserispor",
 "Konyaspor": "Konyaspor",
 "Adana Demirspor ": "Adana Demirspor ",
 "Alanyaspor": "Alanyaspor",
 "Galatasaray": "Galatasaray",
 "Antalyaspor": "Antalyaspor",
 "Karagumruk": "Karagumruk",
 "Trabzonspor": "Trabzonspor",
 "Fenerbahce": "Fenerbahce",
 "Fenerbahçe": "Fenerbahce",
 "Istanbulspor": "Istanbulspor",
 "Istanbulspor AS": "Istanbulspor",
 "Gaziantep": "Gaziantep",
 "Gazisehir Gaziantep FK": "Gaziantep",
 "Istanbul Basaksehir": "Istanbul Basaksehir",
 "Basaksehir": "Istanbul Basaksehir",
 "Giresunspor": "Giresunspor",
 "Matalyaspor": "Matalyaspor",
 "Adana Demirspor": "Adana Demirspor",
 "Sivasspor": "Sivaspor",
 "Sivaspor": "Sivaspor",
 "Malatyaspor": "Malatyaspor",
 "Yeni Malatyaspor": "Malatyaspor",
 "Altinordu": "Altinordu",
 "Goztepe": "Goztepe",
 "Altay": "Altay",
 "Rizespor": "Rizespor",
 "Umraniyespor": "Umraniyespor",
 "Ümraniyespor": "Umraniyespor",
 "Karagumruk": "Fatih Karagumruk",
 "Karagümrük": "Fatih Karagumruk",
 "Fatih Karagumruk": "Fatih Karagumruk",
};
