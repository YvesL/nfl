import React from "react";


export const getValues = (output) => {
  const filteredAndSorted = output
    .filter(o => !o.day && o.outcome)
    .sort((o, p) => o.date > p.date ? 1 : -1)
    .map(a => ({ t: a.date, y: a.netTotal }));

  const values = {};
  filteredAndSorted.forEach(a => {
    values[a.t] = a.y
  });
  return values;
}

export class Results extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      datas: {}
    }
  }

  componentDidUpdate() {
    this.getIfMissing();
    let dataGraph = null;
    if (
      this.state.datas &&
      this.state.datas[this.props.url]
    ) {
      let values;
      if (this.props.url.includes('/total')) {
        // for whole season, graph is based on each week
        values = {};
        let net = 0;
        Object.keys(this.props.stats).forEach((week) => {
          // future ?
          if (
            new Date(week).getTime() >
            new Date().getTime() + 1000 * 60 * 60 * 24 * 7
          ) {
            return;
          }

          net += this.props.stats[week]['/api' + this.props.url].net;
          values[week] = net;
        });
      } else {
        // for leagues, graph is based on output, and each bets
        values = getValues(this.state.datas[this.props.url].output);
      }
      new Chartist.Line('.ct-chart', {
          labels: Object.keys(values),
          series: [
            Object.values(values)
          ]
        }, {
          low: -10,
          showArea: true,
          showPoint: false,
          fullWidth: true,
          chartPadding: {
            right: 40
          },
          lineSmooth: Chartist.Interpolation.cardinal({
            tension: 0
          })
        });
    }
  }

  componentDidMount() {
    this.getIfMissing();
  }

  getIfMissing = () => {
    if (!this.state.datas[this.props.url]) {
      fetch('http://localhost:3000/api' + this.props.url)
      .then((response) => response.json())
      .then((data) => {
        data.output = data.output.reverse();
        let vals = {
          '1-2': { wins: 0, looses: 0 },
          '2-3': { wins: 0, looses: 0 },
          '3-4': { wins: 0, looses: 0 },
          '4-5': { wins: 0, looses: 0 },
          '5-8': { wins: 0, looses: 0 },
          '8+': { wins: 0, looses: 0 },
        }
        data.output.forEach(o => {
          if (o.game && o.outcome) {
            if (o.odds[0] < 2) {
              if (o.outcome === 'won') vals['1-2'].wins += 1;
              if (o.outcome !== 'won') vals['1-2'].looses += 1;
            } else if (o.odds[0] < 3) {
              if (o.outcome === 'won') vals['2-3'].wins += 1;
              if (o.outcome !== 'won') vals['2-3'].looses += 1;
            } else if (o.odds[0] < 4) {
              if (o.outcome === 'won') vals['3-4'].wins += 1;
              if (o.outcome !== 'won') vals['3-4'].looses += 1;
            } else if (o.odds[0] < 5) {
              if (o.outcome === 'won') vals['4-5'].wins += 1;
              if (o.outcome !== 'won') vals['4-5'].looses += 1;
            } else if (o.odds[0] < 8) {
              if (o.outcome === 'won') vals['5-8'].wins += 1;
              if (o.outcome !== 'won') vals['5-8'].looses += 1;
            } else {
              if (o.outcome === 'won') vals['8+'].wins += 1;
              if (o.outcome !== 'won') vals['8+'].looses += 1;
            }
          }
        });
        this.setState({
          datas: {
            ...this.state.datas,
            [this.props.url]: data,
          }
        })
      });
    }
  }

  render() {
    const data = this.state.datas[this.props.url];
    return <div key={this.props.title} className="flex flex-col">
      <h4 className="text-400 text-2xl pb-4 pt-4">{this.props.title}</h4>
      <div style={{width: "900px", height:"400px" }} className="ct-chart"></div>
      {
        data &&
        <>
        <br />
        <p className="text-2xl">
          Net over season :&nbsp;
          <span className="text-2xl">{data.results.net} * base</span></p>
        <p className="">Number bets : <span>{data.results.wins + data.results.looses}</span></p>
        <p className="">Successful bets : <span>{data.results.wins}</span>, lost bets: <span>{data.results.looses}</span> ({Math.round(10000 * data.results.wins / (data.results.looses + data.results.wins)) / 100}%)</p>
        <p className="text-lg pb-4 ">Gain per bet : <span>{Math.round(1000 * data.results.net / (data.results.looses + data.results.wins)) / 1000}</span></p>
        <p className="text-lg pb-4 ">Bets per day : <span></span></p>
        <br />
        </>
      }
      <div className="results">
        {
          this.state.datas &&
          this.state.datas[this.props.url] &&
          this.state.datas[this.props.url].output.map((o, i) => {
            let formattedGame = o.game;
            if (o.day) {
              /* return <div className="pb-4 text-xl font-bold" key={o.date + i + "day"}>
                {
                  o.day < 0 ?
                    <span className="text-2xl underline text-amber-700">Cumulated {o.day}</span> 
                    : <span className="text-2xl underline text-emerald-700">Cumulated +{o.day}</span>
                }
              </div> */
            }
            if (o.outcome) {
              if (o.game) {
                let winner = null;
                if (o.gameScore[0] > o.gameScore[1]) winner = 0;
                if (o.gameScore[1] > o.gameScore[0]) winner = 1;
                formattedGame = o.game.split(' vs ').map((t, i) => {
                  if (winner === i) {
                    return `<b class="font-bold ">${t}</b>`
                  }
                  return t;
                }).join(' vs ')
              }
              return <div key={o.date + i} className="gammes pb-4 passed flex flex-row justif-start align-center">
                <div className="date font-medium ">{o.date}</div>
                <div className="game " dangerouslySetInnerHTML={{ __html: formattedGame}}></div>
                <div className="font-bold border-1 border-inherit border-zinc-800">{o.gameScore.join(' - ')}</div>
                { o.outcome === "won" &&
                  <div className="outcome won">
                    <span>Bet on {o.fav} (good bet)</span>
                    <div className="fc">
                      <span className="odd font-medium">odd: {Math.round(o.odds[0] * 100) / 100}</span>
                    </div>
                    <span className="p-1 rounded net text-lg bet-success">net: +{o.net}</span>
                    <span className="net-total">{o.netTotal > 0 ? '+' : ''}{Math.round(o.netTotal * 100) / 100}</span>
                  </div>
                }
                { o.outcome === "draw" &&
                  <div className="outcome draw">
                    <span>Bet on {o.fav} (lost bet, draw)</span>
                    <div className="fc">
                      <span className="odd font-medium">odd: {Math.round(o.odds[0] * 100) / 100}</span>
                    </div>
                    <span className="p-1 rounded net text-lg bet-failure">net: {o.net}</span>
                    <span className="net-total">{o.netTotal > 0 ? '+' : ''}{Math.round(o.netTotal * 100) / 100}</span>
                  </div>
                }
                { o.outcome === "lost" &&
                  <div className="outcome lost">
                    <span>Bet on {o.fav} (lost bet, other team won)</span>
                    <div className="fc">
                      <span className="odd font-medium">odd: {Math.round(o.odds[0] * 100) / 100}</span>
                    </div>
                    <span className="p-1 rounded net text-lg bet-failure">net: {o.net}</span>
                    <span className="net-total">{o.netTotal > 0 ? '+' : ''}{Math.round(o.netTotal * 100) / 100}</span>
                  </div>
                }
              </div>
            }
            if (o.future) {
              return <div key={o.date + i} className="gammesfutur pb-4">
                <div className="date font-medium">
                  {o.date}
                </div>
                <div className="game">{o.game}</div>
                <div className="beton font-bold">
                  bet on {o.fav}&nbsp;
                </div>
                <div>
                  <span className="odd font-medium">odd: {Math.round(o.odds[0] * 100) / 100}</span> 
                  </div>              
              </div>
            }
            return null;
          })
        }
      </div>
      <table className="table-auto">
        <thead>
          <tr>
            <th style={{width: '220px'}}></th>
            <th>Net</th>
            <th>Number of bets</th>
            <th style={{width: '130px'}}>Won bets</th>
            <th style={{width: '100px'}}>Lost bets</th>
            <th style={{width: '140px'}}>Avg winning odd</th>
          </tr>
        </thead>
        <tbody>
          {
            Object.keys(this.props.stats).reverse().map((a, b) => {
              const stats = this.props.stats[a]['/api' + this.props.url];
              if (
                new Date(a).getTime() >
                new Date().getTime() + 10 * 24 * 60  * 60 * 1000
              ) {
                return <></>
              }
              if (!stats) {
                return <tr key={a}>
                  <td>Week ending on {a}</td>
                  <td>0</td>
                  <td>0</td>
                </tr>
              }
              const net = Math.round(1000 * stats.net) / 1000;
              return <tr key={a}>
                <td>Week ending on {a}</td>
                <td>
                  
                  {net > 0 ? <span className="pos">+{net}</span> : <span className="neg">{net}</span>}</td>
                <td>{stats.bets}</td>
                <td>{
                  stats.bets ?
                  <>
                    <span className="pos">{stats.wins || ''}</span>
                    &nbsp;
                    ({Math.round(100 * stats.wins / stats.bets)}%)
                  </> : null}
                </td>
                <td className="neg">{stats.looses || ''}</td>
                <td className="">{
                  stats.wins ?
                  (Math.round(100 * stats.winsOdds / stats.wins) / 100) : '-'
                }</td>
              </tr>
            })
          }
        </tbody>
      </table>
    </div>
  }
}
