import React from 'react';

import { Results } from './Results';

const routes = {
  '/championship/2021': '🇬🇧 Championship, 2021-2022',
  '/championship/2022': '🇬🇧 Championship, 2022-2023',
  '/premiere/2021': '🇬🇧 Premiere league, 2021-2022',
  '/premiere/2022': '🇬🇧 Premiere league, 2022-2023',
  '/leagueone/2021': '🇬🇧 League one, 2021-2022',
  '/leagueone/2022': '🇬🇧 League one, 2022-2023',
  '/leaguetwo/2021': '🇬🇧 League two, 2021-2022',
  '/leaguetwo/2022': '🇬🇧 League two, 2022-2023',
  '/bundesliga/2021': '🇩🇪 Bundesliga, 2021-2022',
  '/bundesliga/2022': '🇩🇪 Bundesliga, 2022-2023',
  '/bundesliga2/2021': '🇩🇪 Bundesliga 2, 2021-2022',
  '/bundesliga2/2022': '🇩🇪 Bundesliga 2, 2022-2023',
  '/eredivisie/2021': '🇳🇱 Eredivisie, 2021-2022',
  '/eredivisie/2022': '🇳🇱 Eredivisie, 2022-2023',
  '/oneliga/2021': '🇨🇿 One Liga, 2021-2022',
  '/oneliga/2022': '🇨🇿 One Liga, 2022-2023',
  '/seriea/2021': '🇮🇹 Serie A, 2021-2022',
  '/seriea/2022': '🇮🇹 Serie A, 2022-2023',
  '/serieb/2021': '🇮🇹 Serie B, 2021-2022',
  '/serieb/2022': '🇮🇹 Serie B, 2022-2023',
  '/liga/2021': '🇪🇸 Liga, 2021-2022',
  '/liga/2022': '🇪🇸 Liga, 2022-2023',

  /* '/seriea/2021': "🇮🇹 Serie A, 2021-2022",
  '/seriea/2022': "🇮🇹 Serie A, 2022-2023",

  '/superlig/2021': "🇹🇷 Superlig, 2021-2022",
  '/superlig/2022': "🇹🇷 Superlig, 2022-2023",
  '/bundesliga2/2021': "🇩🇪 Bundesliga 2, 2021-2022",
  '/bundesliga2/2022': "🇩🇪 Bundesliga 2, 2022-2023",
  '/primeira/2021': "🇵🇹 Primeira, 2021-2022",
  '/primeira/2022': "🇵🇹 Primeira, 2022-2023",
  '/superliga/2021': "🇷🇸 Super Liga, 2021-2022",
  '/superliga/2022': "🇷🇸 Super Liga, 2022-2023",
  '/jupiler/2021': "🇧🇪 Jupiler, 2021-2022",
  '/jupiler/2022': "🇧🇪 Jupiler, 2022-2023", */
};
export class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      stats: undefined,
    };
  }

  getStats = () => {
    if (!this.state.stats) {
      fetch('http://localhost:3000/stats/2021')
        .then((response) => response.json())
        .then((data) => {
          this.setState({
            stats: {
              ...this.state.stats,
              2021: data,
            },
          });
        });
      fetch('http://localhost:3000/stats/2022')
        .then((response) => response.json())
        .then((data) => {
          this.setState({
            stats: {
              ...this.state.stats,
              2022: data,
            },
          });
        });
    }
  };

  componentDidUpdate() {
    this.getStats();
  }

  componentDidMount() {
    this.getStats();
  }

  render() {
    return (
      <div className='p-12'>
        <div className='pb-4 flex flex-col'>
          <a
            className='pr-4 font-bold underline'
            onClick={() => {
              this.setState({ route: '2021' });
            }}
          >
            2021
          </a>
          <a
            className='pr-4 font-bold underline'
            onClick={() => {
              this.setState({ route: '2022' });
            }}
          >
            2022
          </a>
          {Object.keys(routes).map((r) => {
            return (
              <a
                style={{ whiteSpace: 'nowrap' }}
                className='pr-4 font-bold underline'
                key={r}
                onClick={() => {
                  this.setState({ route: r });
                }}
              >
                {routes[r]}
              </a>
            );
          })}
        </div>
        {this.state.route === '2022' && this.state.stats && (
          <Results
            stats={this.state.stats[2022]
            }
            url={'/total/2022'}
            title={'Season 2022'}
        ></Results>
        )}
        {this.state.route === '2021' && this.state.stats && (
          <Results
            stats={this.state.stats[2021]
            }
            url={'/total/2021'}
            title={'Season 2021'}
          ></Results>
        )}
        {!['2021', '2022'].includes(this.state.route) && this.state.route && (
          <Results
            stats={
              this.state.route.includes('2021')
                ? this.state.stats[2021]
                : this.state.stats[2022]
            }
            url={this.state.route}
            title={routes[this.state.route]}
          ></Results>
        )}
        {!this.state.route && (
          <p
            className='max-w-2xl'
            style={{
              fontSize: '1.2rem',
              maxWidth: '900px',
              textAlign: 'justify',
            }}
          >
            <br />
            Hello welcome on <u>oddwizard.bet</u>. This website showcases the
            result of an AI program trained to find the best possible bets on
            many football leagues.
            <br />
            <br />
            <b>How does it work ?</b>
            <br />
            This AI program is pattern based, it tries to identify oportunities
            that are worth betting on. First, it stores many (many) data from
            games, like how many goals have been scored and conceded in the last
            games, how many victories and defeats, odds for each game etc. Then
            a basic weighting matrix runs to sort out the best possible values.
            <br />
            <br />
            Good opportunities are often a team that has lost its previous games
            and plays against a team that is in shape. In such a scenario the
            odd is often abnormally high, it is worth betting.
            <br />
            <br />
            <b>Does it win everytime ?</b>
            <br />
            No, this program generates more lost bets that successful bets. But
            the winning odds are often high enough to catch up.
            <br />
            <br />
            <b>How much should I bet ? How do I sort out my base ?</b>
            <br />
            The base is the money bet on each game that the algorithm proposes.
            You will do a lot of bets per week. If you bet on all the leagues,
            it will be around 6 bets per day. Let's say you want to put 100 GBP,
            your base could be{' '}
            <b>(100 / 2.5) / (6 * 7 the number of bets per week) = 0.95 GBP</b>,
            this way you are secure against 2+ consecutive weeks of 100% loosing
            bets.
          </p>
        )}
      </div>
    );
  }
}
