module.exports.getProcessArgv = (param) => {
  const index = process.argv.findIndex((arg) => arg === param);
  if (index === -1) {
    return undefined;
  }

  return process.argv[index + 1];
};

module.exports.rightPad = (str, length) => {
  let s = str;
  for (let i = 0; i <= length - str.length; i += 1) {
    s += " ";
  }
  return s;
};

module.exports.IGNORE_OP = {
  'all': [],
};

module.exports.teamsCompleteName = {
  "Manchester United FC": "Man United",
  "Manchester United": "Man United",
  "Manchester Utd": "Man United",
  "Leeds": "Leeds",
  "Leeds United": "Leeds",
  "Brentford": "Brentford",
  "Leicester City FC": "Leicester City",
  "Leicester City": "Leicester City",
  "Leicester": "Leicester City",
  "Nottingham": "Nottingham",
  "Nottingham Forest": "Nottingham",
  "Tottenham Hotspur": "Tottenham",
  "Tottenham": "Tottenham",
  "Tottenham Hotspur FC": "Tottenham",
  "Crystal Palace FC": "Crystal Palace",
  "Crystal Palace": "Crystal Palace",
  "Chelsea FC": "Chelsea",
  "Chelsea": "Chelsea",
  "Brighton & Hove Albion FC": "Brighton & Hove",
  "Brighton": "Brighton & Hove",
  "Brighton and Hove Albion": "Brighton & Hove",
  "Hove Albion FC": "Brighton & Hove",
  "Hove Albion": "Brighton & Hove",
  "Everton FC": "Everton",
  "Everton": "Everton",
  "Cardiff City FC": "Cardiff City",
  "Cardiff City": "Cardiff City",
  "AFC Bournemouth": "Bournemouth",
  "Bournemouth": "Bournemouth",
  "Liverpool FC": "Liverpool",
  "Liverpool": "Liverpool",
  "West Ham United FC": "West Ham",
  "West Ham": "West Ham",
  "Burnley FC": "Burnley",
  "Burnley": "Burnley",
  "Manchester City FC": "Man City",
  "Manchester City": "Man City",
  "Newcastle United FC": "Newcastle",
  "Newcastle United": "Newcastle",
  "Newcastle": "Newcastle",
  "Southampton FC": "Southampton",
  "Southampton": "Southampton",
  "Fulham FC": "Fulham",
  "Fulham": "Fulham",
  "Crystal Palace FC": "Crystal Palace",
  "Crystal Palace": "Crystal Palace",
  "Huddersfield Town AFC": "Huddersfield Town",
  "Huddersfield Town AFC": "Huddersfield Town",
  "Huddersfield Town": "Huddersfield Town",
  "Watford FC": "Watford",
  "Watford": "Watford",
  "Wolverhampton Wanderers FC": "Wolverhampton",
  "Wolverhampton": "Wolverhampton",
  "Wolves": "Wolverhampton",
  "Arsenal FC": "Arsenal",
  "Arsenal": "Arsenal",
  "Norwich": "Norwich City",
  "Norwich City FC": "Norwich City",
  "Norwich City": "Norwich City",
  "Sheffield United FC": "Sheffield United",
  "Sheffield United": "Sheffield United",
  "AFC Bournemouth": "Bournemouth",
  "Bournemouth": "Bournemouth",
  "Aston Villa FC": "Aston Villa",
  "Aston Villa": "Aston Villa",
};
