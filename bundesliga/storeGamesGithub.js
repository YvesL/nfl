const fs = require("fs");
const https = require("https");

const { teamsCompleteName } = require(__dirname + "/utils");

const SEASONS = ['2016-17', '2017-18', '2018-19', "2019-20", "2020-21"];
const HOURS_OFFSET_PARIS = 0;

const main = async (a, season, games) => {
const options = {
    host: 'raw.githubusercontent.com',
    method: 'GET',
    path: `/openfootball/football.json/master/${a}/de.1.json`,
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const json = await new Promise((resolve) => {
    const req = https.request(
      options,
      (res, err) => {
        if (res.statusCode !== 200) {
          throw new Error('status code !== 200 ' + res.statusCode + `/openfootball/football.json/master/${a}/de.1.json`)
        }

        let data = ''
        res.on('data', d => {
          data += d.toString('utf8')
        })
        res.on('end', () => {
          resolve(JSON.parse(data));
        })
      }
    );
    req.end("")
  });

  let endSeason = false;
  let i = 0;
  let j = 0;
  if (json.matches) {
    json.matches.forEach((e) => {
      if (endSeason) return;
      if (!e.score) {
        j+= 1;
        if (j > 5) {
          endSeason = true;
          return;
        }
        return;
      }
      i += 1;
  
      const team1Short = teamsCompleteName[e.team1];
      const team2Short = teamsCompleteName[e.team2];
      if (!team1Short || !team2Short) {
        throw new Error("Missing team for " + e.team1 + " or " + e.team2);
      }
      const homeScore = e.score.ft[0];
      const awayScore = e.score.ft[1];
      if (homeScore < 0 || homeScore > 15 || typeof homeScore !== 'number') {
        throw new Error('Invalid score ' + homeScore)
      }
      if (awayScore < 0 || awayScore > 15 || typeof awayScore !== 'number') {
        throw new Error('Invalid score ' + awayScore)
      }
      if (!games[team1Short]) games[team1Short] = {};
      if (!games[team2Short]) games[team2Short] = {};
    
      if (!games[team1Short][e.date]) {
        games[team1Short][e.date] = {
          wk: Object.keys(games[team1Short]).length,
          date: e.date,
          op: team2Short,
          victory: homeScore > awayScore,
          score: [homeScore, awayScore],
          odds: {},
        }
      }
      if (!games[team2Short][e.date]) {
        games[team2Short][e.date] = {
          wk: Object.keys(games[team2Short]).length,
          date: e.date,
          op: team1Short,
          victory: homeScore < awayScore,
          score: [awayScore, homeScore],
          odds: {},
        }
      }
    });
  } else {

  }
  console.log(a, i, 'games');
}


let i = 0;
const func = async () => {
  if (i === SEASONS.length) {
    return;
  }
  
  const a = SEASONS[i]
  const season = a.slice(0,4)

  let games = {};
  games = JSON.parse(fs.readFileSync(`${__dirname}/games-wh-${season}.json`, "utf8"));
  await main(a, season, games);
  fs.writeFileSync(
    `${__dirname}/games-wh-${season}.json`,
    JSON.stringify(games, null, 2),
    "utf8"
  )
  i += 1;
  await func();
}
func();
