module.exports.getProcessArgv = (param) => {
  const index = process.argv.findIndex((arg) => arg === param);
  if (index === -1) {
    return undefined;
  }

  return process.argv[index + 1];
};

module.exports.rightPad = (str, length) => {
  let s = str;
  for (let i = 0; i <= length - str.length; i += 1) {
    s += " ";
  }
  return s;
};

module.exports.IGNORE_OP = {
  'all': [],
};

module.exports.teamsCompleteName = {
  "Hoffenheim": "Hoffenheim",
  "TSG 1899 Hoffenheim": "Hoffenheim",
  "Fribourg": "Fribourg",
  "SC Freiburg": "Fribourg",
  "Freiburg": "Fribourg",
  "Bochum": "Boch",
  "VfL Bochum": "Boch",
  "Cologne": "Colo",
  "Cologne": "Colo",
  "Union Berlin": "UnionB",
  "Union Berlin": "UnionB",
  "Wolfsburg": "Wolfsburg",
  "VfL Wolfsburg": "Wolfsburg",
  "B. M'glad": "B. M'glad",
  "B. M'gladbach": "B. M'glad",
  "Bor. Mönchengladbach": "B. M'glad",
  "Borussia Monchengladbach": "B. M'glad",
  "B. Monchengladbach": "B. M'glad",
  "M'gladbach": "B. M'glad",
  "RB Leipzig": "Leipzig",
  "RB Leipzig": "Leipzig",
  "Leipzig": "Leipzig",
  "Leipzig": "Leipzig",
  "Stuttgart": "Stutt",
  "VfB Stuttgart": "Stutt",
  "Francfort": "Francfort",
  "Eintracht Frankfurt": "Francfort",
  "Ein Frankfurt": "Francfort",
  "Augsbourg": "Augsbourg",
  "Augsburg": "Augsbourg",
  "FC Augsburg": "Augsbourg",
  "1. FSV Mainz 05": "FSV Mainz",
  "Mainz": "FSV Mainz",
  "Mainz 05": "FSV Mainz",
  "FC Augsbourgurg": "Augsbourg",
  "Bayer 04 Leverkusen": "B.Leverk",
  "Bayer Leverkusen": "B.Leverk",
  "Leverkusen": "B.Leverk",
  "Bayern Munich": "Bayern Mun",
  "Bayern München": "Bayern Mun",
  "Dortmund": "Dortm",
  "Dortmund": "Dortm",
  "Borussia Dortmund": "Dortm",
  "Schalke": "Schalke",
  "Schalke 04": "Schalke",
  "FC Schalke 04": "Schalke",
  "B. Leverkusen": "B.Leverk",
  "B. Leverkusen": "B.Leverk",
  "Werder Brême": "Werder Br",
  "Werder Brême": "Werder Br",
  "Werder Bremen": "Werder Br",
  "Werder Bremen": "Werder Br",
  "Mayence": "FSV Mainz",
  "Mayence": "FSV Mainz",
  "Hertha Berl": "Hertha Berl",
  "Hertha Berlin": "Hertha Berl",
  "Hertha": "Hertha Berl",
  "Hertha BSC": "Hertha Berl",
  "1. FC Nürnberg": "Nuremberg",
  "Nurnberg": "Nuremberg",
  "Hamburg": "Hambourg",
  "Hamburger SV": "Hambourg",
  "Hannover": "Hanovre 96",
  "Hannover 96": "Hanovre 96",
  "Fortuna Düsseldorf": "Dusseldorf",
  "Dusseldorf": "Dusseldorf",
  "Fortuna Dusseldorf": "Dusseldorf",
  "SC Paderborn 07": "SC Paderborn",
  "1. FC Köln": "FC Köln",
  "FC Koln": "FC Köln",
  "Koln": "FC Köln",
  "St Pauli": "St Pauli",
  "1. FC Union Berlin": "Union Berlin",
  "Arminia Bielefeld": "Arminia Bielefeld",
  "Bielefeld": "Arminia Bielefeld",
  "Kaiserslautern": "Kaiserslautern",
  "Greuther Furth": "Greuther Furth",
  "Braunschweig": "Braunschweig",
  "Paderborn": "SC Paderborn",
  "Darmstadt": "Darmstadt",
  "Ingolstadt": "Ingolstadt",
};
