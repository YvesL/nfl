module.exports.getProcessArgv = (param) => {
  const index = process.argv.findIndex((arg) => arg === param);
  if (index === -1) {
    return undefined;
  }

  return process.argv[index + 1];
};

module.exports.rightPad = (str, length) => {
  let s = str;
  for (let i = 0; i <= length - str.length; i += 1) {
    s += " ";
  }
  return s;
};

module.exports.IGNORE_OP = {
  'all': [],
};

module.exports.teamsCompleteName = {
  "Strasbourg": "Strasbourg",
  "Lyon": "Lyon",
  "Toulouse": "Toulouse",
  "Lille": " Lille",
  "Montpellier": "Montpellier",
  "Rennes": "Rennes",
  "Lens": "Lens",
  "Paris SG": "Paris SG",
  "Nice": "Nice",
  "Marseille": "Marseille",
  "Clermont": "Clermont",
  "Lens": "Lens",
  "Brest": "Brest",
  "Angers": "Angers",
  "Lorient": "Lorient",
  "Monaco": "Monaco",
  "Nantes": "Nantes",
  "Metz": "Metz",
  "Auxerre": "Auxerre",
  "Troyes": "Troyes",
  "AC Ajaccio": "AC Ajaccio",
  "Reims": "Reims",
  "St Etienne": "St Etienne",
  "Bordeaux": "Bordeaux",
};
