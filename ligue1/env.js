module.exports.getStartDate = (season) => {
  if (season === "2020") return '2020-09-12';
  if (season === "2021") return '2021-08-10';
  if (season === "2022") return '2022-08-10';
  return `${season}-08-05`;
}

const POINTS_FOR_VICTORY = [0, 12, 20, 32];
module.exports.POINTS_FOR_VICTORY = POINTS_FOR_VICTORY;

const POINTS_FOR_DEFEAT = [0, 12, 20, 32];
module.exports.POINTS_FOR_DEFEAT = POINTS_FOR_DEFEAT;

const BET_IF_SCORE_SUPERIOR_TO = [15, 25, 35, 45, 55];
module.exports.BET_IF_SCORE_SUPERIOR_TO = BET_IF_SCORE_SUPERIOR_TO;

const BET_IF_ODD_SUPERIOR_TO = [2.2, 2.4, 2.6, 2.8, 2.9, 3, 3.1, 3.2, 3.3, 3.4];
module.exports.BET_IF_ODD_SUPERIOR_TO = BET_IF_ODD_SUPERIOR_TO;

const POINTS_PER_GOAL_SCORED = [-8, -4];
module.exports.POINTS_PER_GOAL_SCORED = POINTS_PER_GOAL_SCORED;

const POINTS_PER_GOAL_TAKEN = [-8, -4];
module.exports.POINTS_PER_GOAL_TAKEN = POINTS_PER_GOAL_TAKEN;

const FACTOR_M1 = [1, 2, 3];
module.exports.FACTOR_M1 = FACTOR_M1;

const FACTOR_M2 = [0, 1];
module.exports.FACTOR_M2 = FACTOR_M2;

const FACTOR_M3 = [0, 1];
module.exports.FACTOR_M3 = FACTOR_M3;

const VICTORIES_RATE_BONUS = [0];
module.exports.VICTORIES_RATE_BONUS = VICTORIES_RATE_BONUS;

const HOME_BONUS = [0];
module.exports.HOME_BONUS = HOME_BONUS;

const BET_ON_DRAW = [false];
module.exports.BET_ON_DRAW = BET_ON_DRAW;

module.exports.getNumSimulationsPerProcess = () => {
  /*
    Do not count POINTS_FOR_VICTORY because there is
    one process per season per POINTS_FOR_VICTORY
  */
  return POINTS_FOR_DEFEAT.length *
  BET_IF_SCORE_SUPERIOR_TO.length *
  BET_IF_ODD_SUPERIOR_TO.length *
  POINTS_PER_GOAL_SCORED.length *
  POINTS_PER_GOAL_TAKEN.length *
  FACTOR_M1.length *
  FACTOR_M2.length *
  FACTOR_M3.length *
  VICTORIES_RATE_BONUS.length *
  HOME_BONUS.length *
  BET_ON_DRAW.length;
}

module.exports.bestConfig = {
  "pointsForVictory": 20,
  "pointsForDefeat": 32,
  "betIfScoreSuperiorTo": 15,
  "betIfOddSuperiorTo": 3.2,
  "pointsPerGoalScored": -4,
  "pointsPerGoalTaken": -4,
  "factorM1": 2,
  "factorM2": 1,
  "factorM3": 1,
  "victoriesRateBonus": 0,
  "homeBonus": 0,
  "betOnDraw": false,
  /* "pointsForVictory": 44,
  "pointsForDefeat": 6,
  "betIfScoreSuperiorTo": 160,
  "betIfOddSuperiorTo": 1.4,
  "goalDiffFactor": 12,
  "factorM1": 3,
  "factorM2": 0,
  "factorM3": 1,
  "victoriesRateBonus": 20,
  "homeBonus": 10,
  "betOnDraw": false,
  doNotBetOnTeams: {} */
  // 2018 65/38 x8.76
  // 2019 54/32 x5.16
  /* "pointsForVictory": 20,
  "pointsForDefeat": 11,
  "betIfScoreSuperiorTo": 92,
  "betIfOddSuperiorTo": 1.6,
  "goalDiffFactor": 4,
  "factorM1": 5,
  "factorM2": 1,
  "factorM3": 1,
  "victoriesRateBonus": 1,
  "homeBonus": 16,
  doNotBetOnTeams: {} */
  // 2018 24/3
  // 2019 18/2
  /* "pointsForVictory": 10,
  "pointsForDefeat": 6,
  "betIfScoreSuperiorTo": 130,
  "betIfOddSuperiorTo": 1.1,
  "goalDiffFactor": 20,
  "factorM1": 1,
  "factorM2": 0,
  "factorM3": 1,
  "victoriesRateBonus": 0,//1,
  "homeBonus": 1,
  doNotBetOnTeams: {
    "Bayern Mun": true,
  } */
  // 2019 21/1
  // 2018 20/1
  /* "pointsForVictory": 10,
  "pointsForDefeat": 0,
  "betIfScoreSuperiorTo": 130,
  "betIfOddSuperiorTo": 1.1,
  "goalDiffFactor": 8,
  "factorM1": 2,
  "factorM2": 1,
  "factorM3": 1,
  "victoriesRateBonus": 2,
  "homeBonus": 1,
  "doNotBetOnTeams": {} */
};
