const puppeteer = require("puppeteer");
const { parse } = require("node-html-parser");
const fs = require("fs");
const { parseDate, getProcessArgv } = require('./utils');

const findArg = (param) => {
  return process.argv.findIndex((arg) => arg === param) !== -1
}
let HOURS_OFFSET_PARIS = 0;
const YEARS_BASED_ON_MONTH = {
  1: {
    'Jan': 1,
    'Feb': 1,
    'Mar': 1,
    'Apr': 1,
    'May': 1,
    'Jun': 1,
  },
  1: {
    'Jan': 1,
    'Feb': 1,
    'Mar': 1,
    'Apr': 1,
    'May': 1,
    'Jun': 1,
    'Jul': 1,
    'Aug': 1,
  },
}
let YEAR_BASED_ON_MONTH;
let SEASON = 2022;
if (findArg('--season') && ["2019", "2020", "2021"].includes(getProcessArgv('--season'))) {
  SEASON = parseInt(getProcessArgv('--season'));
}

let games;
let url;
let path;
let teamsCompleteName;
let league;
if (findArg('--bundesliga')) {
  league = 'bundesliga';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/germany/bundesliga/`;
} else if (findArg('--bundesliga2')) {
  league = 'bundesliga2';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  throw 'not supported'
} else if (findArg('--seriea')) {
  league = 'seriea';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/italy/serie-a/`;
} else if (findArg('--serieb')) {
  league = 'serieb';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/italy/serie-b/`;
} else if (findArg('--superlig')) {
  league = 'superlig';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  throw 'not supported'
} else if (findArg('--liga')) {
  league = 'liga';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/spain/laliga/`;
} else if (findArg('--premiere')) {
  league = 'premiere';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = 'https://www.oddsportal.com/soccer/england/premier-league/'
} else if (findArg('--championship')) {
  league = 'championship';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/england/championship/`;
} else if (findArg('--eredivisie')) {
  league = 'eredivisie';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/netherlands/eerste-divisie/`;
} else if (findArg('--primeira')) {
  league = 'primeira';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/portugal/liga-portugal/`;
} else if (findArg('--oneliga')) {
  league = 'oneliga';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/czech-republic/mol-cup/`;
} else if (findArg('--superliga')) {
  league = 'superliga';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  throw new Error('not supported')
} else if (findArg('--jupiler')) {
  league = 'jupiler';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  throw 'not supported'
} else if (findArg('--leagueone')) {
  league = 'leagueone';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/england/league-one/`;
} else if (findArg('--leaguetwo')) {
  league = 'leaguetwo';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/england/league-two/`;
} else if (findArg('--botola')) {
  league = 'botola';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[1]
  url = `https://www.oddsportal.com/soccer/morocco/botola-pro/results/`;
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/soccer/morocco/botola-pro-2021-2022/results/';
  }
} else if (findArg('--nba')) {
  league = 'nba';
  YEAR_BASED_ON_MONTH = YEARS_BASED_ON_MONTH[2]
  HOURS_OFFSET_PARIS = -8;
  url = `https://www.oddsportal.com/basketball/usa/nba/results/`;
  if (SEASON === 2021) {
    url = 'https://www.oddsportal.com/basketball/usa/nba-2021-2022/results/'
  } else if (SEASON === 2020) {
    url = 'https://www.oddsportal.com/basketball/usa/nba-2020-2021/results/'
  }
} else {
  throw new Error('unknown league, provide for example --bundesliga')
}
teamsCompleteName = require(__dirname + `/${league}/utils`).teamsCompleteName;
path = `${__dirname}/${league}/games-wh-${SEASON}.json`;
games = JSON.parse(fs.readFileSync(path, "utf8"));

let i = 1;
let j = 0;
const go = async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  console.log(url)
  await page.goto(url);

  await new Promise((r) => {
    setTimeout(r, 6000);
  });
  let html;
  try {
    html = await page.evaluate(() => {
      return document.querySelectorAll('table#tournamentTable')[0].innerHTML;
    });
  } catch (err) {
    console.log(err);
    html = null;
  }

  if (html) {
    const root = parse(html);
    const lines = root.querySelectorAll("tr");

    if (lines.length === 1) {
      console.log(j, 'games recorded');
      fs.writeFileSync(
        path,
        JSON.stringify(games, null, 2),
        'utf8'
      )
      process.exit(0);
    }
    let date;
    let playoffs = false;
    lines.forEach(a => {
      let d;
      try {
        if (a.childNodes[0].childNodes[0].rawTagName === 'span') {
          d = a.childNodes[0].childNodes[0].childNodes[0].rawText;
          // Skip Play Offs or Play Out or Relegation games
          if (a.childNodes[0].childNodes[1] && a.childNodes[0].childNodes[1].rawText) {
            if (a.childNodes[0].childNodes[1].rawText.includes('Leg')) {
              console.log('skip because of Leg')
              playoffs = true;
            }
            if (a.childNodes[0].childNodes[1].rawText.includes('Relegation')) {
              console.log('skip because of relegation game')
              playoffs = true;
            }
            if (a.childNodes[0].childNodes[1].rawText.includes('Championship Group')) {
              console.log('skip because of Championship Group game')
              playoffs = true;
            }
            if (a.childNodes[0].childNodes[1].rawText.includes('Play')) {
              console.log('skip because of play offs')
              playoffs = true;
            }
          } else {
            playoffs = false;
          }
        }
      } catch (e) {}
      if (d) {
        console.log('d', d)
        date = parseDate(
          d,
          0, 
          0,
          SEASON,
          YEAR_BASED_ON_MONTH
        );
        return;
      }
      if (playoffs) {
        return;
      }
      if (date === "ignore") {
        return;
      }
      if (!a.childNodes[1] || typeof a.childNodes[1].childNodes[0] === 'undefined') {
        return;
      }

      if (!date) {
        return;
      }

      // each game unique date
      console.log('-------- now must calculate precise date for game, hour :', a.childNodes[0].childNodes[0].rawText);
      const dateWithHour = `${date}T${a.childNodes[0].childNodes[0].rawText}`;
      console.log('dateWithHour', dateWithHour)
      const gameDate = new Date(new Date(dateWithHour).getTime() + HOURS_OFFSET_PARIS * 60 * 60 * 1000).toISOString().slice(0,10)
      console.log('gameDate', gameDate);

      const tryToGetText = (t1, t2, t3) => {
        console.log(t1);
        console.log(t2);
        console.log(t3);
        try {
          if (t1.rawText) {
            if (!t1.rawText.includes(' - ')) throw 'no1'
            return t1.rawText
          } else {
            if (!t1.childNodes[0].rawText.includes(' - ')) throw 'no2'
            return t1.childNodes[0].rawText;
          }
        } catch (err) {
          try {
            if (t2.rawText) {
              if (!t2.rawText.includes(' - ')) throw 'no3'
              return t2.rawText
            } else {
              if (!t2.rawText.includes(' - ')) throw 'no4'
              return t2.childNodes[0].rawText;
            }
          } catch (err) {
            if (t3.rawText) {
              if (!t3.rawText.includes(' - ')) throw 'no5'
              return t3.rawText
            } else {
              if (!t3.childNodes[0].rawText.includes(' - ')) throw 'no6'
              return t3.childNodes[0].rawText;
            }
          }
        }
      }
      const teams = tryToGetText(
        a.childNodes[1].childNodes[4],
        a.childNodes[1].childNodes[0],
        a.childNodes[1].childNodes[2],
      )
      let team1 = teams.split(' - ')[0];
      if (!teamsCompleteName[team1]) {
        if (team1 === "Team LeBron") return;
        if (team1 === "Team Durant") return;
        console.log('teams : ', teams)
        throw new Error('Unknown team ' + team1)
        process.exit(1)
      }
      team1 = teamsCompleteName[team1];
      let team2 = teams.split(' - ')[1];
      if (!teamsCompleteName[team2]) {
        if (team2 === "Team LeBron") return;
        if (team2 === "Team Durant") return;
        console.log('teams : ', teams)
        throw new Error('Unknown team ' + team2)
        process.exit(1)
      }
      team2 = teamsCompleteName[team2];
      // postponed
      if (a.childNodes[2].childNodes[0].rawText === 'postp.') {
        return;
      }
      if (a.childNodes[2].childNodes[0].rawText.includes('abn')) {
        return;
      }
      if (a.childNodes[2].childNodes[0].rawText.includes('award')) {
        return;
      }
      const odd1 = parseFloat(a.childNodes[2].childNodes[0].rawText);
      if (odd1 < 1 || odd1 > 40) {
        throw new Error('Error with odd 1 ' + odd1);
        process.exit(1)
      }
      let oddDraw;
      let odd2;
      if (findArg('--nba') || findArg('--nfl')) {
        odd2 = parseFloat(a.childNodes[3].childNodes[0].rawText);
        if (odd2 < 1 || odd2 > 40) {
          throw new Error('Error with oddDraw ' + oddDraw);
          process.exit(1)
        }
        console.log(`${gameDate} - ${team1} (odd ${odd1}) - ${team2} (odd ${odd2})`)
      } else {
        oddDraw = parseFloat(a.childNodes[3].childNodes[0].rawText);
        if (oddDraw < 1 || oddDraw > 40) {
          throw new Error('Error with oddDraw ' + oddDraw);
          process.exit(1)
        }
        odd2 = parseFloat(a.childNodes[4].childNodes[0].rawText);
        if (odd2 < 1 || odd2 > 80) {
          throw new Error('Error with odd2 ' + odd2);
          process.exit(1)
        }
        console.log(`${gameDate} - ${team1} (odd ${odd1}) - (draw ${oddDraw}) - ${team2} (odd ${odd2})`)
      }
      // bug with this game
      if (gameDate === "2022-03-18" && team1 === "Boch") {
        return;
      }
      if (!games[team1]) {
        games[team1] = {}
      }
      if (!games[team1][gameDate]) {
        games[team1][gameDate] = {
          date: gameDate,
          op: team2,
          home: true,
        }
      }
      let oddsTeam1;
      if (findArg('--nba') || findArg('--nfl')) {
        oddsTeam1 = [odd1,odd2]
      } else {
        oddsTeam1 = [odd1,odd2,oddDraw]
      }
      games[team1][gameDate] = {
        ...games[team1][gameDate],
        odds: {
          ...games[team1][gameDate].odds,
          oddsportal: oddsTeam1
        }
      }
      if (!games[team2]) {
        games[team2] = {}
      }
      if (!games[team2][gameDate]) {
        games[team2][gameDate] = {
          date: gameDate,
          op: team1,
          home: false,
        }
      }
      let oddsTeam2;
      if (findArg('--nba') || findArg('--nfl')) {
        oddsTeam2 = [odd2,odd1]
      } else {
        oddsTeam2 = [odd2,odd1,oddDraw]
      }
      games[team2][gameDate] = {
        ...games[team2][gameDate],
        odds: {
          ...games[team2][gameDate].odds,
          oddsportal: oddsTeam2
        }
      }
      j += 1;
    });

    console.log('end of page ' + i);
    console.log(j, 'odds updated or games created')
    await browser.close();
    fs.writeFileSync(
      path,
      JSON.stringify(games, null, 2),
      'utf8'
    )
    process.exit(0);
  }
}
go();