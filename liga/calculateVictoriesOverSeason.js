const fs = require("fs");

const getProcessArgv = require("./utils").getProcessArgv;

const SEASONS = getProcessArgv("--seasons");

SEASONS.split(',').forEach(season => {
  const games = JSON.parse(
    fs.readFileSync(__dirname + "/games-wh-" + season + ".json", "utf8")
  );
  console.log(season, Object.keys(games).length, 'teams');
  Object.keys(games).forEach((team) => {
    const gamesKeys = Object.keys(games[team])
    gamesKeys.forEach((date, index) => {
      const currentGame = games[team][date];
      const previousGame = games[team][gamesKeys[index - 1]];
      if (!currentGame.hasOwnProperty('victory')) {
        if (currentGame.v) {
          delete currentGame.v;
        }
        return;
      }
      if (index === 0 && games[team][date].hasOwnProperty("victory")) {
        currentGame.v = currentGame.victory ? 1 : 0;
      } else if (currentGame.hasOwnProperty("victory")) {
        if (
          !previousGame.hasOwnProperty("victory") ||
          !previousGame.hasOwnProperty("v")
        ) {
          console.log(previousGame);
          throw new Error('Previous game has no v or victory property ' + date)
        }
        currentGame.v = (previousGame.v * index + (currentGame.victory ? 1 : 0)) / (index + 1);
        currentGame.v = Math.round(currentGame.v * 10000) / 10000;
      }
    });
  });
  
  fs.writeFileSync(
    __dirname + "/games-wh-" + season + ".json",
    JSON.stringify(games, null, 2),
    "utf8"
  );
  
});
