module.exports.getProcessArgv = (param) => {
  const index = process.argv.findIndex((arg) => arg === param);
  if (index === -1) {
    return undefined;
  }

  return process.argv[index + 1];
};

module.exports.rightPad = (str, length) => {
  let s = str;
  for (let i = 0; i <= length - str.length; i += 1) {
    s += " ";
  }
  return s;
};

module.exports.IGNORE_OP = {
  'all': [],
};

module.exports.teamsCompleteName = {
  "Atletico Madrid": "Atletico Mad",
  "Atlético Madrid": "Atletico Mad",
  "Atl. Madrid": "Atletico Mad",
  "Celta Vigo": "Celta Vig",
  "RC Celta Vigo": "Celta Vig",
  "Real Madrid": "Real Madrid",
  "Real Madrid": "Real Madrid",
  "Elche": "Elche",
  "Elche": "Elche",
  "Majorque": "Majorque",
  "Mallorca": "Majorque",
  "Ath Bilbao": "Athletic Bilb",
  "Athletic Bilbao": "Athletic Bilb",
  "Athletic Club Bilbao": "Athletic Bilb",
  "Getafe": "Getafe",
  "Getafe CF": "Getafe",
  "Betis": "Betis",
  "Real Betis": "Betis",
  "Villarreal": "Villarreal",
  "Villareal": "Villarreal",
  "Villarreal CF": "Villarreal",
  "Villareal CF": "Villarreal",
  "Real Sociedad": "Real Socie",
  "Real Sociedad": "Real Socie",
  "Osasuna": "Osasuna",
  "CA Osasuna": "Osasuna",
  "Almeria": "Almeria",
  "Almeria": "Almeria",
  "Valladolid": "Real Vall",
  "Real Valladolid": "Real Vall",
  "Real Valladolid CF": "Real Vall",
  "CD Leganés": "Leganés",
  "Granada CF": "Grenade",
  "Granada": "Grenade",
  "RCD Mallorca": "Mallorca",
  "SD Eibar": "Eibar",
  "Deportivo Alaves": "Alaves",
  "Alaves": "Alaves",
  "Cadix": "Cadix",
  "Cadiz": "Cadix",
  "Cadiz CF": "Cadix",
  "Elche CF": "Elche",
  "Cádiz CF": "Cadix",
  "Girona": "Girona",
  "Girona FC": "Girona",
  "Rayo Vallecano": "Rayo Valle",
  "Rayo Vallecano": "Rayo Valle",
  "Rayo Vallecano": "Rayo Valle",
  "Valence": "Valence",
  "Valence": "Valence",
  "Valencia": "Valence",
  "Valencia CF": "Valence",
  "Espanyol": "Espanyol",
  "RCD Espanyol": "Espanyol",
  "Deportivo Alavés": "Alavés",
  "Séville": "Séville",
  "Sevilla FC": "Séville",
  "Sevilla": "Séville",
  "Barcelone": "Barcelone",
  "FC Barcelona": "Barcelone",
  "Barcelona": "Barcelone",
  "Levante": "Levante",
  "Levante UD": "Levante",
  "UD Levante": "Levante",
  "SD Eibar": "Eibar",
  "Eibar": "Eibar",
  "SD Huesca": "Huesca",
  "Huesca": "Huesca",
};
