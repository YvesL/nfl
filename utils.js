const Writable = require('stream').Writable;
const readline = require('readline');

module.exports.getSingleProcessArgv = (param) => {
  const index = process.argv.findIndex((arg) => arg === param);
  if (index === -1) {
    return false;
  }

  return true;
};

module.exports.getProcessArgv = (param) => {
  const index = process.argv.findIndex((arg) => arg === param);
  if (index === -1) {
    return undefined;
  }

  return process.argv[index + 1];
};

module.exports.toId = (config) => {
  return `${config.pointsForVictory}/${config.pointsForDefeat}/${config.betIfScoreSuperiorTo}/${config.betIfOddSuperiorTo}/${config.pointsPerGoalScored}/${config.pointsForGoalTaken}/${config.factorM1}/${config.factorM2}/${config.factorM3}/${config.victoriesRateBonus}/${config.homeBonus}/${config.betOnDraw}`;
};

module.exports.parseDate = (
  d,
  hour,
  offset,
  yearStartSeason,
  yearBasedOnMonth
) => {
  let date = null;
  console.log('parseDate : ', d);
  let month;
  let day;
  let year;
  if (
    d.includes('Mon') ||
    d.includes('Tue') ||
    d.includes('Wed') ||
    d.includes('Fri') ||
    d.includes('Sun') ||
    d.includes('Sat') ||
    d.includes('Thu')
  ) {
    console.log('FUN MODE');
    month = d.slice(4, 7);
    console.log('month', month);
    day = d.slice(8, 10);
    console.log('day', day);
    console.log(year);
    year = yearStartSeason + (yearBasedOnMonth[yearStartSeason] || 0);
  } else if (d.length === 11) {
    day = d.slice(0, 2);
    month = d.slice(3, 6);
    year = d.slice(7, 12);
    // ongoing game
  } else if (d.includes("'")) {
    return 'ignore';
    // half time
  } else if (d.includes('HT')) {
    return 'ignore';
  } else if (d.includes('League')) {
    return 'ignore';
  } else if (d.includes('National')) {
    return 'ignore';
  } else if (d.includes('Yesterday')) {
    const a = d.replace('Yesterday, ', '');
    day = a.slice(0, 2);
    month = a.slice(3, 6);
    year = yearStartSeason + (yearBasedOnMonth[month] || 0);
  } else if (d.includes('Play Offs')) {
    return 'ignore';
  } else if (d.includes('Relegation')) {
    return 'ignore';
  } else if (d.includes('Pre-seas')) {
    return 'ignore';
  } else if (d.includes('All Stars')) {
    return 'ignore';
  } else if (d.includes('Today')) {
    const a = d.replace('Today, ', '');
    day = a.slice(0, 2);
    month = a.slice(3, 6);
    year = yearStartSeason + (yearBasedOnMonth[month] || 0);
  } else if (d.includes('Tomorrow')) {
    console.log('TOMOROW', d);
    const a = d.replace('Tomorrow, ', '');
    day = a.slice(0, 2);
    month = a.slice(3, 6);
    year = yearStartSeason + (yearBasedOnMonth[month] || 0);
  } else {
    console.log('Unknown date ' + d);
    throw new Error('Unknown date ' + d);
  }
  month = {
    Jan: '01',
    Feb: '02',
    Mar: '03',
    Apr: '04',
    May: '05',
    Jun: '06',
    Jul: '07',
    Aug: '08',
    Sep: '09',
    Oct: '10',
    Nov: '11',
    Dec: '12',
  }[month];

  if (typeof month !== 'string' || month.length !== 2) {
    console.log('Unknown month ' + d);
    throw new Error('Unknown month ' + d);
  }
  date = `${year}-${month}-${day}`;
  return date;
  /* console.log(date);
  date = new Date(date);
  console.log(date);
  console.log('hour', hour)
  date.setHours(hour);
  console.log(date);
  console.log('offset', offset)
  date = new Date(new Date(date).getTime() + offset * 60 * 60 * 1000);
  console.log(date); */
  return date.toISOString().slice(0, 10);
};
module.exports.fromId = (id) => {
  return {
    pointsForVictory: parseFloat(id.split('/')[0]),
    pointsForDefeat: parseFloat(id.split('/')[1]),
    betIfScoreSuperiorTo: parseFloat(id.split('/')[2]),
    betIfOddSuperiorTo: parseFloat(id.split('/')[3]),
    pointsPerGoalScored: parseFloat(id.split('/')[4]),
    pointsPerGoalTaken: parseFloat(id.split('/')[5]),
    factorM1: parseFloat(id.split('/')[6]),
    factorM2: parseFloat(id.split('/')[7]),
    factorM3: parseFloat(id.split('/')[8]),
    victoriesRateBonus: parseFloat(id.split('/')[9]),
    homeBonus: parseFloat(id.split('/')[10]),
    betOnDraw: id.split('/')[11] === 'false' ? false : true,
  };
};

module.exports.rightPad = (str, length) => {
  let s = str;
  for (let i = 0; i <= length - str.length; i += 1) {
    s += ' ';
  }
  return s;
};

const teams2020 = [
  'den',
  'chi',
  'dal',
  'lar',
  'det',
  'lac',
  'gb',
  'car',
  'sf',
  'min',
  'kc',
  'ind',
  'nyg',
  'cin',
  'wsh',
  'sea',
  'bal',
  'cle',
  'phi',
  'pit',
  'mia',
  'buf',
  'nyj',
  'ari',
  'tb',
  'ne',
  'jax',
  'no',
  'atl',
  'ten',
  'hou',
  'lv',
];

module.exports.IGNORE_OP = {
  2016: ['oak', 'sd'],
  2017: ['oak', 'was'],
  2018: ['oak'],
  2019: ['oak'],
  2020: [],
  2021: [],
};

const TEAMS = {
  2016: teams2020.filter((t) => t !== 'lv'),
  2017: teams2020.filter((t) => t !== 'lv'),
  2018: teams2020.filter((t) => t !== 'lv'),
  2019: teams2020.filter((t) => t !== 'lv').concat(['ari']),
  2020: teams2020,
  2021: teams2020,
};
module.exports.TEAMS = TEAMS;

module.exports.simplePrompt = (text) => {
  return new Promise((resolve) => {
    var mutableStdout = new Writable({
      write: function (chunk, encoding, callback) {
        if (!this.muted) process.stdout.write(chunk, encoding);
        callback();
      },
    });

    mutableStdout.muted = false;

    var rl = readline.createInterface({
      input: process.stdin,
      output: mutableStdout,
      terminal: true,
    });

    rl.question(text, function () {
      resolve('');
      console.log('');
      rl.close();
    });

    mutableStdout.muted = true;
  });
};

module.exports.plusNDays = (date, n) => {
  return new Date(new Date(date).getTime() + n * 1000 * 60 * 60 * 24)
    .toISOString()
    .slice(0, 10);
};

module.exports.ukToEuOddFormat = (ukOdd) => {
  const numerator = parseInt(ukOdd.split('/')[0]);
  const denominator = parseInt(ukOdd.split('/')[1]);
  return parseFloat(((numerator + denominator) / denominator).toFixed(2));
};
