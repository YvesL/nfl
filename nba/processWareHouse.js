const fs = require("fs");
const path = require("path");
const { teamsCompleteName, IGNORE_OP } = require('./utils');

let odds = fs.readFileSync(path.join(process.cwd(), 'nba/NBA_ML_Combined.csv'), 'utf8');
odds = odds.replace(/\"/g, '')

const games = {};

const seasonFromDate = (date) => {
  if (new Date(date).getMonth() < 6) {
    return parseInt(date.slice(0,4)) - 1;
  }
  if (new Date(date).getMonth() >= 6) {
    return parseInt(date.slice(0,4));
  }

  throw new Error('Unknown season ' + date)
}

if (seasonFromDate("2018-09-20") !== 2018) {
  throw new Error('2018-09-20 should have been season 2018')
}
if (seasonFromDate("2019-02-20") !== 2018) {
  throw new Error('2019-02-20 should have been season 2018')
}

const usToEuOdd = (odd) => {
  if (odd < 0) {
    return 1 + (100 / Math.abs(odd))
  } else {
    return odd / 100
  }
}
if (usToEuOdd(-100) !== 2) {
  throw new Error('usToEuOdd -100 should equal 2')
}
if (usToEuOdd(-160) !== 1.625) {
  throw new Error('usToEuOdd -160 should equal 1.625')
}
if (usToEuOdd(-400) !== 1.25) {
  throw new Error('usToEuOdd -400 should equal 1.25')
}
if (usToEuOdd(200) !== 2) {
  throw new Error('usToEuOdd 200 should equal 2')
}
if (usToEuOdd(800) !== 8) {
  throw new Error('usToEuOdd 800 should equal 8')
}

const lines = odds.split('\n');

lines.slice(1).forEach(l => {
  if (l.length === 0) return;
  const spl = l.split(',');
  let homeTeam = spl[14];
  let awayTeam = spl[2];
  if (
    IGNORE_OP.all.find(a => a === homeTeam) ||
    IGNORE_OP.all.find(a => a === awayTeam)
  ) {
    console.log(`Ignoring game ${homeTeam} - ${awayTeam}`);
    return;
  }
  if (!teamsCompleteName[homeTeam]) {
    console.log(l)
    console.log(spl)
    throw new Error('unknown home team ' + homeTeam)
  }
  if (!teamsCompleteName[awayTeam]) {
    console.log(l)
    throw new Error('unknown away team ' + awayTeam)
  }

  homeTeam = teamsCompleteName[homeTeam];
  awayTeam = teamsCompleteName[awayTeam];

  // do average between open and close
  let homeMoneyline = (parseInt(spl[20]) + parseInt(spl[21])) / 2;
  let awayMoneyline = (parseInt(spl[8]) + parseInt(spl[9])) / 2;

  // Avoid absurd odds if lots of change / inversion open/close
  if (
    (parseInt(spl[20]) < 0 && parseInt(spl[21]) > 0) ||
    (parseInt(spl[20]) > 0 && parseInt(spl[21]) < 0)
  ) {
    console.log(`Avoiding absurd average of open/close ${parseInt(spl[20])},${parseInt(spl[21])}, taking close odd`)
    homeMoneyline = parseInt(spl[21]);
    awayMoneyline = parseInt(spl[9]);
  }
  
  // Avoid absurd odds if lots of change / inversion open/close
  if (
    (parseInt(spl[8]) < 0 && parseInt(spl[9]) > 0) ||
    (parseInt(spl[8]) > 0 && parseInt(spl[9]) < 0)
  ) {
    console.log(`Avoiding absurd average of open/close ${parseInt(spl[8])},${parseInt(spl[9])}, taking close odd`)
    homeMoneyline = parseInt(spl[21]);
    awayMoneyline = parseInt(spl[9]);
  }

  
  homeMoneyline = Math.round(100 * usToEuOdd(homeMoneyline)) / 100;
  awayMoneyline = Math.round(100 * usToEuOdd(awayMoneyline)) / 100;

  const date = `${spl[1].slice(0,4)}-${spl[1].slice(4,6)}-${spl[1].slice(6,8)}`;
  //console.log(date, homeTeam, awayTeam);
  const homeScore = parseInt(spl[15]);
  const awayScore = parseInt(spl[3]);
  if (homeScore < 0 || homeScore > 200 || typeof homeScore !== 'number') {
    console.log(spl)
    throw new Error('Invalid score ' + homeScore + typeof homeScore)
  }
  if (awayScore < 0 || awayScore > 200 || typeof awayScore !== 'number') {
    console.log(spl)
    throw new Error('Invalid score ' + awayScore + typeof awayScore)
  }

  const season = seasonFromDate(date);

  if (!games[season]) {
    try {
      games[season] = JSON.parse(fs.readFileSync(
        path.join(process.cwd(), `nba/games-wh-${season}.json`),
        'utf8'
      ));
      console.log(`found nba/games-wh-${season}.json file`)
    } catch (err) {
      console.log(`created nba/games-wh-${season}.json file`)
      games[season] = {}
    }
  }

  if (!games[season][homeTeam]) games[season][homeTeam] = {};
  if (!games[season][awayTeam]) games[season][awayTeam] = {};

  if (!games[season][homeTeam][date]) {
    games[season][homeTeam][date] = {
      wk: Object.keys(games[season][homeTeam]).length,
      date: date,
      home: true,
      op: awayTeam,
      victory: homeScore > awayScore,
      score: [homeScore, awayScore],
      odds: { "wh": [homeMoneyline, awayMoneyline] },
    }
  } else {
    games[season][homeTeam][date].home = true;
    games[season][homeTeam][date].odds.wh = [homeMoneyline, awayMoneyline]
  }

  if (!games[season][awayTeam][date]) {
    games[season][awayTeam][date] = {
      wk: Object.keys(games[season][awayTeam]).length,
      date: date,
      home: false,
      op: homeTeam,
      victory: awayScore > homeScore,
      score: [awayScore, homeScore],
      odds: { "wh": [awayMoneyline, homeMoneyline] },
    }  
  } else {
    games[season][awayTeam][date].home = false;
    games[season][awayTeam][date].odds.wh = [awayMoneyline, homeMoneyline]
  }
});

console.log(Object.keys(games).length, 'seasons');
console.log(Object.keys(games).join(', '));
Object.keys(games).forEach(s => {
  let i = 0;
  Object.keys(games[s]).forEach(a => {
    i += Object.keys(games[s][a]).length
  });
  console.log(s, Object.keys(games[s]).length, 'teams', i / 2, 'games in total');
  fs.writeFileSync(
    path.join(process.cwd(), `nba/games-wh-${s}.json`),
    JSON.stringify(games[s], null, 2),
    'utf8'
  );
})