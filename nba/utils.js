const Writable = require("stream").Writable;
const readline = require("readline");

module.exports.getProcessArgv = (param) => {
  const index = process.argv.findIndex((arg) => arg === param);
  if (index === -1) {
    return undefined;
  }

  return process.argv[index + 1];
};

module.exports.rightPad = (str, length) => {
  let s = str;
  for (let i = 0; i <= length - str.length; i += 1) {
    s += " ";
  }
  return s;
};

const teams2021 = [
  "dal",
  "no",
  "phx",
  "den",
  "cle",
  "cha",
  "det",
  "phi",
  "ny",
  "ind",
  "atl",
  "orl",
  "mem",
  "mil",
  "utah",
  "wsh",
  "bkn",
  "bos",
  "lal",
  "lac",
  "min",
  "por",
  "gs",
  "sa",
  "okc",
  "chi",
  "hou",
  "mia",
  "tor",
  "sac",
];

const TEAMS = {
  2020: teams2021,
  2021: teams2021,
  2022: teams2021,
};
module.exports.TEAMS = TEAMS;

module.exports.simplePrompt = (text) => {
  return new Promise((resolve) => {
    var mutableStdout = new Writable({
      write: function (chunk, encoding, callback) {
        if (!this.muted) process.stdout.write(chunk, encoding);
        callback();
      },
    });

    mutableStdout.muted = false;

    var rl = readline.createInterface({
      input: process.stdin,
      output: mutableStdout,
      terminal: true,
    });

    rl.question(text, function () {
      resolve("");
      console.log("");
      rl.close();
    });

    mutableStdout.muted = true;
  });
};

module.exports.teamsCompleteName = {
  "Toronto": "tor",
  "Toronto Raptors": "tor",
  "toronto raptors": "tor",
  "toronto-raptors": "tor",
  raptors: "tor",
  gsw: "gs",
  "Golden State": "gs",
  "Golden State Warriors": "gs",
  "Golden": "gs",
  "golden state": "gs",
  "golden state warriors": "gs",
  "golden-state-warriors": "gs",
  warriors: "gs",
  "Boston": "bos",
  "Boston Celtics": "bos",
  "boston celtics": "bos",
  "boston-celtics": "bos",
  celtics: "bos",
  "Houston Rockets": "hou",
  "Houston": "hou",
  "houston rockets": "hou",
  "houston-rockets": "hou",
  rockets: "hou",
  "New York": "ny",
  "NY Knicks": "ny",
  "ny knicks": "ny",
  "new york knicks": "ny",
  "New York Knicks": "ny",
  "new-york-knicks": "ny",
  knicks: "ny",
  nyk: "ny",
  "Dallas Mavericks": "dal",
  "Dallas": "dal",
  "dallas mavericks": "dal",
  "dallas-mavericks": "dal",
  mavericks: "dal",
  "Memphis Grizzlies": "mem",
  "Memphis": "mem",
  "memphis grizzlies": "mem",
  "memphis-grizzlies": "mem",
  grizzlies: "mem",
  "Minnesota Timb.": "min",
  "Minnesota": "min",
  "minnesota timb.": "min",
  "minnesota timberwolves": "min",
  "Minnesota Timberwolves": "min",
  "minnesota-timberwolves": "min",
  timberwolves: "min",
  "Indiana Pacers": "ind",
  "Indiana": "ind",
  "indiana pacers": "ind",
  "indiana-pacers": "ind",
  pacers: "ind",
  "Charlotte Hornets": "cha",
  "Charlotte": "cha",
  "charlotte hornets": "cha",
  "charlotte-hornets": "cha",
  hornets: "cha",
  uta: "utah",
  "Utah Jazz": "utah",
  "Utah": "utah",
  "utah jazz": "utah",
  "utah-jazz": "utah",
  jazz: "utah",
  "Chicago Bulls": "chi",
  "Chicago": "chi",
  "chicago bulls": "chi",
  "chicago-bulls": "chi",
  bulls: "chi",
  "New Orleans Pelicans": "no",
  "New Orleans": "no",
  "new orleans pelicans": "no",
  "new-orleans-pelicans": "no",
  pelicans: "no",
  nop: "no",
  "Atlanta Hawks": "atl",
  "Atlanta": "atl",
  "atlanta hawks": "atl",
  "atlanta-hawks": "atl",
  hawks: "atl",
  "Portland TB": "por",
  "Portland": "por",
  "portland tb": "por",
  "portland trail blazers": "por",
  "Portland Trail Blazers": "por",
  "portland-trail-blazers": "por",
  blazers: "por",
  "Milwaukee Bucks": "mil",
  "Milwaukee": "mil",
  "milwaukee bucks": "mil",
  "milwaukee-bucks": "mil",
  bucks: "mil",
  "Phoenix Suns": "phx",
  "Phoenix": "phx",
  "phoenix suns": "phx",
  "phoenix-suns": "phx",
  suns: "phx",
  "Oklahoma City Thunder": "okc",
  "Oklahoma City": "okc",
  "oklahoma city thunder": "okc",
  "oklahoma-city-thunder": "okc",
  thunder: "okc",
  "Sacramento Kings": "sac",
  "Sacramento": "sac",
  "sacramento kings": "sac",
  "sacramento-kings": "sac",
  kings: "sac",
  "Los Angeles Lakers": "lal",
  "LA Lakers": "lal",
  "L.A. Lakers": "lal",
  "la lakers": "lal",
  "los angeles lakers": "lal",
  "los-angeles-lakers": "lal",
  lakers: "lal",
  "Miami Heat": "mia",
  "Miami": "mia",
  "miami heat": "mia",
  "miami-heat": "mia",
  heat: "mia",
  "Cleveland Cavaliers": "cle",
  "Cleveland": "cle",
  "cleveland cavaliers": "cle",
  "cleveland-cavaliers": "cle",
  cavaliers: "cle",
  "Washington Wiz.": "wsh",
  "Washington Wizards": "wsh",
  "Washington": "wsh",
  was: "wsh",
  "washington wiz.": "wsh",
  "washington": "wsh",
  "washington wizards": "wsh",
  "washington-wizards": "wsh",
  wizards: "wsh",
  "Philadelphia": "phi",
  "Philadelphie": "phi",
  "Philadelphie 76ers": "phi",
  "Philadelphia 76ers": "phi",
  "philadelphie 76ers": "phi",
  "philadelphia 76ers": "phi",
  "philadelphia-76ers": "phi",
  sixers: "phi",
  "Detroit": "det",
  "Detroit Pistons": "det",
  "detroit pistons": "det",
  "detroit-pistons": "det",
  pistons: "det",
  "Orlando": "orl",
  "Orlando Magic": "orl",
  "orlando magic": "orl",
  "orlando-magic": "orl",
  magic: "orl",
  "San Antonio": "sa",
  "San Antonio Spurs": "sa",
  "san antonio spurs": "sa",
  "san-antonio-spurs": "sa",
  spurs: "sa",
  sas: "sa",
  "Brooklyn Nets": "bkn",
  "brooklyn nets": "bkn",
  "brooklyn-nets": "bkn",
  "Brooklyn": "bkn",
  nets: "bkn",
  bkn: "bkn",
  "Los Angeles Clippers": "lac",
  "L.A. Clippers": "lac",
  "LA Clippers": "lac",
  "la clippers": "lac",
  clippers: "lac",
  "los angeles clippers": "lac",
  "los-angeles-clippers": "lac",
  clippers: "lac",
  "Denver Nuggets": "den",
  "Denver": "den",
  "denver nuggets": "den",
  "denver-nuggets": "den",
  nuggets: "den",
  "Adelaide 36Ers": "ade",
};

module.exports.IGNORE_OP = {
  'all': ["Unknown", "Melbourne United", "New Zealand", "CA San Lorenzo de Almagro", "Team LeBron", "Team Giannis", "Adelaide 36Ers", "Maccabi Haifa"],
  2016: [],
  2017: [],
  2018: [],
  2019: [],
  2020: [],
  2021: [],
};