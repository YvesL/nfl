const fs = require("fs");

const getProcessArgv = require("./utils").getProcessArgv;

const SEASON = getProcessArgv("--season");

const odds = JSON.parse(
  fs.readFileSync(__dirname + "/cotes-" + SEASON + ".json", "utf8")
);
const games = JSON.parse(
  fs.readFileSync(__dirname + "/games-" + SEASON + ".json", "utf8")
);

const day = 1000 * 60 * 60 * 24;
Object.keys(odds).forEach((date) => {
  if (new Date(date).getTime() > new Date().getTime() + day * 20) {
    return;
  }
  odds[date].forEach((odd) => {
    const team1 = Object.keys(odd)[0];
    const team2 = Object.keys(odd)[1];
    if (games[team1] && games[team1][date]) {
      if (!games[team1][date].odds) {
        games[team1][date].odds = {};
      }
      games[team1][date].odds = {
        ...games[team1][date].odds,
        betclic: odd,
      };
    } else {
      throw new Error(date + " " + team1 + " not found");
    }
    if (games[team2] && games[team2][date]) {
      if (!games[team2][date].odds) {
        games[team2][date].odds = {};
      }
      games[team2][date].odds = {
        ...games[team2][date].odds,
        betclic: odd,
      };
    } else {
      throw new Error(date + " " + team2 + " not found");
    }
    console.log(date + " " + team1 + "-" + team2 + " ok");
  });
});

fs.writeFileSync(
  __dirname + "/games-" + SEASON + ".json",
  JSON.stringify(games, null, 2),
  "utf8"
);
