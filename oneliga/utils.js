module.exports.getProcessArgv = (param) => {
  const index = process.argv.findIndex((arg) => arg === param);
  if (index === -1) {
    return undefined;
  }

  return process.argv[index + 1];
};

module.exports.rightPad = (str, length) => {
  let s = str;
  for (let i = 0; i <= length - str.length; i += 1) {
    s += " ";
  }
  return s;
};

module.exports.IGNORE_OP = {
  'all': [],
};

module.exports.teamsCompleteName = {
  "Slavia Prague": "Slavia Prague",
  "Zlin": "FC Fastav Zlin",
  "FC Fastav Zlin": "FC Fastav Zlin",
  "Fastav Zlín": "FC Fastav Zlin",
  "Mlada Boleslav": "Mlada Boleslav",
  "Viktoria Plzen": "FC Viktoria Plzen",
  "FC Viktoria Plzen": "FC Viktoria Plzen",
  "Plzen": "FC Viktoria Plzen",
  "Banik Ostrava": "Banik Ostrava",
  "Ostrava": "Banik Ostrava",
  "Jablonec": "Jablonec",
  "Teplice": "Teplice",
  "Ceske Budejovice": "Ceske Budejovice",
  "Bohemians 1905": "Bohemians 1905",
  "Bohemians": "Bohemians 1905",
  "Hradec Kralove": "Hradec Kralove",
  "Slovacko": "Slovacko",
  "Slovan Liberec": "Slovan Liberec",
  "Liberec": "Slovan Liberec",
  "Karvina": "Karvina",
  "Pardubice": "Pardubice",
  "FK Pardubice": "Pardubice",
  "Sigma Olomouc": "Sigma Olomouc",
  "Sparta Prague": "Sparta Prague",
  "Zbrojovka Brno": "Zbrojovka Brno",
  "Brno": "Zbrojovka Brno",
};
