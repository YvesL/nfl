const { rightPad } = require("./utils");

let processedByDate = {};
const absolutesMemoized = {};

let minOddOffsetForBetting = 0.5;
let maxOddOffsetForBetting = 0.9;

let games;
let defaultOdd;

let winsTotal = 0;
let netTotal = 0;
let winsNetTotal = 0;
let drawsTotal = 0;
let loosesTotal = 0;

const calculateAbsolute = (
  game,
  pointsForVictory,
  pointsForDefeat,
  pointsPerGoalScored,
  pointsPerGoalTaken,
  victoriesRateBonus,
  homeBonus,
  verbose
) => {
  let s = 0;
  const pointsForGoalsScored = (game.score[0] * pointsPerGoalScored);
  const pointsForGoalsTaken = (game.score[1] * pointsPerGoalTaken);
  // todo what to do for draw ?
  if (game.victory) {
    s = pointsForVictory + pointsForGoalsScored + pointsForGoalsTaken;
    if (verbose) console.log(`victory ${game.score.join('-')} ${pointsForVictory} + points goals scored (${pointsPerGoalScored}*${game.score[0]}) + points goals taken (${pointsPerGoalTaken}*${game.score[1]}) = ${s}`)
  } else {
    s = pointsForDefeat + pointsForGoalsScored + pointsForGoalsTaken;
    if (verbose) console.log(`defeat ${game.score.join('-')} ${pointsForDefeat} + points goals scored (${pointsPerGoalScored}*${game.score[0]}) + points goals taken (${pointsPerGoalTaken}*${game.score[1]}) = ${s}`)
    // draw ?
  }
  if (victoriesRateBonus !== 0) {
    if (!game.hasOwnProperty('v')) {
      throw new Error('No game.v cannot take into account victoriesRateBonus')
    }
    if (verbose) console.log('+bonus (victories)', victoriesRateBonus, game.v, game.v * victoriesRateBonus);
    // todo ignore begining season ?
    s += game.v * victoriesRateBonus;
  }
  if (game.home && homeBonus) {
    if (verbose) console.log('+bonus (home)', homeBonus);
    s += homeBonus;
  }
  if (verbose) console.log('absolute', s)
  return s;
};

/*
  At each game, set a score (also called relative scored) to a given team
  based on the absolute scoreds of the three preceding games, and the factors
  for m1 (last game) m2 and m3.
*/
const calculateRelative = (m1, m2, m3, factorM1, factorM2, factorM3, verbose) => {
  if (verbose) {
    console.log('m1.absolute', m1.absolute, 'relative m1',factorM1, factorM1 * m1.absolute)
    console.log('m2.absolute', m2.absolute, 'relative m2',factorM2, factorM2 * m2.absolute)
    console.log('m3.absolute', m3.absolute, 'relative m3',factorM3, factorM3 * m3.absolute)
  }
  return (
    factorM1 * m1.absolute + factorM2 * m2.absolute + factorM3 * m3.absolute
  );
};

const process = (
  pointsForVictory,
  pointsForDefeat,
  pointsPerGoalScored,
  pointsPerGoalTaken,
  factorM1,
  factorM2,
  factorM3,
  victoriesRateBonus,
  homeBonus,
  verbose
) => {
  const gamesKeys = Object.keys(games);
  for (let i = 0; i < gamesKeys.length; i += 1) {
    const teamGames = games[gamesKeys[i]];
    const teamGamesKeys = Object.keys(teamGames);
    // already sorted chronologically
    for (let j = 0; j < teamGamesKeys.length; j += 1) {
      const game = teamGames[teamGamesKeys[j]];
      game.odd = undefined;
      if (game.odds) {
        if (game.odds.wh) {
          game.odd = game.odds.wh;
        } else if (game.odds.betclic) {
          game.odd = game.odds.betclic;
        } else if (game.odds.oddsportal) {
          game.odd = game.odds.oddsportal;
        } else if (game.odds.oddspedia) {
          game.odd = game.odds.oddspedia;
        } else {
          game.odd = game.odds[Object.keys(game.odds)[0]];
        }
      }

      const key = `${game.date}${game.op}${pointsForVictory}${pointsForDefeat}${pointsPerGoalScored}${pointsPerGoalTaken}${victoriesRateBonus}${homeBonus}`;
      if (verbose) console.log("===", game.date, gamesKeys[i]);
      if (!game.hasOwnProperty("victory")) {
        game.future = true;
      } else {
        if (absolutesMemoized.hasOwnProperty(key)) {
          game.absolute = absolutesMemoized[key];
        } else {
          game.absolute = game.score
            ? Math.round(
                calculateAbsolute(
                  game,
                  pointsForVictory,
                  pointsForDefeat,
                  pointsPerGoalScored,
                  pointsPerGoalTaken,
                  victoriesRateBonus,
                  homeBonus,
                  verbose
                ) * 1000
              ) / 1000
            : null;
          absolutesMemoized[key] = game.absolute
        }
      }
      game.relative = "unknown";
      const m1 = teamGames[teamGamesKeys[j - 1]];
      const m2 = teamGames[teamGamesKeys[j - 2]];
      const m3 = teamGames[teamGamesKeys[j - 3]];
      if (m1 && m2 && m3 && typeof m1.absolute === "number") {
        game.relative = calculateRelative(
          m1,
          m2,
          m3,
          factorM1,
          factorM2,
          factorM3,
          verbose
        );
        if (verbose) console.log('relative', game.relative)
      }
      const wins = [];
      if (m1) wins.push(m1.victory ? "W" : "L");
      if (m2) wins.push(m2.victory ? "W" : "L");
      if (m3) wins.push(m3.victory ? "W" : "L");
      game.wins = wins;
    };
  };
};

const predict = (date, season, betsAlreadyTreated) => {
  let output = "";

  const scores = [];

  // games right before or same day as [date]
  // indexed by [team]
  let gamesJustBefore = {};
  // incoming games, after [date]
  // indexed by [team]
  let gamesIncoming = {};

  const gamesKeys = Object.keys(games);
  for (let i = 0; i < gamesKeys.length; i += 1) {
    const team = gamesKeys[i];
    const teamGames = games[team];
    if (!teamGames) {
      throw new Error("No games for team " + team + ", season " + season);
    }
    // reverse to get the most recent games first
    const gameDatesReverse = Object.keys(teamGames).reverse();
    for (let j = 0; j < gameDatesReverse.length; j += 1) {
      const gameDate = gameDatesReverse[j];
      if (!gamesJustBefore[team] && gameDate <= date) {
        gamesJustBefore[team] = teamGames[gameDate];
      }
    };

    // get incoming game, in the future of [date]
    const teamGamesKeys = Object.keys(teamGames);
    for (let j = 0; j < teamGamesKeys.length; j += 1) {
      const gameDate = teamGamesKeys[j];
      if (gamesIncoming[team]) {
        continue;
      }
      if (new Date(gameDate).getTime() > new Date(date).getTime()) {
        gamesIncoming[team] = teamGames[gameDate];
      }
    };
  };

  const gamesJustBeforeKeys = Object.keys(gamesJustBefore);
  for (let i = 0; i < gamesJustBeforeKeys.length; i += 1) {
    const team = gamesJustBeforeKeys[i];
    if (!gamesIncoming[team]) {
      continue;
    }

    const op = gamesIncoming[team].op;
    if (!op) {
      console.log("ERROR NO OPONENT for team ", team);
      console.log(gamesIncoming[team]);
      throw new Error();
    }

    // do not bet twice on the same game (all games
    // are twice in the games-202x.json file)
    if (
      betsAlreadyTreated[gamesIncoming[team].date] &&
      (betsAlreadyTreated[gamesIncoming[team].date][team] ||
        betsAlreadyTreated[gamesIncoming[team].date][gamesIncoming[team].op])
    ) {
      continue;
    }

    const gameIncoming = gamesIncoming[team];
    const opGameIncoming = gamesIncoming[op];

    if (!opGameIncoming) {
      throw new Error(
        "did not find opGameIncoming season:" +
          season +
          " team:" +
          team +
          " op:" +
          op +
          "gameIncoming.date:" +
          gameIncoming.date +
          " " +
          Object.keys(gamesIncoming).join(",")
      );
    }
    // Check that incoming game is the same for
    // [team] and [op]
    if (opGameIncoming.date !== gameIncoming.date) {
      continue;
    }
    const gameJustBefore = gamesJustBefore[team];
    let opGameJustBefore = gamesJustBefore[op];
    // the oponent's game just before [date] may be after
    // team's most recent game
    if (
      gamesIncoming[op] &&
      new Date(gamesIncoming[op].date).getTime() <
        new Date(gameIncoming.date).getTime()
    ) {
      opGameJustBefore = gamesIncoming[op];
    }

    // game has already been processed for this date
    // processedByDate != betsAlreadyTreated
    if (
      processedByDate[date + team + op] ||
      processedByDate[date + op + team]
    ) {
      continue;
    }
    processedByDate[date + team + op] = true;

    if (
      gameJustBefore.future ||
      gameIncoming.relative === "unknown" ||
      !opGameJustBefore ||
      opGameJustBefore.future ||
      opGameIncoming.relative === "unknown"
    ) {
      if (!output.length) output += `{ "processing": "${date}" },`;
      
      output +=
        `{ "incoming": true, "date": "${gameIncoming.date}", "game": "${team} vs ${op}", "text": "not enough games to calculate score" },`;

      if (!betsAlreadyTreated[gameIncoming.date]) {
        betsAlreadyTreated[gameIncoming.date] = {};
      }
      betsAlreadyTreated[gameIncoming.date][team] = true;
      continue;
    }

    const teamString = `${rightPad(team, 2)}(${gameIncoming.wins.join(",")})`;
    const opString = `${rightPad(op, 2)}(${opGameIncoming.wins.join(",")})`;

    // Compare the two .relative scores
    const comp = gameIncoming.relative - opGameIncoming.relative;

    const fav = comp > 0 ? team : op;
    scores.push({
      // always fav first
      game: fav === team ?
        `${teamString} vs ${opString}` :
        `${opString} vs ${teamString}`,
      fav: fav, // bet on favorite
      score: Math.round(10000 * Math.abs(comp)) / 10000,
      date: gameIncoming.date,
    });
  };

  scores
    .sort((s1, s2) => s2.score - s1.score)
    .forEach((a) => {
      if (!output.length) output += `{ "processing": "${date}" },`;
      output +=
        `{ "incoming": true, "date": "${a.date}", "game": "${a.game}", "fav": "${a.fav}", "score": ${Math.round(a.score * 1000) / 1000} },`;

      if (!betsAlreadyTreated[a.date]) betsAlreadyTreated[a.date] = {};
      betsAlreadyTreated[a.date][a.fav] = true;
    });

  return { scores, output };
};

const oddFromScore = (team, game) => {
  if (typeof defaultOdd === "number") {
    return defaultOdd;
  }

  if (!game.odd) {
    throw new Error(
      "Odd not found " + game.date + " " + team + " - " + game.op
    );
  }
  if (game.op === team) {
    return game.odd[1];
  } else {
    return game.odd[0];
  }
};

const bet = (scores, betIfScoreSuperiorTo, betIfOddSuperiorTo) => {
  let output = "";
  const results = {};
  let netForBettingDay = 0;
  let highScores = scores;
  highScores = scores
    // only consider scores where score > betIfScoreSuperiorTo
    // do not bet on the others
    .filter((s) => s.score > betIfScoreSuperiorTo);
  
  const highScoresKeys = Object.keys(highScores);
  for (let i = 0; i < highScoresKeys.length; i += 1) {
    const s = highScores[highScoresKeys[i]];
    const game = games[s.fav][s.date];

    /*
      Don't bet if odd of favorite (oponent) is not suffiscient
    */
    if (s.fav === game.op && game.odd[1] < betIfOddSuperiorTo) {
      continue;
    }
    /*
      Don't bet if odd of favorite (local) is not suffiscient
    */
    if (s.fav !== game.op && game.odd[0] < betIfOddSuperiorTo) {
      continue;
    }

    const odds = `[${s.fav === s.op ? game.odd[1] : game.odd[0]}]`;
    const betsValue = `[["fav", 1]]`;

    if (game.draw === true) {
      if (!results[s.date]) results[s.date] = {};

      // what to do when draw ?
      results[s.date][s.fav] = 0;
      drawsTotal += 1;
      winsNetTotal += results[s.date][s.fav];
      netTotal += results[s.date][s.fav];

      output +=
        `{ "date": "${s.date}", "outcome": "\x1b[35mdraw\x1b[0m", "game": "${s.game}", "gameScore": [${game.score.join(',')}], "fav": "${s.fav}", "score": ${s.score}, "net": ${results[s.date][s.fav]},  "odds": ${odds}, "bets": ${betsValue}, "netTotal": ${netTotal} },`;

    } else if (game.victory === true) {
      // try to find the odd
      const net = oddFromScore(s.fav, game);
      if (isNaN(net) || typeof net !== 'number') {
        throw new Error('net is NaN or typeof !== number ' + net)
      }
      if (!results[s.date]) results[s.date] = {};
      results[s.date][s.fav] = Math.round(100 * (net - 1)) / 100;
      netForBettingDay += results[s.date][s.fav];
      winsTotal += 1;
      winsNetTotal += results[s.date][s.fav];
      netTotal += results[s.date][s.fav];

      output +=
        `{ "date": "${s.date}", "outcome": "\x1b[36mwon\x1b[0m", "game": "${s.game}", "gameScore": [${game.score.join(',')}], "fav": "${s.fav}", "score": ${s.score}, "net": ${results[s.date][s.fav]},  "odds": ${odds}, "bets": ${betsValue}, "netTotal": ${netTotal} },`;

    } else if (game.victory === false) {
      if (!results[s.date]) results[s.date] = {};

      // odd is -1, stake is lost
      results[s.date][s.fav] = -1;
      netForBettingDay -= 1;
      loosesTotal += 1;
      netTotal -= 1;

      output +=
        `{ "date": "${s.date}", "outcome": "\x1B[31mlost\x1b[0m", "game": "${s.game}", "gameScore": [${game.score.join(',')}], "fav": "${s.fav}", "score": ${s.score}, "net": -1,  "odds": ${odds}, "bets": ${betsValue}, "netTotal": ${netTotal} },`;
    }
    if (game.future === true) {
      output +=
        `{ "date": "${s.date}", "future": true, "game": "${s.game}", "fav": "${s.fav}", "score": ${s.score}, "odds": ${odds}, "bets": ${betsValue} },`;
    }
  };

  if (scores[0] && Object.keys(results).length) {
    output += `{ "date": "${scores[0].date}", "day": ${Math.round(netForBettingDay * 1000) / 1000} },`;
  }

  return { output, results };
};

const betForDates = (
  startDate,
  datesToProcess,
  betIfScoreSuperiorTo,
  betIfOddSuperiorTo,
  season,
  verbose
) => {
  let output = "";
  let i = 0;
  const bets = {};
  const betsAlreadyTreated = {};
  let results = [];
  const func = (d) => {
    // predicts who will win, the incoming games by comparing
    // the .relative properties
    let t4;
    if (verbose) {
      t4 = new Date().getTime();
    }
    const predictions = predict(d, season, betsAlreadyTreated);
    output += predictions.output;

    if (predictions.scores.length > 0) {
      const betResults = bet(
        predictions.scores,
        betIfScoreSuperiorTo,
        betIfOddSuperiorTo
      );
      const betResultsResultsKeys = Object.keys(betResults.results);
      for (let j = 0; j < betResultsResultsKeys.length; j += 1) {
        const betDate = betResultsResultsKeys[j];
        if (!bets[d]) bets[d] = [];
        const betForDateKeys = Object.keys(betResults.results[betDate]);
        for (let k = 0; k < betForDateKeys.length; k += 1) {
          const team = betForDateKeys[k];
          bets[d] = bets[d].concat(betResults.results[betDate][team]);
          results = results.concat(betResults.results[betDate][team]);
        };
      };
      output += betResults.output;
    }
    if (i < datesToProcess) {
      i += 1;
      // increase day by day
      d = new Date(new Date(d).getTime() + 1000 * 3600 * 24)
        .toISOString()
        .slice(0, 10);
      func(d);
    }
  };
  func(startDate);

  return { bets, output };
};

const run = (
  gamesArg,
  defaultOddArg,
  pointsForVictory,
  pointsForDefeat,
  betIfScoreSuperiorTo,
  betIfOddSuperiorTo,
  pointsPerGoalScored,
  pointsPerGoalTaken,
  factorM1,
  factorM2,
  factorM3,
  victoriesRateBonus,
  homeBonus,
  betOnDraw,
  season,
  date,
  runs,
  verbose = false,
) => {
  games = gamesArg;
  defaultOdd = defaultOddArg;
  processedByDate = {};
  winsTotal = 0;
  winsNetTotal = 0;
  netTotal = 0;
  drawsTotal = 0;
  loosesTotal = 0;

  let t1;
  if (verbose) {
    t1 = new Date().getTime()
  }

  // adds .absolute score for each game
  // adds .relative score for each game (if at least 3 previous games)
  // adds .wins property for each game (ex: ["L" (m1), "L" (m2), "W" (m3)])
  process(
    pointsForVictory,
    pointsForDefeat,
    pointsPerGoalScored,
    pointsPerGoalTaken,
    factorM1,
    factorM2,
    factorM3,
    victoriesRateBonus,
    homeBonus,
    verbose
  );
  if (verbose) {
    console.log(`[debug] process took ${new Date().getTime() - t1}ms`)
  }

  let t2;
  if (verbose) {
    t2 = new Date().getTime()
  }
  const { bets, output } = betForDates(
    date,
    runs,
    betIfScoreSuperiorTo,
    betIfOddSuperiorTo,
    season,
    verbose
  );
  if (verbose) {
    console.log(`[debug] betForDates took ${new Date().getTime() - t2}ms`)
  }

  let t3;
  if (verbose) {
    t3 = new Date().getTime()
  }
  const wins = winsTotal + (betOnDraw ? drawsTotal : 0);
  const looses = loosesTotal + (betOnDraw ? 0 : drawsTotal);
  const net = Math.round(netTotal * 100) / 100;
  const results = {
    netPerBet: Math.round(1000 * net / (wins + looses)) / 1000,
    net: net,
    wins: wins,
    winsNet: Math.round(winsNetTotal * 100) / 100,
    looses: looses,
    rate: betOnDraw ? 
      Math.round(((winsTotal + drawsTotal) / (winsTotal + drawsTotal + loosesTotal)) * 100) / 100 :
      Math.round((winsTotal / (winsTotal + drawsTotal + loosesTotal)) * 100) / 100 ,
  }

  return { results: results, output:output.slice(0, -1) };
};

module.exports.run = run;
