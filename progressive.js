const fs = require('fs');

const { getSingleProcessArgv, getProcessArgv, plusNDays } = require('./utils');
const { exec } = require("child_process");

const season = getProcessArgv("--season");
if (typeof season !== 'string' || season.length !== 4) {
  throw new Error('Missing --season')
}
const daysToRun = parseInt(getProcessArgv("--days"), 10);
if (typeof daysToRun !== 'number') {
  throw new Error('Missing --daysToRun')
}

let envs;
let league;
if (getSingleProcessArgv("--nfl")) {
  MAX_N_BETS = 400;
  league = 'nfl';
} else if (getSingleProcessArgv("--nba")) {
  MAX_N_BETS = 400;
  league = 'nba'
} else if (getSingleProcessArgv("--ncaa")) {
  MAX_N_BETS = 400;
  league = 'ncaa'
} else if (getSingleProcessArgv("--khl")) {
  league = 'khl'
} else if (getSingleProcessArgv("--ligue1")) {
  league = 'ligue1'
} else if (getSingleProcessArgv("--bundesliga")) {
  league = 'bundesliga'
} else if (getSingleProcessArgv("--bundesliga2")) {
  league = 'bundesliga2'
} else if (getSingleProcessArgv("--seriea")) {
  league = 'seriea'
} else if (getSingleProcessArgv("--serieb")) {
  league = 'serieb'
} else if (getSingleProcessArgv("--superlig")) {
  league = 'superlig'
} else if (getSingleProcessArgv("--liga")) {
  league = 'liga'
} else if (getSingleProcessArgv("--premiere")) {
  league = 'premiere'
} else if (getSingleProcessArgv("--championship")) {
  league = 'championship'
} else if (getSingleProcessArgv("--eredivisie")) {
  league = 'eredivisie'
} else if (getSingleProcessArgv("--primeira")) {
  league = 'primeira'
} else if (getSingleProcessArgv("--oneliga")) {
  league = 'oneliga'
} else if (getSingleProcessArgv("--superliga")) {
  league = 'superliga'
} else if (getSingleProcessArgv("--jupiler")) {
  league = 'jupiler'
} else if (getSingleProcessArgv("--botola")) {
  league = 'botola'
} else if (getSingleProcessArgv("--leagueone")) {
  league = 'leagueone'
} else if (getSingleProcessArgv("--leaguetwo")) {
  league = 'leaguetwo'
} else if (getSingleProcessArgv("--north")) {
  league = 'north'
} else if (getSingleProcessArgv("--south")) {
  league = 'south'
} else {
  throw new Error("no competition");
}

envs = `./${league}/env.js`;
gamesStringified = fs.readFileSync(`./${league}/games-wh-${season}.json`, "utf8");
games = JSON.parse(gamesStringified);

const {
  POINTS_FOR_DEFEAT,
  BET_IF_SCORE_SUPERIOR_TO,
  BET_IF_ODD_SUPERIOR_TO,
  POINTS_PER_GOAL_SCORED,
  POINTS_PER_GOAL_TAKEN,
  FACTOR_M1,
  FACTOR_M2,
  FACTOR_M3,
  VICTORIES_RATE_BONUS,
  HOME_BONUS,
  getStartDate,
  DO_NOT_BET_ON_TEAMS,
  BET_ON_DRAW,
  getNumSimulationsPerProcess,
} = require(envs);

let startDate = getStartDate(season);
// start with monday
if (new Date(startDate).getDay() === 0) startDate = plusNDays(startDate, 1)
if (new Date(startDate).getDay() === 2) startDate = plusNDays(startDate, -1)
if (new Date(startDate).getDay() === 3) startDate = plusNDays(startDate, -2)
if (new Date(startDate).getDay() === 4) startDate = plusNDays(startDate, -3)
if (new Date(startDate).getDay() === 5) startDate = plusNDays(startDate, -4)
if (new Date(startDate).getDay() === 6) startDate = plusNDays(startDate, -5)

let date = startDate;
let i = 0;
let j = 'monday';
const progressive = {};
const f = () => {
  console.log(date, 'is', j);
  console.log('i', i);
  const script = `node predictForSeason.js --seasons ${season} --${league} --days ${i + 4} --net --start-date ${startDate}`;
  console.log(script);
  exec(
    script,
    { maxBuffer: 1024 * 1000 * 4 },
    function (error, stdout, stderr) {
      if (error) {
        console.log(error);
        console.log(stderr);
        throw new Error("error in child process");
      }
      if (i < daysToRun) {

        progressive[date] = stdout;
        console.log(stdout);
        if (j === 'thursday') {
          j = 'monday';
          date = plusNDays(date, 4)
          i += 4
        } else {
          j = 'thursday';
          date = plusNDays(date, 3)
          i += 3
        }
        f();
      }
    }
  );
}

f();